#scripts to load the facial mesh and labels
import argparse
import glob
import numpy as np
import torch
from torch_geometric.data import InMemoryDataset, Data
from tqdm import tqdm

from psbody.mesh import Mesh
from utils import get_vert_connectivity
from transform import Normalize
import mesh_operations
from psbody.mesh import Mesh, MeshViewers
import pandas as pd

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
def scipy_to_torch_sparse(scp_matrix):
    values = scp_matrix.data
    indices = np.vstack((scp_matrix.row, scp_matrix.col))
    i = torch.LongTensor(indices)
    v = torch.FloatTensor(values)
    shape = scp_matrix.shape

    sparse_tensor = torch.sparse.FloatTensor(i, v, torch.Size(shape))
    return sparse_tensor



class ComaDataset(InMemoryDataset):
    def __init__(self, root_dir, dtype='train', split='sliced', split_term='sliced', nVal = 100, transform=None, pre_transform=None):
        self.root_dir = root_dir
        self.split = split
        self.split_term = split_term
        self.nVal = nVal
        self.transform = transform
        self.pre_tranform = pre_transform
        # Downloaded data is present in following format root_dir/*/*/*.py
        self.data_file = glob.glob(self.root_dir + '*.ply')
        print(self.root_dir)
        super(ComaDataset, self).__init__(root_dir, transform, pre_transform)
        if dtype == 'train':
            data_path = self.processed_paths[0]
        elif dtype == 'val':
            data_path = self.processed_paths[1]
        elif dtype == 'test':
            data_path = self.processed_paths[2]
        else:
            raise Exception("train, val and test are supported data types")

        norm_path = self.processed_paths[3]
        norm_dict = torch.load(norm_path)
        self.mean, self.std = norm_dict['mean'], norm_dict['std']
        self.data, self.slices = torch.load(data_path)
        if self.transform:
            self.data = [self.transform(td) for td in self.data]

    @property
    def raw_file_names(self):
        return self.data_file

    @property
    def processed_file_names(self):
        processed_files = ['training.pt', 'val.pt', 'test.pt', 'norm.pt']
        processed_files = [self.split_term+'_'+pf for pf in processed_files]
        return processed_files

    def process(self):
        train_data, val_data, test_data = [], [], []
        label_train, label_test, label_val = [], [], []
        face_train, face_test, face_val = [], [], []
        train_vertices = []
        idx = 0
        label_y = 0
        min_num = 99999999
        count_1 = 0
        count_2 = 0
        count_semi = 0
        f = open('filter_list_13.txt','r') #load file list for inclusion   (not available for public)
        filter_list_13 = f.readlines()
        f = open('filter_list2.txt','r') #load file list for inclusion   (not available for public)
        filter_list_9 = f.readlines() 

        data_dir = self.root_dir

        df = pd.read_excel(r'final.xlsx', sheet_name = 0 ) #load tabular labels (not available for public)
        d_imgid = np.array(df['IMAGEID'].values)
        d_subid = np.array(df['subid'].values)
        d_idc = np.array(df['IDC'].values)
        d_age = np.array(df['AGE'].values)
        d_idcg = np.array(df['IDCG'].values)
        d_gender = np.array(df['out_GENDER'].values)
        d_idcb = np.array(df['IDCB'].values)
        d_bmi = np.array(df['out_BMI'].values)
        d_height = np.array(df['out_height'].values)
        d_weight = np.array(df['out_weight'].values)
        d_51 = np.array(df['out51'].values)
        d_52 = np.array(df['out52'].values)
        d_53 = np.array(df['out53'].values)
        d_61 = np.array(df['out61'].values)
        d_62 = np.array(df['out62'].values)
        d_11 = np.array(df['out11'].values)
        d_12 = np.array(df['out12'].values)
        d_13 = np.array(df['out13'].values)
        d_71 = np.array(df['out71'].values)
        d_72 = np.array(df['out72'].values)
        d_81 = np.array(df['out81'].values)
        d_82 = np.array(df['out82'].values)
        d_21 = np.array(df['out21'].values)
        d_22 = np.array(df['out22'].values)
        d_birth_age = np.array(df['out_GESTBIR'].values)
        d_edu_m = np.array(df['out_EDUCM'].values)
        d_edu_p = np.array(df['out_EDUCP'].values)
        d_income = np.array(df['out_INCOME'].values)
        d_ethnicity = np.array(df['out_ETHNINFv3'].values)


        #meshtemp = Mesh(filename = '/home/lau/env/code/python/VAE_mesh/spiralnet_plus/data/CoMA/template/template.ply')
        for i_total, data_file in tqdm(enumerate(self.data_file)):
            flag_9, flag_13, semi_flag = 0, 0, 0
            name_without_path = data_file.replace(data_dir, '')
            name_without_path = name_without_path.replace('.ply', '')
            if '(' in name_without_path:
                continue

            name_without_path2 = data_file.replace(data_dir, '')
            name_filter = name_without_path2 + '\n'
            if name_filter in filter_list_13:
                flag_13 = 1
                continue
            if name_filter in filter_list_9:
                flag_9 = 1
            if flag_9 == 0 and flag_13 == 0:
                continue
            mesh = Mesh(filename=data_file)
            name_without_path = name_without_path.replace('.ply', '')
            index_imgid = np.argwhere(d_imgid==int(name_without_path))
            if len(index_imgid) == 0:
                mesh_verts = torch.Tensor(mesh.v)
                adjacency = get_vert_connectivity(mesh.v, mesh.f).tocoo()
                edge_index = torch.Tensor(np.vstack((adjacency.row, adjacency.col))).long()
                data = Data(x=mesh_verts, y=label_y, edge_index=edge_index)
                val_data.append(data)
                label_y = 0
                label_val.append(label_y)
                count_semi = count_semi + 1
                #train_vertices.append(mesh.v)
                continue # no data from table are found    
            idc_v = d_idc[index_imgid]
            label_age = d_age[index_imgid]
            label_11 = int(d_11[index_imgid])
            label_12 = int(d_12[index_imgid]) 
            label_51 = int(d_51[index_imgid])
            label_52 = int(d_52[index_imgid]) 
            label_53 = int(d_53[index_imgid]) 
            label_61 = int(d_61[index_imgid])-1  #0 mean with PAE,  1 mean without PAE
            label_62 = int(d_62[index_imgid])
            label_71 = int(d_71[index_imgid]) 
            label_ba = int(d_birth_age[index_imgid])
            label_d_ethnicity = int(d_ethnicity[index_imgid])
            label_bmi = float(d_bmi[index_imgid])
            label_weight = float(d_weight[index_imgid])
            label_height = float(d_height[index_imgid])
            label_g = int(d_gender[index_imgid])
            label_subid = int(d_subid[index_imgid])
            if label_d_ethnicity >801:
                mesh_verts = torch.Tensor(mesh.v)
                adjacency = get_vert_connectivity(mesh.v, mesh.f).tocoo()
                edge_index = torch.Tensor(np.vstack((adjacency.row, adjacency.col))).long()
                data = Data(x=mesh_verts, y=label_y, edge_index=edge_index)
                val_data.append(data)
                label_y = 0
                label_val.append(label_y)
                count_semi = count_semi + 1
                #train_vertices.append(mesh.v)
                continue
            #if label_d_ethnicity != 1:
            #   continue
            if label_d_ethnicity == 200:
                label_d_ethnicity = 11
            if label_d_ethnicity == 300:
                label_d_ethnicity = 12
            if label_d_ethnicity == 400:
                label_d_ethnicity = 13
            if label_d_ethnicity == 500:
                label_d_ethnicity = 14
            if label_d_ethnicity == 600:
                label_d_ethnicity = 15
            if label_d_ethnicity == 700:
                label_d_ethnicity = 16
            if label_d_ethnicity == 800:
                label_d_ethnicity = 17
            if label_d_ethnicity ==2 or label_d_ethnicity==3 or label_d_ethnicity == 5 or label_d_ethnicity == 6 or label_d_ethnicity==8 or label_d_ethnicity ==9 or label_d_ethnicity == 10 or label_d_ethnicity ==11 or label_d_ethnicity == 12 or label_d_ethnicity == 13 or label_d_ethnicity == 14 or label_d_ethnicity == 15 or label_d_ethnicity == 17 :
                mesh_verts = torch.Tensor(mesh.v)
                adjacency = get_vert_connectivity(mesh.v, mesh.f).tocoo()
                edge_index = torch.Tensor(np.vstack((adjacency.row, adjacency.col))).long()
                data = Data(x=mesh_verts, y=label_y, edge_index=edge_index)
                val_data.append(data)
                label_y = 0
                label_val.append(label_y)
                count_semi = count_semi + 1
                #train_vertices.append(mesh.v)
                continue
            if label_d_ethnicity != 1 :
                label_d_ethnicity = 0
            if label_51 >800 or label_61 > 800 or label_71 > 800 or label_ba > 800 or int(label_bmi) == 999 or int(label_weight) == 999 or int(label_height) == 999 or label_g == 999:
                mesh_verts = torch.Tensor(mesh.v)
                adjacency = get_vert_connectivity(mesh.v, mesh.f).tocoo()
                edge_index = torch.Tensor(np.vstack((adjacency.row, adjacency.col))).long()
                data = Data(x=mesh_verts, y=label_y, edge_index=edge_index)
                val_data.append(data)
                label_y = 0
                label_val.append(label_y)
                count_semi = count_semi + 1
                #train_vertices.append(mesh.v)
                continue

            if label_61 == 1 or label_61 ==2 :
                if label_62>1 and label_62<=6 :
                    label_61 = 1
                    #print(label_51, label_52)
                else:
                    mesh_verts = torch.Tensor(mesh.v)
                    adjacency = get_vert_connectivity(mesh.v, mesh.f).tocoo()
                    edge_index = torch.Tensor(np.vstack((adjacency.row, adjacency.col))).long()
                    data = Data(x=mesh_verts, y=label_y,  edge_index=edge_index)
                    val_data.append(data)
                    label_y = 0
                    label_val.append(label_y)
                    count_semi = count_semi + 1
                    #train_vertices.append(mesh.v)
                    continue
            if label_61 == 0 :
                if label_51 == 1:
                    mesh_verts = torch.Tensor(mesh.v)
                    adjacency = get_vert_connectivity(mesh.v, mesh.f).tocoo()
                    edge_index = torch.Tensor(np.vstack((adjacency.row, adjacency.col))).long()
                    data = Data(x=mesh_verts, y=label_y, edge_index=edge_index)
                    val_data.append(data)
                    label_y = 0
                    label_val.append(label_y)
                    count_semi = count_semi + 1
                    #train_vertices.append(mesh.v)
                    continue
                elif label_51 == 0:
                    label_61 =0

            if label_subid == 999:
                print('big_err')
                #continue
            label_g = label_g - 1  
            label_y = label_g 
            label_pos = torch.Tensor([label_bmi, label_weight, label_height, label_61, label_d_ethnicity, label_71,label_ba])

            count_1 = count_1 + 1


            idx = idx + 1
            mesh_verts = torch.Tensor(mesh.v)
            adjacency = get_vert_connectivity(mesh.v, mesh.f).tocoo()
            edge_index = torch.Tensor(np.vstack((adjacency.row, adjacency.col))).long()
            data = Data(x=mesh_verts, y=label_y, pos= label_pos, edge_index=edge_index)


            if self.split == 'sliced':
                if idx % 100 >= 80 and idx % 100 < 100:
                #if idx == 1:
                    test_data.append(data)
                    label_test.append(label_y)
                #elif idx % 100 >= 15 and idx % 100 < 16:
                #    val_data.append(data)
                #    label_val.append(label_y)

                else:
                    train_data.append(data)
                    train_vertices.append(mesh.v)
                    label_train.append(label_y)

            elif self.split == 'expression':
                if data_file.split('/')[-2] == self.split_term:
                   test_data.append(data)

                else:
                    train_data.append(data)
                    train_vertices.append(mesh.v)
                    label_train.append(label_y)


            elif self.split == 'identity':
                if data_file.split('/')[-3] == self.split_term:
                    test_data.append(data)
                    label_test.append(label_y)

                else:
                    train_data.append(data)
                    train_vertices.append(mesh.v)
                    label_train.append(label_y)

            else:
                raise Exception('sliced, expression and identity are the only supported split terms')
        
            

        if self.split != 'sliced':
            val_data = test_data[-self.nVal:]
            test_data = test_data[:-self.nVal]
        #print(np.size(train_vertices))
        #print(np.shape(train_vertices))
        print('labeled number, unlabeled number:',idx, count_semi)
        mean_train = torch.Tensor(np.mean(train_vertices, axis=0))
        std_train = torch.Tensor(np.std(train_vertices, axis=0))
        norm_dict = {'mean': mean_train, 'std': std_train}
        if self.pre_transform is not None:
            if hasattr(self.pre_transform, 'mean') and hasattr(self.pre_transform, 'std'):
                if self.pre_tranform.mean is None:
                    self.pre_tranform.mean = mean_train
                if self.pre_transform.std is None:
                    self.pre_tranform.std = std_train
            train_data = [self.pre_transform(td) for td in train_data]
            val_data = [self.pre_transform(td) for td in val_data]
            test_data = [self.pre_transform(td) for td in test_data]
        for i in range(len(train_data)):
          train_data[i].y = label_train[i]
        for i in range(len(test_data)):
          test_data[i].y = label_test[i]
        for i in range(len(val_data)):
          val_data[i].y = label_val[i]

        print('---------------------------------------------')
        torch.save(self.collate(train_data), self.processed_paths[0])
        torch.save(self.collate(val_data), self.processed_paths[1])
        torch.save(self.collate(test_data), self.processed_paths[2])
        torch.save(norm_dict, self.processed_paths[3])

def prepare_sliced_dataset(path):
    ComaDataset(path, pre_transform=Normalize())


def prepare_expression_dataset(path):
    test_exps = ['bareteeth', 'cheeks_in', 'eyebrow', 'high_smile', 'lips_back', 'lips_up', 'mouth_down',
                 'mouth_extreme', 'mouth_middle', 'mouth_open', 'mouth_side', 'mouth_up']
    for exp in test_exps:
        ComaDataset(path, split='expression', split_term=exp, pre_transform=Normalize())

def prepare_identity_dataset(path):
    test_ids = ['FaceTalk_170725_00137_TA', 'FaceTalk_170731_00024_TA', 'FaceTalk_170811_03274_TA',
                'FaceTalk_170904_00128_TA', 'FaceTalk_170908_03277_TA', 'FaceTalk_170913_03279_TA',
                'FaceTalk_170728_03272_TA', 'FaceTalk_170809_00138_TA', 'FaceTalk_170811_03275_TA',
                'FaceTalk_170904_03276_TA', 'FaceTalk_170912_03278_TA', 'FaceTalk_170915_00223_TA']

    for ids in test_ids:
        ComaDataset(path, split='identity', split_term=ids, pre_transform=Normalize())


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Data preparation for Convolutional Mesh Autoencoders')
    parser.add_argument('-s', '--split', default='sliced', help='split can be sliced, expression or identity ')
    parser.add_argument('-d', '--data_dir', help='path where the downloaded data is stored')

    args = parser.parse_args()
    split = args.split
    data_dir = args.data_dir
    if split == 'sliced':
        prepare_sliced_dataset(data_dir)
    elif split == 'expressioin':
        prepare_expression_dataset(data_dir)
    elif split == 'identity':
        prepare_identity_dataset(data_dir)
    else:
        raise Exception("Only sliced, expression and identity split are supported")
