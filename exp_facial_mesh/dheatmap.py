# scripts to draw facial heatmaps
import pyvista as pv
import cv2
import numpy as np

pathm = '../reconstruction_frames/frame0.ply'

path1 = '../reconstruction_frames/frame0.ply'
path2 = '../reconstruction_frames/frame1.ply'
path3 = '../reconstruction_frames/frame2.ply'
path4 = '../reconstruction_frames/frame3.ply'
path5 = '../reconstruction_frames/frame4.ply'
path6 = '../reconstruction_frames/frame5.ply'
path7 = '../reconstruction_frames/frame6.ply'
path8 = '../reconstruction_frames/frame7.ply'
path9 = '../reconstruction_frames/frame8.ply'
path10 = '../reconstruction_frames/frame9.ply'
path11 = '../reconstruction_frames/frame10.ply'

val = 1.5
meshm = pv.read(pathm)
center_point = np.mean(meshm.points,axis=0)
center_point = center_point# - [0, 15, 55]
print(center_point)

distance0 = meshm.points - center_point
distance0 = np.sqrt(distance0[:,0]*distance0[:,0] + distance0[:,1]*distance0[:,1] +distance0[:,2]*distance0[:,2])

pv.set_plot_theme("ParaView")
mesh1 = pv.read(path1)
#mesh1.plot(show_edges=False,color='gray')
distance1 = mesh1.points - center_point
distance1 = np.sqrt(distance1[:,0]*distance1[:,0] + distance1[:,1]*distance1[:,1] +distance1[:,2]*distance1[:,2])
color_r = distance1 - distance0
print(color_r[0])
mesh1['colors'] = color_r
plotter = pv.Plotter(off_screen=True)
plotter.add_mesh(mesh1, scalars='colors', clim=[-val, val],show_edges=False,rgb=False)
cv2.imwrite(path1.replace('ply','jpg'),plotter.image[60:650,250:800,:])
plotter.clear()



mesh2 = pv.read(path2)
#mesh1.plot(show_edges=True)
distance2 = mesh2.points - center_point
distance2 = np.sqrt(distance2[:,0]*distance2[:,0] + distance2[:,1]*distance2[:,1] +distance2[:,2]*distance2[:,2])
color_r = distance2 - distance0
mesh2['colors'] = color_r
plotter = pv.Plotter(off_screen=True)
plotter.add_mesh(mesh2, scalars='colors', clim=[-val, val],show_edges=False,rgb=False)
cv2.imwrite(path2.replace('ply','jpg'),plotter.image)
plotter.clear()

mesh3 = pv.read(path3)
#mesh1.plot(show_edges=True)
distance3 = mesh3.points - center_point
distance3 = np.sqrt(distance3[:,0]*distance3[:,0] + distance3[:,1]*distance3[:,1] +distance3[:,2]*distance3[:,2])
color_r = distance3 - distance0
mesh3['colors'] = color_r
plotter = pv.Plotter(off_screen=True)
plotter.add_mesh(mesh3, scalars='colors', clim=[-val, val],show_edges=False,rgb=False)
cv2.imwrite(path3.replace('ply','jpg'),plotter.image[60:650,250:800,:])
plotter.clear()


mesh4 = pv.read(path4)
#mesh1.plot(show_edges=True)
distance4 = mesh4.points - center_point
distance4 = np.sqrt(distance4[:,0]*distance4[:,0] + distance4[:,1]*distance4[:,1] +distance4[:,2]*distance4[:,2])
color_r = distance4 - distance0
mesh4['colors'] = color_r
plotter = pv.Plotter(off_screen=True)
plotter.add_mesh(mesh4, scalars='colors', clim=[-val, val],show_edges=False,rgb=False)
cv2.imwrite(path4.replace('ply','jpg'),plotter.image[60:650,250:800,:])
plotter.clear()


mesh5 = pv.read(path5)
#mesh1.plot(show_edges=True)
distance5 = mesh5.points - center_point
distance5 = np.sqrt(distance5[:,0]*distance5[:,0] + distance5[:,1]*distance5[:,1] +distance5[:,2]*distance5[:,2])
color_r = distance5 - distance0
mesh5['colors'] = color_r
plotter = pv.Plotter(off_screen=True)
plotter.add_mesh(mesh5, scalars='colors', clim=[-val, val],show_edges=False,rgb=False)
cv2.imwrite(path5.replace('ply','jpg'),plotter.image[60:650,250:800,:])
plotter.clear()

mesh6 = pv.read(path6)
#mesh1.plot(show_edges=True)
distance6 = mesh6.points - center_point
distance6 = np.sqrt(distance6[:,0]*distance6[:,0] + distance6[:,1]*distance6[:,1] +distance6[:,2]*distance6[:,2])
color_r = distance6 - distance0
mesh6['colors'] = color_r
plotter = pv.Plotter(off_screen=True)
plotter.add_mesh(mesh6, scalars='colors', clim=[-val, val],show_edges=False,rgb=False)
cv2.imwrite(path6.replace('ply','jpg'),plotter.image[60:650,250:800,:])
plotter.clear()


mesh7 = pv.read(path7)
#mesh1.plot(show_edges=True)
distance7 = mesh7.points - center_point
distance7 = np.sqrt(distance7[:,0]*distance7[:,0] + distance7[:,1]*distance7[:,1] +distance7[:,2]*distance7[:,2])
color_r = distance7 - distance0
mesh7['colors'] = color_r
plotter = pv.Plotter(off_screen=True)
plotter.add_mesh(mesh7, scalars='colors', clim=[-val, val],show_edges=False,rgb=False)
cv2.imwrite(path7.replace('ply','jpg'), plotter.image[60:650,250:800,:])
plotter.clear()


mesh8 = pv.read(path8)
#mesh1.plot(show_edges=True)
distance8 = mesh8.points - center_point
distance8 = np.sqrt(distance8[:,0]*distance8[:,0] + distance8[:,1]*distance8[:,1] +distance8[:,2]*distance8[:,2])
color_r = distance8 - distance0
mesh8['colors'] = color_r
plotter = pv.Plotter(off_screen=True)
plotter.add_mesh(mesh8, scalars='colors', clim=[-val, val],show_edges=False,rgb=False)
cv2.imwrite(path8.replace('ply','jpg'),plotter.image[60:650,250:800,:])
plotter.clear()


mesh9 = pv.read(path9)
#mesh1.plot(show_edges=True)
distance9 = mesh9.points - center_point
distance9 = np.sqrt(distance9[:,0]*distance9[:,0] + distance9[:,1]*distance9[:,1] +distance9[:,2]*distance9[:,2])
color_r = distance9 - distance0
mesh9['colors'] = color_r
plotter = pv.Plotter(off_screen=True)
plotter.add_mesh(mesh9, scalars='colors', clim=[-val, val],show_edges=False,rgb=False)
cv2.imwrite(path9.replace('ply','jpg'),plotter.image[60:650,250:800,:])
plotter.clear()

mesh10 = pv.read(path10)

#mesh1.plot(show_edges=True)
distance10 = mesh10.points - center_point
distance10 = np.sqrt(distance10[:,0]*distance10[:,0] + distance10[:,1]*distance10[:,1] +distance10[:,2]*distance10[:,2])
color_r = distance10 - distance0
mesh10['colors'] = color_r
plotter = pv.Plotter(off_screen=True)
plotter.add_mesh(mesh10, scalars='colors', clim=[-val, val],show_edges=False,rgb=False)
cv2.imwrite(path10.replace('ply','jpg'),plotter.image[60:650,250:800,:])
plotter.clear()



mesh11 = pv.read(path11)

#mesh1.plot(show_edges=True)
distance11 = mesh11.points - center_point
distance11 = np.sqrt(distance11[:,0]*distance11[:,0] + distance11[:,1]*distance11[:,1] +distance11[:,2]*distance11[:,2])
color_r = distance11 - distance0
mesh11['colors'] = color_r
plotter = pv.Plotter(off_screen=True)
plotter.add_mesh(mesh11, scalars='colors', clim=[-val, val],show_edges=False,rgb=False)
#plotter.add_points(center_point)
#plotter.show()
#plotter.screenshot('screenshot.png',transparent_background=True) 
cv2.imwrite(path11.replace('ply','jpg'),plotter.image[60:650,250:800,:])
plotter.clear()
