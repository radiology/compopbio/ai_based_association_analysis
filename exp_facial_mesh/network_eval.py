# scripts for the sampling along vector p*
import time
import os
import torch
import torch.nn.functional as F
from psbody.mesh import Mesh
import numpy as np
import seaborn as sns
import umap
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve
from sklearn.metrics import auc






def torch_corr(input1, input2, device):

    mean_1 = torch.mean(input1)
    mean_2 = torch.mean(input2)
    var_1 = torch.var(input1)
    var_2 = torch.var(input2)
    vector_mean1 = mean_1*torch.ones([len(input1)]).to(device)
    vector_mean2 = mean_2*torch.ones([len(input1)]).to(device)

    diff_1 = input1 - vector_mean1
    diff_2 = input2 - vector_mean2
    exp = torch.mul(diff_1,diff_2)
    exp = torch.sum(exp)   

    #exp = 0
    #for i in range(len(input1)):
    #    exp = exp + (input1[i]-mean_1)*(input2[i]-mean_2)  
    #print(exp)
    #exit(0)
    exp = exp / len(input1)
    exp = exp / torch.sqrt(var_1*var_2)
    return exp


def eval_error(model, test_loader, device, meshdata, out_dir, mesh, numz):
    model.eval()



    final_c = 0.5505
    final_b = -0.0616

    errors = []
    mean = meshdata.mean
    std = meshdata.std
    count0, count1,acc_count_h = 0,0,0
    z0, z1 = [], []
    list_0 = []
    list_1 = []
    z_project_list = []
    bmi_list, hei_list , gender_list, zp_list = [], [] , [],[]

  

    val_weight = model.pe.weight

    print(val_weight,model.pe.bias)
    #exit(0)
    y_pre_list, y_pre_list_f, y_pre_list_m,al_list = [], [], [],[]
    with torch.no_grad():
        for i, data in enumerate(test_loader):
            x = data.x.to(device)

            z, zp = model.encoder(x)
            count1 = count1 + 1

            zp_list.append(zp.unsqueeze(0))
            z0.append(z.unsqueeze(0))
            z_project = model.pe(z)
            z_project_val = torch.sum(z_project,dim=1)
            z_project_list.append(z_project_val)

        zp_list_tensor = torch.cat(zp_list).squeeze(1)
        zp_list_tensor = zp_list_tensor.squeeze(1)

        z0_tensor = torch.cat(z0)
        z_project_list_tensor = torch.cat(z_project_list)
        num0, _ ,_ = z0_tensor.size()
        z0_tensor =z0_tensor.view(num0,numz)
        z_project_list_tensor =z_project_list_tensor.view(num0,1)
        print(z0_tensor.size())
        z0_mean = torch.mean(z0_tensor,dim=0)
        z0_max ,_= torch.max(z0_tensor,dim=0)
        z0_min ,_= torch.min(z0_tensor,dim=0)
        z_project_max ,_= torch.max(z_project_list_tensor,dim=0)
        z_project_min ,_= torch.min(z_project_list_tensor,dim=0)
        print(z_project_max, z_project_min)
        #middle = z0_mean


        for i in range(11): #0.02 0.26
            middle = z0_mean + (5-i) * val_weight*3
            middle_project = model.pe(middle)
            print(middle_project)
            out0 = model.decoder(middle)
            out0 = np.squeeze(out0)
            out0 = out0.cpu().numpy() * std.numpy() + mean.numpy()
            result_mesh = Mesh(v=out0, f=mesh.f)
            result_mesh.write_ply('../reconstruction_frames' + '/frame' + str(i) + '.ply',ascii=True)
