# scripts for the SSL training
import time
import os
import torch
import torch.nn.functional as F
from psbody.mesh import Mesh
import numpy as np
import seaborn as sns
import umap
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve
from sklearn.metrics import auc


device = torch.device('cuda', 1)
model2 = torch.nn.Linear(1, 1).to(device)
optimizer2 = torch.optim.Adam(model2.parameters(),
                             lr=1e-3,
                             weight_decay=0)

def torch_corr(input1, input2, device):

    mean_1 = torch.mean(input1)
    mean_2 = torch.mean(input2)
    var_1 = torch.var(input1)
    var_2 = torch.var(input2)
    vector_mean1 = mean_1*torch.ones([len(input1)]).to(device)
    vector_mean2 = mean_2*torch.ones([len(input1)]).to(device)

    diff_1 = input1 - vector_mean1
    diff_2 = input2 - vector_mean2
    exp = torch.mul(diff_1,diff_2)
    exp = torch.sum(exp)   

    exp = exp / len(input1)
    exp = abs(exp / torch.sqrt(var_1*var_2))
    return exp



def run(model, train_loader_semi, test_loader, epochs, optimizer, scheduler, writer,
        device, numz, train_loader_corr):
    train_losses, test_losses,decorr_loss = [], [], []
    max_auc = 999
    max_epoch = 0
    train_loss_all, test_loss_all, train_acc_all, test_acc_all = [], [], [], []
    for epoch in range(1, epochs + 1):
        t = time.time()


        train_loss = train_semi(model, optimizer, train_loader_semi, train_loader_corr, device,numz, epoch)
        #train_loss = train_supervised(model, optimizer, train_loader_corr, device,numz, epoch)

        t_duration = time.time() - t

        max_auc,max_epoch, test_loss = test(model, test_loader, device, numz,max_auc,max_epoch, epoch)

        scheduler.step()
        info = {
            'current_epoch': epoch,
            'epochs': epochs,
            'train_loss': train_loss,
            'test_loss': test_loss,
            't_duration': t_duration
        }

        writer.print_info(info)
        writer.save_checkpoint(model, optimizer, scheduler, epoch)

        train_loss_all.append(train_loss)
        test_loss_all.append(test_loss)

        plt.figure()
        plt.plot(test_loss_all)
        plt.savefig('test_loss')
        plt.close()

        plt.figure()
        plt.plot(train_loss_all)
        plt.savefig('train_loss')
        plt.close()



def train_semi(model, optimizer, loader_semi, loader, device,numz, epoch): #for semi-supervised training
    model.train()
    model2.train()
    iterator = iter(loader)
    iterator_rec = iter(loader_semi)

#1---------------------------------------------------------------------
    total_loss,total_loss_r = 0,0
    acc_num = 0
    acc_g = 0
    total_num = 0
    total_loss_gender,total_loss_decorr, total_loss_hei, total_loss_bmi,total_loss_wei, total_loss_rec, total_loss_ba, total_loss_ms ,total_loss_eth,total_loss_al= 0, 0, 0, 0, 0,0,0,0,0,0

    decorr_list = []
    #for batch_num in range(20):
    for data in loader:
#-----------------------------update by loss_rec---------------
        data_rec  = iterator_rec.next()
        x = data_rec.x.to(device)
        optimizer.zero_grad()
        out, _ = model(data_rec)
        loss_rec = F.l1_loss(out, x, reduction='mean')
     
        loss_rec.backward()
        optimizer.step()
#----------------------------update by loss_rec + loss_corr ------------
        #data  = iterator.next()
        x = data.x.to(device)
        optimizer.zero_grad()
        total_num = total_num + 1
        #print(total_num)
        gender_t = data.y.to(device).float().squeeze(1)
        ms_t = data.pos[:,5].to(device).float()
        ba_t = data.pos[:,6].to(device).float()
        eth_t = data.pos[:,4].to(device).float()
        al_t = data.pos[:,3].to(device).float()
        bmi_t = data.pos[:,0].to(device).float()
        out, zp= model(data)
        zp = zp.squeeze(1)

        loss = 2*(torch_corr(bmi_t, zp, device)+torch_corr(ba_t, zp, device)+ torch_corr(gender_t, zp, device) + torch_corr(ms_t, zp, device) + torch_corr(eth_t, zp, device))- torch_corr(al_t, zp, device) + F.l1_loss(out, x, reduction='mean') 

        total_loss += loss.item()
        loss_gender = torch_corr(gender_t, zp, device)
        loss_bmi = torch_corr(bmi_t, zp, device)
        loss_eth = torch_corr(eth_t, zp, device)
        loss_ba = torch_corr(ba_t, zp, device)
        loss_ms = torch_corr(ms_t, zp, device)
        loss_al = torch_corr(al_t, zp, device)
        total_loss_gender += loss_gender.item()
        total_loss_bmi += loss_bmi.item()
        total_loss_eth += loss_eth.item()
        total_loss_ba += loss_ba.item()
        total_loss_ms += loss_ms.item()
        total_loss_al += loss_al.item()
        loss_rec = F.l1_loss(out, x, reduction='mean')
        total_loss_rec += loss_rec.item()
        
        loss.backward()
        optimizer.step()


        optimizer2.zero_grad()
        out, zp= model(data)
        ypre = model2(zp)#traing for LR
        ypre = ypre.squeeze(1) 
        loss_pre =  F.mse_loss(torch.sigmoid(ypre), al_t)
        loss_pre.backward()
        optimizer.zero_grad()
        optimizer2.step()

    print('bmi_loss, eth_loss,ba_loss,ms_loss,ba_loss,gender_loss, al_loss:',total_loss_bmi / len(loader), total_loss_eth / len(loader), total_loss_ba / len(loader),total_loss_ms / len(loader),total_loss_gender / len(loader),total_loss_al / len(loader))
    return total_loss_rec / len(loader)#, total_loss_decorr / len(loader)


def train_supervised(model, optimizer, loader, device,numz, epoch): # for supervised training
    model.train()
    model2.train()
    total_loss,total_loss_r = 0,0
    acc_num = 0
    acc_g = 0
    total_num = 0
    total_loss_gender,total_loss_decorr, total_loss_hei, total_loss_bmi,total_loss_wei, total_loss_rec, total_loss_ba, total_loss_ms, total_loss_eth,total_loss_al = 0, 0, 0, 0, 0,0,0,0,0,0

    decorr_list = []
   
    for data in loader:
        x = data.x.to(device)
        optimizer.zero_grad()

        total_num = total_num + 1
        #print(total_num)
        gender_t = data.y.to(device).float().squeeze(1)
        ms_t = data.pos[:,5].to(device).float()
        ba_t = data.pos[:,6].to(device).float()
        eth_t = data.pos[:,4].to(device).float()
        al_t = data.pos[:,3].to(device).float()
        bmi_t = data.pos[:,0].to(device).float()
        out, zp= model(data)
        zp = zp.squeeze(1)

        loss = 2*(torch_corr(bmi_t, zp, device)+torch_corr(ba_t, zp, device)+ torch_corr(gender_t, zp, device) + torch_corr(ms_t, zp, device) + torch_corr(eth_t, zp, device))- torch_corr(al_t, zp, device) + F.l1_loss(out, x, reduction='mean') #supervised


        total_loss += loss.item()
        loss_gender = torch_corr(gender_t, zp, device)
        loss_bmi = torch_corr(bmi_t, zp, device)
        loss_eth = torch_corr(eth_t, zp, device)
        loss_ba = torch_corr(ba_t, zp, device)
        loss_ms = torch_corr(ms_t, zp, device)
        loss_al = torch_corr(al_t, zp, device)
        total_loss_gender += loss_gender.item()
        total_loss_bmi += loss_bmi.item()
        total_loss_eth += loss_eth.item()
        total_loss_ba += loss_ba.item()
        total_loss_ms += loss_ms.item()
        total_loss_al += loss_al.item()
        loss_rec = F.l1_loss(out, x, reduction='mean')
        total_loss_rec += loss_rec.item()
        
        loss.backward()
        optimizer.step()

        optimizer2.zero_grad()
        out, zp= model(data)
        ypre = model2(zp)
        ypre = ypre.squeeze(1) 
        loss_pre =  F.mse_loss(torch.sigmoid(ypre), al_t)
        loss_pre.backward()
        optimizer.zero_grad()
        optimizer2.step()



    print('bmi_loss, eth_loss,ba_loss,ms_loss,gender_loss, al_loss:',total_loss_bmi / len(loader), total_loss_eth / len(loader), total_loss_ba / len(loader),total_loss_ms / len(loader),total_loss_gender / len(loader),total_loss_al / len(loader))
    return total_loss_rec / len(loader)#, total_loss_decorr / len(loader)





def test(model, loader, device, numz,max_auc,max_epoch,epoch):
    model.eval()
    model2.eval()
    total_loss = 0
    latent_all = []
    latent_all2 = []
    label_all = []
    label_all2 = []
    gender_all = []
    latent_all = np.array(latent_all)
    marker_list = []
    acc_count = 0
    acc_g = 0
    acc_count_b, acc_count_w,acc_count_h = 0,0,0
    gender_list,bmi_list, hei_list , wei_list, zp_list, al_list, ba_list, ms_list, eth_list = [], [] , [], [],[],[],[],[],[]
    with torch.no_grad():
        for i, data in enumerate(loader):
            x = data.x.to(device)
            pred, zp = model(data)
            zp = zp.squeeze(1)
            zp = torch.sigmoid(model2(zp))
            gender_t = data.y.to(device).float().squeeze(0)
            ms_t = data.pos[:,5].to(device).float()
            ba_t = data.pos[:,6].to(device).float()
            eth_t = data.pos[:,4].to(device).float()
            al_t = data.pos[:,3].to(device).float()
            bmi_t = data.pos[:,0].to(device).float()
            gender_list.append(gender_t.unsqueeze(0))
            bmi_list.append(bmi_t.unsqueeze(0))
            al_list.append(al_t.unsqueeze(0))
            eth_list.append(eth_t.unsqueeze(0))
            ba_list.append(ba_t.unsqueeze(0))
            ms_list.append(ms_t.unsqueeze(0))
            zp_list.append(zp.unsqueeze(0))

            loss = F.l1_loss(pred, x, reduction='mean') 
            total_loss += loss.item()

    #wei_list_tensor = torch.cat(wei_list).squeeze(1)
    gender_list_tensor = torch.cat(gender_list).squeeze(1)
    bmi_list_tensor = torch.cat(bmi_list).squeeze(1)
    al_list_tensor = torch.cat(al_list).squeeze(1)
    eth_list_tensor = torch.cat(eth_list).squeeze(1)
    ba_list_tensor = torch.cat(ba_list).squeeze(1)
    ms_list_tensor = torch.cat(ms_list).squeeze(1)
    zp_list_tensor = torch.cat(zp_list).squeeze(1)
    #print(zp_list_tensor.size(), bmi_list_tensor.size())
    #corr_wei = torch_corr(wei_list_tensor, zp_list_tensor, device)
    corr_gender = torch_corr(gender_list_tensor, zp_list_tensor, device)
    corr_bmi = torch_corr(bmi_list_tensor, zp_list_tensor, device)
    corr_al = torch_corr(al_list_tensor, zp_list_tensor, device)
    corr_eth = torch_corr(eth_list_tensor, zp_list_tensor, device)
    corr_ba = torch_corr(ba_list_tensor, zp_list_tensor, device)
    corr_ms = torch_corr(ms_list_tensor, zp_list_tensor, device)


    fpr, tpr, thresholds = roc_curve(al_list_tensor.cpu().numpy(), zp_list_tensor.cpu().numpy(), pos_label=1)
    curr_auc1 = auc(fpr, tpr)

    if curr_auc1 < 0.5:
        fpr, tpr, thresholds = roc_curve(al_list_tensor.cpu().numpy(), zp_list_tensor.cpu().numpy(), pos_label=0)
        curr_auc1 = auc(fpr, tpr)
        print(curr_auc1)
    else:
        print(curr_auc1)


    print('--------------bmi_loss, eth_loss,ms_loss,ba_loss,gender_loss, al_loss:',corr_bmi.cpu().numpy() , corr_eth.cpu().numpy(), corr_ms.cpu().numpy() ,corr_ba.cpu().numpy(),corr_gender.cpu().numpy() ,corr_al.cpu().numpy())
    return max_auc, max_epoch, total_loss / len(loader)


