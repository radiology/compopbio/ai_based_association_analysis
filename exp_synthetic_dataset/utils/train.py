import os
import sys
import argparse
import numpy as np
import torch
import torch.optim as optim
import torch.nn as nn
import math
import torch.nn.functional as F
sys.path.append(".")
import matplotlib.pyplot as plt
from utils.data import get_celeba_loaders
from utils.vis import plot_loss, imsave, Logger, imsave_inp
from utils.loss import FLPLoss, KLDLoss
from simple_vae import VAE

from sklearn.metrics import roc_curve
from sklearn.metrics import auc
import pandas as pd
import seaborn as sns
from sklearn.feature_selection import mutual_info_regression
import dcor


path_training_data = '../synthetic_dataset/5folds/train_fold_1.txt' # paths for input, switch the number for different folds
path_testing_data ='../synthetic_dataset/5folds/test_fold_1.txt'

# Device
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


parser = argparse.ArgumentParser(description='vae.pytorch')
parser.add_argument('--logdir', type=str, default="./log/vae-123")
parser.add_argument('--batch_train', type=int, default=16)
parser.add_argument('--batch_test', type=int, default=64)
parser.add_argument('--epochs', type=int, default=50) 
parser.add_argument('--initial_lr', type=float, default=0.005)
parser.add_argument('--lambda_par', type=float, default=1000.0) # Eq. 5 in the paper # 300 is good
parser.add_argument('--eta', type=float, default=2.0)# Eq. 3 in the paper     if eta==0, then disable confounder correction
parser.add_argument('--model', type=str, default="pvae", choices=["vae-123", "vae-345", "pvae"])
parser.add_argument('--attr', type=str, default="frame")
args = parser.parse_args()


# Dataloader
dataloaders = get_celeba_loaders(args.batch_train, args.batch_test, path_training_data, path_testing_data)
# Model
model = VAE(device=device).to(device)

# Reconstruction loss
if args.model == "pvae":
    reconst_criterion = nn.MSELoss(reduction='sum')
elif args.model == "vae-123" or args.model == "vae-345":
    reconst_criterion = FLPLoss(args.model, device, reduction='mean')
# KLD loss
kld_criterion = KLDLoss(reduction='sum')

# Solver
optimizer = optim.Adam(model.parameters(), lr=args.initial_lr)
# Scheduler
scheduler = optim.lr_scheduler.StepLR(optimizer, 1, gamma=0.97, last_epoch=-1)

#----model lr, see t^=LR(zp) in the manuscript

model_lr = torch.nn.Linear(1,1).to(device)
optimizer_lr = optim.Adam(model_lr.parameters(), lr=0.05)
scheduler_lr = optim.lr_scheduler.StepLR(optimizer_lr, 1, gamma=0.97, last_epoch=-1)
#----
# Log
logdir = args.logdir

if not os.path.exists(logdir):
    os.makedirs(logdir)
# Logger
logger = Logger(os.path.join(logdir, "log.txt"))
# History
history = {"train": [], "test": []}

# Save config
logger.write('----- Options ------')
for k, v in sorted(vars(args).items()):
    logger.write('%s: %s' % (str(k), str(v)))



def torch_corr(input1, input2, device):  #The correlation loss function

    mean_1 = torch.mean(input1)
    mean_2 = torch.mean(input2)
    var_1 = torch.var(input1)
    var_2 = torch.var(input2)
    vector_mean1 = mean_1*torch.ones([len(input1)]).to(device)
    vector_mean2 = mean_2*torch.ones([len(input2)]).to(device)
    #print(input2, vector_mean2)
    diff_1 = input1 - vector_mean1
    diff_2 = input2 - vector_mean2

    exp = torch.mul(diff_1,diff_2)
    exp = torch.sum(exp)   


    exp = exp / len(input1)
    exp = exp / torch.sqrt(var_1*var_2)
    return abs(exp)



# Start training
for epoch in range(args.epochs):
    num_list, col_list, zp_list, num_list_train,zp_list_train,col_list_train,logit_out_list = [],[],[],[],[],[],[]
    mse_loss_total = 0
    for phase in ["train", "test"]:
        if phase == "train":
            model.train(True)
            model_lr.train(True)
            logger.write(f"\n----- Epoch {epoch+1} -----")
        else:
            model.train(False)
            model_lr.train(False)
        # Loss
        running_loss = 0.0
        # Data num
        data_num = 0

        i = 0
        i_test = 0
        for data in dataloaders[phase]:
            x = data[0].to(device)
            num_t = data[1].to(device) #radius
            col_t = data[2].to(device) #brightness
            id_name = data[3]

            #print(num_t, col_t,id_name)
            #exit(0)
            # Optimize params

            if phase == "train":
                optimizer.zero_grad()
                if len(num_t) != args.batch_train:
                    continue
                # Pass forward
                x = x.to(device)
                rec_x, mean, logvar, latent_z , zp  = model(x)
                # Calc loss
                zp = zp.view([len(num_t)])
                corr_loss = - torch_corr(zp, col_t,device) + args.eta*torch_corr(zp, num_t, device)
                reconst_loss =  reconst_criterion(rec_x, x)
                kld_loss = kld_criterion(mean, logvar)
                loss =    reconst_loss +  kld_loss+ args.lambda_par*corr_loss 
                #loss =    reconst_loss + args.lambda_par*corr_loss 
                loss.backward()
                optimizer.step()
                #-------finish the update of the proposed AI model

                optimizer_lr.zero_grad()
                rec_x, _, _, _ , zp= model(x)
                #print(zp.size())
                logit_out = model_lr(zp)
                logit_out = logit_out.view([len(logit_out)])
                #print(logit_out.size(), col_t.size())
                loss_lr = F.mse_loss(logit_out.float(), col_t.float(),reduction='mean')
                loss_lr.backward()
                optimizer.zero_grad()
                optimizer_lr.step()
                zp = zp.view([len(num_t)])
                zp_list_train = np.append(zp_list_train, zp.detach().cpu().numpy())
                num_list_train = np.append(num_list_train, num_t.cpu().numpy())
                col_list_train = np.append(col_list_train, col_t.cpu().numpy())
                # ------------------------finish the update of the LR model
                x[x<0]=0
                rec_x[rec_x<0]=0
                x[x>1]=1
                rec_x[rec_x>1]=1
                # Visualize
                #if i == 0 and x.size(0) >= args.batch_train:
                 #   imsave(x, rec_x, os.path.join(logdir, f"epoch{epoch+1}", f"train.png"), 2, 4)
    
            elif phase == "test":
                with torch.no_grad():
                    optimizer.zero_grad()
                    # Pass forward
                    x = x.to(device)
                    rec_x, mean, logvar, latent_z, zp= model(x)
                    logit_out = model_lr(zp)
                    zp = zp.view([len(num_t)])
                    logit_out = logit_out.view([len(num_t)])
                    #print(logit_out.size(), col_t.size())
                    loss_lr_test = F.mse_loss(logit_out.float(), col_t.float(),reduction='mean')
                    num_list = np.append(num_list, num_t.cpu().numpy())
                    logit_out_list = np.append(logit_out_list, logit_out.cpu().numpy())
                    col_list = np.append(col_list, col_t.cpu().numpy())
                    zp_list = np.append(zp_list, zp.cpu().numpy())
                    # Calc loss
                    mse_loss_total = mse_loss_total +loss_lr_test.item()
                    diy_l1loss = nn.L1Loss(reduction='mean')
                    loss = diy_l1loss(rec_x, x)  #L1 loss for Table 1 in the manuscript
                    x[x<0]=0
                    rec_x[rec_x<0]=0
                    x[x>1]=1
                    rec_x[rec_x>1]=1
                    #if i_test == 0 and x.size(0) >= 16:
                   #     imsave(x, rec_x, os.path.join(logdir, f"epoch{epoch+1}", f"test-{i}.png"), 8, 8)
                   #     i_test = i_test + 1

            i = i + 1
            # Add stats

            running_loss += loss  #* x.size(0)
            data_num +=1

        # Log

        epoch_loss = running_loss / data_num
        logger.write(f"{phase} L1-Loss : {epoch_loss:.4f}")
        history[phase].append(epoch_loss)

        #if phase == "test":
        #    plot_loss(logdir, history)
#-----
    mse_loss_total = mse_loss_total/len(dataloaders["test"])
    #print('data_num:, len_dataloader:', data_num, len(dataloaders["test"]))
    scheduler.step()
    scheduler_lr.step()
    #tp_list=np.array(tp_list)
    zp_list_train=np.array(zp_list_train)
    zp_list=np.array(zp_list)
    #zp2_list=np.array(zp2_list)
    col_list=np.array(col_list)
    logit_out_list = np.array(logit_out_list)
    num_list=np.array(num_list)
    col_list_train=np.array(col_list_train)
    num_lis_traint=np.array(num_list_train)
    corr_loss_num = np.corrcoef(zp_list, num_list)[0,1]   #the correlation coefficient in the test set should be global-population-level, instead of batch-level
    corr_loss_col = np.corrcoef(zp_list, col_list)[0,1]
    print(num_list.shape, num_list_train.shape)
    corr_loss_num_train = np.corrcoef(zp_list_train, num_list_train)[0,1]   #the correlation coefficient in the test set should be global-population-level, instead of batch-level
    corr_loss_col_train = np.corrcoef(zp_list_train, col_list_train)[0,1]
    #print(num_list_train.shape, zp_list_train, num_list.shape, zp_list.shape)

    mi_train_c = mutual_info_regression(zp_list_train.reshape(-1,1), num_list_train.reshape(-1))
    mi_test_c = mutual_info_regression(zp_list.reshape(-1,1), num_list.reshape(-1))
    mi_test_t = mutual_info_regression(zp_list.reshape(-1,1), col_list.reshape(-1))
    epoch_preds_64 = zp_list.astype(np.float64)
    epoch_cf1_64 = num_list.astype(np.float64)
    epoch_targets_64 = col_list.astype(np.float64)

    # Calculate distance correlations
    dcor_c = dcor.u_distance_correlation_sqr(epoch_preds_64.reshape(-1, 1), epoch_cf1_64)
    dcor_t = dcor.u_distance_correlation_sqr(epoch_preds_64.reshape(-1, 1), epoch_targets_64)


    print('corr_radius, corr_brightness, MI_train_c in train set:', corr_loss_num_train,corr_loss_col_train,mi_train_c)
    print('corr_radius, corr_brightness, mi_test_c, mi_test_t,dcor_c,dcor_t:', corr_loss_num,corr_loss_col,mi_test_c, mi_test_t,dcor_c,dcor_t)
    print('r-mse_loss of brightness in test set:',np.sqrt(mse_loss_total)) 
    #print('corr_num2, corr_col2', corr_loss_num2,corr_loss_col2)
    torch.save(model.state_dict(),\
        os.path.join(logdir, 'ours_with_VAE'+str(epoch)+'.pth'))

    torch.save(model_lr.state_dict(),\
        os.path.join(logdir, 'ours_with_LR'+str(epoch)+'.pth'))



#---------------- plot the results after training
dataloaders = get_celeba_loaders(1, 1, path_training_data,path_testing_data)
with torch.no_grad():
    model.train(False)
    print(model.pe.weight)
    #exit(0)
    z_list, zp_list, num_list, col_list = [], [], [], []
    for data in dataloaders["test"]:
        x = data[0]
        x = x.to(device)
        num_t = data[1].to(device)
        col_t = data[2].to(device)
        rec_x, _, _, latent_z , zp= model(x)
        z_list.append(latent_z.unsqueeze(1))
        zp_list.append(zp.unsqueeze(1))
        num_list.append(num_t.unsqueeze(0))
        col_list.append(col_t.unsqueeze(0))

    z_list_tensor = torch.cat(z_list).squeeze(1)
    zp_list_tensor = torch.cat(zp_list).squeeze(1)
    zp_list_tensor = zp_list_tensor.squeeze(1)


    num_list_tensor = torch.cat(num_list).squeeze(1)
    col_list_tensor = torch.cat(col_list).squeeze(1)
    #exit(0)
    z_mean = torch.mean(z_list_tensor,dim=0)
    zp_mean = torch.mean(zp_list_tensor,dim=0)
    zp_std = torch.std(zp_list_tensor, dim=0)
    z_project_max,_ = torch.max(zp_list_tensor,dim=0)
    z_project_min,_ = torch.min(zp_list_tensor,dim=0)
    print('mean-3*SD, mean+3*SD:',model_lr((zp_mean-3*zp_std).unsqueeze(0)).item(), model_lr((zp_mean+3*zp_std).unsqueeze(0)).item())
    rec_tensors = torch.zeros((11,1, 64, 64))
    z_mean = z_mean.unsqueeze(0)


    flag_sampling = 1
    par_amp = 0.03
    loop_count = 0
    while flag_sampling and loop_count < 10000:      # control the sampling range to be mean+-3*SD
        loop_count = loop_count + 1
        middle_left = z_mean + par_amp*(0-5) * model.pe.weight  
        middle_right = z_mean + par_amp*(10-5) * model.pe.weight  
        middle_project_left = model.pe(middle_left)
        middle_project_right = model.pe(middle_right)
        #print(middle_project_right)
        tmp_max = torch.maximum(middle_project_left, middle_project_right)
        if torch.abs(tmp_max - (zp_mean+3*zp_std)) < 0.01:
            break
        if tmp_max - (zp_mean+3*zp_std) > 0:
            par_amp = par_amp - 0.001 
        else:
            par_amp = par_amp + 0.001

    
    points_list = []  # now start the sampling along vector p*
    for i in range(11): 
        middle = z_mean + (i-5) * model.pe.weight*par_amp 
        middle_project = model.pe(middle)
        logit_out = model_lr(middle_project)
        points_list.append(middle)
        print('brightness frame:',i+1, logit_out.item())
        rec_tensors[i] = model.decode(middle)
        rec_tensors[rec_tensors<0]=0
        rec_tensors[rec_tensors>1]=1
        imsave_inp(rec_tensors, os.path.join(logdir, f"{args.attr}-{i+1}.png"))


    #plot the datapoints in the latent space, together with the vector p*
    points_list = torch.cat(points_list)
    points_list = points_list.cpu().numpy()
    print(points_list.shape)

    latent_all= np.array(z_list_tensor.cpu().numpy())
    latent_all = latent_all.reshape((-1,2))
    print(latent_all.shape)

    #exit(0)
    z_list_numpy = z_list_tensor.cpu().numpy()
    num_list = num_list_tensor.cpu().numpy()
    col_list = col_list_tensor.cpu().numpy()
    print(z_list_numpy.shape)
    data_df = pd.DataFrame({ 'z0':z_list_numpy[:,0],'z1':z_list_numpy[:,1],'Brightness':col_list, 'Radius':num_list})

    plt.figure(figsize=(15,9))
    cmap = sns.cubehelix_palette(rot=-.2, as_cmap=True,reverse=True)
    g = sns.relplot(
        #alpha=0.7,
        data=data_df,
        x="z0", y="z1",
        hue="Brightness", size="Radius",
        palette = cmap, sizes=(15, 180),
        #palette = "gist_gray_r", sizes=(2, 100),
    )

    #exit(0)
    #g.set(xscale="log", yscale="log")
    line_pe = model.pe.weight.cpu().numpy()
    point1 = [points_list[0,0],points_list[0,1]]
    point2 = [points_list[10,0],points_list[10,1]]
    x_values = [point1[0], point2[0]]
    y_values = [point1[1], point2[1]]
#    plt.plot(x_values, y_values, 'go', linestyle="--")
    #g.ax.xaxis.grid(True, "minor", linewidth=.25)
    #g.ax.yaxis.grid(True, "minor", linewidth=.25)
    plt.plot(points_list[:,0], points_list[:,1], '.', color='green');
    plt.arrow(points_list[0,0],points_list[0,1],points_list[10,0] - points_list[0,0],points_list[10,1]-points_list[0,1], head_width=0.2, head_length=0.2, linewidth=1.5, color='g', length_includes_head=False)
    plt.axis('scaled')
    g.despine(left=True, bottom=True)
    plt.savefig('latent_space.svg', format='svg', dpi=1200,bbox_inches="tight")
