import os
import sys
import argparse
import numpy as np
import pandas as pd
import torch
import torch.optim as optim
import torch.nn as nn
from PIL import Image
sys.path.append(".")
import seaborn as sns
import umap
import matplotlib.pyplot as plt
#from utils.anno import gen_images
from vis import imsave_inp, imsave
from simple_vae import VAE
from data import get_celeba_loaders


path_training_data = '../synthetic_dataset/5folds/train_fold_1.txt' # paths for input, switch the number for different folds
path_testing_data ='../synthetic_dataset/5folds/test_fold_1.txt'


parser = argparse.ArgumentParser(description='vae.pytorch')
parser.add_argument('--logdir', type=str, default="./log/vae-123")
parser.add_argument('--num', type=int, default=10)
parser.add_argument('--gpu', type=str, default="0")
parser.add_argument('--model', type=str, default="pvae", choices=["vae-123", "vae-345", "pvae"])
parser.add_argument('--path', type=str, default="./log/pvae/final_model.pth")
parser.add_argument('--attr', type=str, default="Smiling")
parser.add_argument('--batch_train', type=int, default=64)
parser.add_argument('--batch_test', type=int, default=16)
args = parser.parse_args()

device = torch.device('cuda', 1)
reconst_criterion = nn.MSELoss(reduction='sum')
# Log
logdir = os.path.join(args.logdir, "interpolate")
if not os.path.exists(logdir):
    os.makedirs(logdir)
# Model
model = VAE(device=device, is_train=False).to(device)
print(args.path)
try:
    model.load_state_dict(torch.load("/home/lau/codes/mesh_VAE/vae_ball_git/log/pvae/final_model97.pth.pth"))
except:
    print("Invalid weight path.")


def torch_corr(input1, input2, device):

    mean_1 = torch.mean(input1)
    mean_2 = torch.mean(input2)
    var_1 = torch.var(input1)
    var_2 = torch.var(input2)
    vector_mean1 = mean_1*torch.ones([len(input1)]).to(device)
    vector_mean2 = mean_2*torch.ones([len(input1)]).to(device)

    diff_1 = input1 - vector_mean1
    diff_2 = input2 - vector_mean2
    exp = torch.mul(diff_1,diff_2)
    exp = torch.sum(exp)   

    #exp = 0
    #for i in range(len(input1)):
    #    exp = exp + (input1[i]-mean_1)*(input2[i]-mean_2)  
    #print(exp)
    #exit(0)
    exp = exp / len(input1)
    exp = exp / torch.sqrt(var_1*var_2)
    return exp
#---------------- plot the results after training
dataloaders = get_celeba_loaders(1, 1, path_training_data,path_testing_data)
with torch.no_grad():
    model.train(False)
    print(model.pe.weight)
    #exit(0)
    z_list, zp_list, num_list, col_list = [], [], [], []
    for data in dataloaders["test"]:
        x = data[0]
        x = x.to(device)
        num_t = data[1].to(device)
        col_t = data[2].to(device)
        rec_x, _, _, latent_z , zp= model(x)
        z_list.append(latent_z.unsqueeze(1))
        zp_list.append(zp.unsqueeze(1))
        num_list.append(num_t.unsqueeze(0))
        col_list.append(col_t.unsqueeze(0))

    z_list_tensor = torch.cat(z_list).squeeze(1)
    zp_list_tensor = torch.cat(zp_list).squeeze(1)
    zp_list_tensor = zp_list_tensor.squeeze(1)


    num_list_tensor = torch.cat(num_list).squeeze(1)
    col_list_tensor = torch.cat(col_list).squeeze(1)
    #exit(0)
    z_mean = torch.mean(z_list_tensor,dim=0)
    zp_mean = torch.mean(zp_list_tensor,dim=0)
    zp_std = torch.std(zp_list_tensor, dim=0)
    z_project_max,_ = torch.max(zp_list_tensor,dim=0)
    z_project_min,_ = torch.min(zp_list_tensor,dim=0)
    print('mean-3*SD, mean+3*SD:',model_lr((zp_mean-3*zp_std).unsqueeze(0)).item(), model_lr((zp_mean+3*zp_std).unsqueeze(0)).item())
    rec_tensors = torch.zeros((11,1, 64, 64))
    z_mean = z_mean.unsqueeze(0)


    flag_sampling = 1
    par_amp = 0.03
    loop_count = 0
    while flag_sampling and loop_count < 10000:      # control the sampling range to be mean+-3*SD
        loop_count = loop_count + 1
        middle_left = z_mean + par_amp*(0-5) * model.pe.weight  
        middle_right = z_mean + par_amp*(10-5) * model.pe.weight  
        middle_project_left = model.pe(middle_left)
        middle_project_right = model.pe(middle_right)
        #print(middle_project_right)
        tmp_max = torch.maximum(middle_project_left, middle_project_right)
        if torch.abs(tmp_max - (zp_mean+3*zp_std)) < 0.01:
            break
        if tmp_max - (zp_mean+3*zp_std) > 0:
            par_amp = par_amp - 0.001 
        else:
            par_amp = par_amp + 0.001

    
    points_list = []  # now start the sampling along vector p*
    for i in range(11): 
        middle = z_mean + (i-5) * model.pe.weight*par_amp 
        middle_project = model.pe(middle)
        logit_out = model_lr(middle_project)
        points_list.append(middle)
        print('brightness frame:',i+1, logit_out.item())
        rec_tensors[i] = model.decode(middle)
        rec_tensors[rec_tensors<0]=0
        rec_tensors[rec_tensors>1]=1
        imsave_inp(rec_tensors, os.path.join(logdir, f"{args.attr}-{i+1}.png"))

    points_list = torch.cat(points_list)
    points_list = points_list.cpu().numpy()
    print(points_list.shape)

    latent_all= np.array(z_list_tensor.cpu().numpy())
    latent_all = latent_all.reshape((-1,2))
    print(latent_all.shape)

    #exit(0)
    z_list_numpy = z_list_tensor.cpu().numpy()
    num_list = num_list_tensor.cpu().numpy()
    col_list = col_list_tensor.cpu().numpy()
    print(z_list_numpy.shape)
    data_df = pd.DataFrame({ 'z0':z_list_numpy[:,0],'z1':z_list_numpy[:,1],'Brightness':col_list, 'Radius':num_list})

    plt.figure(figsize=(15,9))
    cmap = sns.cubehelix_palette(rot=-.2, as_cmap=True,reverse=True)
    g = sns.relplot(
        #alpha=0.7,
        data=data_df,
        x="z0", y="z1",
        hue="Brightness", size="Radius",
        palette = cmap, sizes=(15, 180),
        #palette = "gist_gray_r", sizes=(2, 100),
    )

    #exit(0)
    #g.set(xscale="log", yscale="log")
    line_pe = model.pe.weight.cpu().numpy()
    point1 = [points_list[0,0],points_list[0,1]]
    point2 = [points_list[10,0],points_list[10,1]]
    x_values = [point1[0], point2[0]]
    y_values = [point1[1], point2[1]]
#    plt.plot(x_values, y_values, 'go', linestyle="--")
    #g.ax.xaxis.grid(True, "minor", linewidth=.25)
    #g.ax.yaxis.grid(True, "minor", linewidth=.25)
    plt.plot(points_list[:,0], points_list[:,1], '.', color='green');
    plt.arrow(points_list[0,0],points_list[0,1],points_list[10,0] - points_list[0,0],points_list[10,1]-points_list[0,1], head_width=0.2, head_length=0.2, linewidth=1.5, color='g', length_includes_head=False)
    plt.axis('scaled')
    g.despine(left=True, bottom=True)
    plt.savefig('latent_space.svg', format='svg', dpi=1200,bbox_inches="tight")

