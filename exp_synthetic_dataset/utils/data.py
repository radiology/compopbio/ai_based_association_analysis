import os
import glob
import numpy as np
from PIL import Image, ImageOps
import cv2
import imgaug as ia
from imgaug import augmenters as iaa
import torch
import torchvision
from torchvision import datasets, transforms
from torch.utils.data import Dataset, DataLoader,TensorDataset

class ImgAugTransform:
  def __init__(self):
    self.aug = iaa.Sequential([
        iaa.Fliplr(0.5),
        iaa.Sometimes(0.4,\
            iaa.ContrastNormalization((0.8, 1.2), per_channel=0.5)),
        iaa.Sometimes(0.4,\
            iaa.GaussianBlur(sigma=(0, 0.2))),
    ])
      
  def __call__(self, img):
    img = np.array(img)
    img = self.aug.augment_image(img)
    return img




class ImageDataset(Dataset):
    def __init__(self, paths_image,is_aug=False):
        super(ImageDataset, self).__init__()

        # Length
        self.length = len(paths_image)
        # Image path
        self.paths_image = paths_image

        # Augment
        self.is_aug = is_aug
        self.transform = transforms.Compose([
            transforms.ColorJitter(brightness=0.2, contrast=0.2, saturation=0.2, hue=0.1),
            ImgAugTransform(),
            lambda x: Image.fromarray(x),
        ])
        # Preprocess
        self.output = transforms.Compose([
            transforms.Resize((64, 64)),
            transforms.ToTensor(),
            ])

    def __len__(self):
        return self.length

    def __getitem__(self, idx):
        # Image
        #print(idx)
        img = Image.open(self.paths_image[idx].replace('\n',''))

        img = self.output(img)

        file_name = self.paths_image[idx].replace('\n','')
        id_name = file_name.split('/')
        id_name = id_name[len(id_name)-1].replace('.png','')

        split_file_name = file_name.split('_')
        len_split = len(split_file_name)
        color = split_file_name[len_split-1].replace('.png','')
        number = split_file_name[len_split-2]

        return img, float(number) ,1-( float(color)-3.5735)/(131.4535-3.5735), str(id_name) #convert gray scaleto brightness (0-1)



# this dataloader was implemented with class 'ImageDataset' for dynamic memory control
def get_celeba_loaders(batch_train, batch_test, path_training_data,path_testing_data):



    f_train = open(path_training_data,'r')      # paths for input, switch the number for different folds
    file_list_train = f_train.readlines()

    f_test = open(path_testing_data,'r')      # paths for input
    file_list_test = f_test.readlines()


    datasets = {
        "train":  ImageDataset(file_list_train,True),
        "test": ImageDataset(file_list_test, False)
    }

    dataloaders = {
        "train": DataLoader(  datasets["train"],  batch_size=batch_train, shuffle=True),
        "test": DataLoader( datasets["test"], batch_size=batch_test, shuffle=False)
    }

    return dataloaders
