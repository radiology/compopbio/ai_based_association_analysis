# 'vae-123', 'vae-345', 'pvae'
model="pvae"
logdir="./log/${model}"
eta=2.0


python -u utils/train.py --model $model --logdir $logdir --eta $eta

exit 0
