#!/bin/bash
#SBATCH --ntasks=2           ### How many CPU cores do you need?
#SBATCH --mem=24G            ### How much RAM memory do you need?
#SBATCH -p hm                 ### The queue to submit to: express, short, long, interactive
##SBATCH --nodelist=gpu006
#SBATCH --exclude=gpu-hm-001
##SBATCH --exclude=gpu004
###SBATCH --exclude=gpu001
#SBATCH --gres=gpu:1         ### How many GPUs do you need?
#SBATCH -t 6-00:00:00        ### The time limit in D-hh:mm:ss format
#SBATCH -o /trinity/home/xliu1/RSS_cognition_beta0.4/rec_only_all/out_%j.log        ### Where to store the console output (%j is the job number)
#SBATCH -e /trinity/home/xliu1/RSS_cognition_beta0.4/rec_only_all/error_%j.log      ### Where to store the error output
#SBATCH --job-name=rec_only_all  ### Name your job so you can distinguish between jobs

f=0

# Load the modules

module purge
# automatically loads all dependencies such as cuda
# replace with required tensorflow version! (check with module avail which tensorflow version are available)
# module load TensorFlow/2.2.0-fosscuda-2019b-Python-3.7.4  
# module load TensorFlow/1.15.2-fosscuda-2019b-Python-3.7.4
module load Python/3.7.4-GCCcore-8.3.0

# use when you need to read/write many files quickly in tmp directory:
# source /tmp/${SLURM_JOB_USER}.${SLURM_JOB_ID}/prolog.env
# activate virtualenv after loading tensorflow/python module
# replace with your own virtualenv!

#source "/trinity/home/bli/venv_lacune_tf-gpu/bin/activate"
#. activate 
source "/trinity/home/bli/venv_brainVAE_pytorch/bin/activate"

echo "Job starts ---"

nvidia-smi
echo "nvcc version"
nvcc --version

cd /trinity/home/bli/Representation_learning/Script/
#cd /trinity/home/bli/Lacune_detection/Exp/Script
#python3 "Train_baselineBCE_WM_Diff_FNloss_regL2-shell.py" ${f}
#python3 "train_GPUcluster_GM_VAEx16_saveBest_beta_RSSADNI.py" "--beta" "0.4"
# change nef=16, batch=8
#python3 "train_GPUcluster_GM_VAEx16_saveBest_beta_RSS_n64_semi.py"
# python3 "train_GPUcluster_GM_VAEx16_saveBest_beta_RSS_n64_semi_dualInput_runCorr.py"
#python3 "train_GPUcluster_GM_VAEx16_saveBest_beta_RSS_n64_supervisedGfactor_runCap.py"

#python3 "train_GPUcluster_GM_Encoderx16_saveBest_beta_RSS_n64_supervisedCorrOnly.py"
#python3 "train_GPUcluster_GM_VAEx16_saveBest_beta_RSS_n64_supervised_tgt_22_gamma0_beta0.1.py"
#python3 "train_GPUcluster_GM_VAEx16_saveBest_beta_RSS_n64_semi_dualInput_gfOnly_batchTest.py"
#python3 "train_GPUcluster_GM_VAEx16_saveBest_beta_RSS_n64_semi_dualInput_gfOnly_runCap.py"


#python3 "Laufinal_n64_supervised_tgt_4_gamma0.001_beta12_ncc2.5_f4_plt.py"
python3 "Laufinal_n64_S_x16_batch8_rec_for_jy.py"
