import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import numpy as np
from os.path import join, exists, dirname
from os import environ, makedirs
import cv2
# import torch

if "DISPLAY" not in environ:
    plt.switch_backend("Agg")

def plot_loss(dirname_, history):
    """ Plot loss """

    plot_train = np.array(history["train"])
    is_val = ("test" in history.keys())
    if is_val:
        plot_val = np.array(history["test"])

    if not exists(dirname_):
        makedirs(dirname_)

    # epochs
    n_epochs = len(plot_train)

    # X axis
    x = [i for i in range(1, n_epochs+1)]
    
    # plot loss
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, plot_train[:], label='train loss')
    if is_val:
        plt.plot(x, plot_val[:], label='test loss')

    plt.legend()
    plt.savefig(join(dirname_,'loss.png'))
    plt.close()

def plot_loss_multi(dirname_, history):
    """ Plot loss """

    plot_train = np.array(history["train"])
    is_val = ("validation" in history.keys()) #"test"
    if is_val:
        plot_val = np.array(history["validation"]) #"test"

    if not exists(dirname_):
        makedirs(dirname_)

    # epochs
    n_epochs = int(len(plot_train)/3)

    # X axis
    x = [i for i in range(1, n_epochs+1)]
    
    # loss, 0, 3,6,9
    loss_train = [j for i, j in enumerate(plot_train) if 0 == i%3]
    loss_vali  = [j for i, j in enumerate(plot_val) if 0 == i%3]
    # rec, 1, 4, 7, 
    rec_train  = [j for i, j in enumerate(plot_train) if 1 == i%3]
    rec_vali   = [j for i, j in enumerate(plot_val) if 1 == i%3]
    # ncc, 2, 5, 8
    ncc_train  = [j for i, j in enumerate(plot_train) if 2 == i%3]
    ncc_vali   = [j for i, j in enumerate(plot_val) if 2 == i%3]
        
    
    # plot loss
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, loss_train, label='train loss')
    if is_val:
        plt.plot(x, loss_vali, label='test loss')

    plt.legend()
    plt.savefig(join(dirname_,'loss.png'))
    plt.close()
    
    # plot rec term
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, rec_train, label='train L1-norm')
    if is_val:
        plt.plot(x, rec_vali, label='test L1-norm')

    plt.legend()
    plt.savefig(join(dirname_,'L1norm.png'))
    plt.close()    
    
    # plot ncc term
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, ncc_train, label='train NCC')
    if is_val:
        plt.plot(x, ncc_vali, label='test NCC')

    plt.legend()
    plt.savefig(join(dirname_,'NCC.png'))
    plt.close()    
    return


def plot_loss_multi4(dirname_, history, nrMetric):
    """ Plot loss """

    plot_train = np.array(history["train"])
    is_val = ("validation" in history.keys()) #"test"
    if is_val:
        plot_val = np.array(history["validation"]) #"test"

    if not exists(dirname_):
        makedirs(dirname_)

    # epochs
    n_epochs = int(len(plot_train)/nrMetric) #3

    # X axis
    x = [i for i in range(1, n_epochs+1)]
    
    # loss, 0, 4,8, 12
    loss_train = [j for i, j in enumerate(plot_train) if 0 == i%nrMetric]
    loss_vali  = [j for i, j in enumerate(plot_val) if 0 == i%nrMetric]
    # rec, 1, 5, 9, 
    rec_train  = [j for i, j in enumerate(plot_train) if 1 == i%nrMetric]
    rec_vali   = [j for i, j in enumerate(plot_val) if 1 == i%nrMetric]
    # ncc, 2, 6, 10
    ncc_train  = [j for i, j in enumerate(plot_train) if 2 == i%nrMetric]
    ncc_vali   = [j for i, j in enumerate(plot_val) if 2 == i%nrMetric]
    # corr, 3, 7, 11    
    corr_train  = [j for i, j in enumerate(plot_train) if 3 == i%nrMetric]
    corr_vali   = [j for i, j in enumerate(plot_val) if 3 == i%nrMetric]
    
    ## plot loss
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, loss_train, label='train loss')
    if is_val:
        plt.plot(x, loss_vali, label='test loss')

    plt.legend()
    plt.savefig(join(dirname_,'loss.png'))
    plt.close()
    
    ## plot rec term
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, rec_train, label='train L1-norm')
    if is_val:
        plt.plot(x, rec_vali, label='test L1-norm')

    plt.legend()
    plt.savefig(join(dirname_,'L1norm.png'))
    plt.close()    
    
    ## plot ncc term
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, ncc_train, label='train NCC')
    if is_val:
        plt.plot(x, ncc_vali, label='test NCC')

    plt.legend()
    plt.savefig(join(dirname_,'NCC.png'))
    plt.close()    
    
    ## plot corr term
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, corr_train, label='train Corr')
    if is_val:
        plt.plot(x, corr_vali, label='test Corr')

    plt.legend()
    plt.savefig(join(dirname_,'Corr.png'))
    plt.close()     
    
    
    return


def plot_loss_multi8(dirname_, history, nrMetric):
    """ Plot loss """

    plot_train = np.array(history["train"])
    is_val = ("validation" in history.keys()) #"test"
    if is_val:
        plot_val = np.array(history["validation"]) #"test"

    if not exists(dirname_):
        makedirs(dirname_)

    # epochs
    n_epochs = int(len(plot_train)/nrMetric) #3

    # X axis
    x = [i for i in range(1, n_epochs+1)]
    
    # loss, 0, 4,8, 12
    loss_train = [j for i, j in enumerate(plot_train) if 0 == i%nrMetric]
    loss_vali  = [j for i, j in enumerate(plot_val) if 0 == i%nrMetric]
    # rec, 1, 5, 9, 
    rec_train  = [j for i, j in enumerate(plot_train) if 1 == i%nrMetric]
    rec_vali   = [j for i, j in enumerate(plot_val) if 1 == i%nrMetric]
    # ncc, 2, 6, 10
    ncc_train  = [j for i, j in enumerate(plot_train) if 2 == i%nrMetric]
    ncc_vali   = [j for i, j in enumerate(plot_val) if 2 == i%nrMetric]
    # corr, 3, 7, 11    
    corr_train  = [j for i, j in enumerate(plot_train) if 3 == i%nrMetric]
    corr_vali   = [j for i, j in enumerate(plot_val) if 3 == i%nrMetric]

    age_train  = [j for i, j in enumerate(plot_train) if 4 == i%nrMetric]
    age_vali   = [j for i, j in enumerate(plot_val) if 4 == i%nrMetric]

    sex_train  = [j for i, j in enumerate(plot_train) if 5 == i%nrMetric]
    sex_vali   = [j for i, j in enumerate(plot_val) if 5 == i%nrMetric]

    eduy_train  = [j for i, j in enumerate(plot_train) if 6 == i%nrMetric]
    eduy_vali   = [j for i, j in enumerate(plot_val) if 6 == i%nrMetric]

    tgt_train  = [j for i, j in enumerate(plot_train) if 7 == i%nrMetric]
    tgt_vali   = [j for i, j in enumerate(plot_val) if 7 == i%nrMetric]
    
    ## plot loss
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, loss_train, label='train loss')
    if is_val:
        plt.plot(x, loss_vali, label='test loss')

    plt.legend()
    plt.savefig(join(dirname_,'loss.png'))
    plt.close()
    
    ## plot rec term
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, rec_train, label='train L1-norm')
    if is_val:
        plt.plot(x, rec_vali, label='test L1-norm')

    plt.legend()
    plt.savefig(join(dirname_,'L1norm.png'))
    plt.close()    
    
    ## plot ncc term
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, ncc_train, label='train NCC')
    if is_val:
        plt.plot(x, ncc_vali, label='test NCC')

    plt.legend()
    plt.savefig(join(dirname_,'NCC.png'))
    plt.close()    
    
    ## plot corr term
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, corr_train, label='train Corr')
    if is_val:
        plt.plot(x, corr_vali, label='test Corr')

    plt.legend()
    plt.savefig(join(dirname_,'Corr.png'))
    plt.close()     

    """Correlation terms separately
    """
    ## plot corr term
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, age_train, label='train Corr Age')
    if is_val:
        plt.plot(x, age_vali, label='test Corr Age')

    plt.legend()
    plt.savefig(join(dirname_,'Corr_ages.png'))
    plt.close() 

    ## plot corr term
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, sex_train, label='train Corr Sex')
    if is_val:
        plt.plot(x, sex_vali, label='test Corr Sex')

    plt.legend()
    plt.savefig(join(dirname_,'Corr_sex.png'))
    plt.close() 

    ## plot corr term
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, eduy_train, label='train Corr Eduy')
    if is_val:
        plt.plot(x, eduy_vali, label='test Corr Eduy')

    plt.legend()
    plt.savefig(join(dirname_,'Corr_eduy.png'))
    plt.close() 

    ## plot corr term
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, tgt_train, label='train Corr Gfactor')
    if is_val:
        plt.plot(x, tgt_vali, label='test Corr Gfactor')

    plt.legend()
    plt.savefig(join(dirname_,'Corr_gfactor.png'))
    plt.close() 
        
    return

def plot_loss_multi6_age_gf(dirname_, history, nrMetric):
    """ Plot loss """

    plot_train = np.array(history["train"])
    is_val = ("validation" in history.keys()) #"test"
    if is_val:
        plot_val = np.array(history["validation"]) #"test"

    if not exists(dirname_):
        makedirs(dirname_)

    # epochs
    n_epochs = int(len(plot_train)/nrMetric) #3

    # X axis
    x = [i for i in range(1, n_epochs+1)]
    
    # loss, 0, 4,8, 12
    loss_train = [j for i, j in enumerate(plot_train) if 0 == i%nrMetric]
    loss_vali  = [j for i, j in enumerate(plot_val) if 0 == i%nrMetric]
    # rec, 1, 5, 9, 
    rec_train  = [j for i, j in enumerate(plot_train) if 1 == i%nrMetric]
    rec_vali   = [j for i, j in enumerate(plot_val) if 1 == i%nrMetric]
    # ncc, 2, 6, 10
    ncc_train  = [j for i, j in enumerate(plot_train) if 2 == i%nrMetric]
    ncc_vali   = [j for i, j in enumerate(plot_val) if 2 == i%nrMetric]
    # corr, 3, 7, 11    
    corr_train  = [j for i, j in enumerate(plot_train) if 3 == i%nrMetric]
    corr_vali   = [j for i, j in enumerate(plot_val) if 3 == i%nrMetric]

    age_train  = [j for i, j in enumerate(plot_train) if 4 == i%nrMetric]
    age_vali   = [j for i, j in enumerate(plot_val) if 4 == i%nrMetric]

    tgt_train  = [j for i, j in enumerate(plot_train) if 5 == i%nrMetric]
    tgt_vali   = [j for i, j in enumerate(plot_val) if 5 == i%nrMetric]
    
    ## plot loss
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, loss_train, label='train loss')
    if is_val:
        plt.plot(x, loss_vali, label='test loss')

    plt.legend()
    plt.savefig(join(dirname_,'loss.png'))
    plt.close()
    
    ## plot rec term
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, rec_train, label='train L1-norm')
    if is_val:
        plt.plot(x, rec_vali, label='test L1-norm')

    plt.legend()
    plt.savefig(join(dirname_,'L1norm.png'))
    plt.close()    
    
    ## plot ncc term
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, ncc_train, label='train NCC')
    if is_val:
        plt.plot(x, ncc_vali, label='test NCC')

    plt.legend()
    plt.savefig(join(dirname_,'NCC.png'))
    plt.close()    
    
    ## plot corr term
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, corr_train, label='train Corr')
    if is_val:
        plt.plot(x, corr_vali, label='test Corr')

    plt.legend()
    plt.savefig(join(dirname_,'Corr.png'))
    plt.close()     

    """Correlation terms separately
    """
    ## plot corr term
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, age_train, label='train Corr Age')
    if is_val:
        plt.plot(x, age_vali, label='test Corr Age')

    plt.legend()
    plt.savefig(join(dirname_,'Corr_ages.png'))
    plt.close() 


    ## plot corr term
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, tgt_train, label='train Corr Gfactor')
    if is_val:
        plt.plot(x, tgt_vali, label='test Corr Gfactor')

    plt.legend()
    plt.savefig(join(dirname_,'Corr_gfactor.png'))
    plt.close() 
        
    return

def plot_loss_multi6_encoder(dirname_, history, nrMetric):
    """ Plot loss """

    plot_train = np.array(history["train"])
    is_val = ("validation" in history.keys()) #"test"
    if is_val:
        plot_val = np.array(history["validation"]) #"test"

    if not exists(dirname_):
        makedirs(dirname_)

    # epochs
    n_epochs = int(len(plot_train)/nrMetric) #3

    # X axis
    x = [i for i in range(1, n_epochs+1)]
    
    # loss, 0, 4,8, 12
    loss_train = [j for i, j in enumerate(plot_train) if 0 == i%nrMetric]
    loss_vali  = [j for i, j in enumerate(plot_val) if 0 == i%nrMetric]
    # corr, 3, 7, 11    
    corr_train  = [j for i, j in enumerate(plot_train) if 1 == i%nrMetric]
    corr_vali   = [j for i, j in enumerate(plot_val) if 1 == i%nrMetric]

    age_train  = [j for i, j in enumerate(plot_train) if 2 == i%nrMetric]
    age_vali   = [j for i, j in enumerate(plot_val) if 2 == i%nrMetric]

    sex_train  = [j for i, j in enumerate(plot_train) if 3 == i%nrMetric]
    sex_vali   = [j for i, j in enumerate(plot_val) if 3 == i%nrMetric]

    eduy_train  = [j for i, j in enumerate(plot_train) if 4 == i%nrMetric]
    eduy_vali   = [j for i, j in enumerate(plot_val) if 4 == i%nrMetric]

    tgt_train  = [j for i, j in enumerate(plot_train) if 5 == i%nrMetric]
    tgt_vali   = [j for i, j in enumerate(plot_val) if 5 == i%nrMetric]
    
    ## plot loss
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, loss_train, label='train loss')
    if is_val:
        plt.plot(x, loss_vali, label='test loss')

    plt.legend()
    plt.savefig(join(dirname_,'loss.png'))
    plt.close()    
    
    ## plot corr term
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, corr_train, label='train Corr')
    if is_val:
        plt.plot(x, corr_vali, label='test Corr')

    plt.legend()
    plt.savefig(join(dirname_,'Corr.png'))
    plt.close()     

    """Correlation terms separately
    """
    ## plot corr term
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, age_train, label='train Corr Age')
    if is_val:
        plt.plot(x, age_vali, label='test Corr Age')

    plt.legend()
    plt.savefig(join(dirname_,'Corr_ages.png'))
    plt.close() 

    ## plot corr term
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, sex_train, label='train Corr Sex')
    if is_val:
        plt.plot(x, sex_vali, label='test Corr Sex')

    plt.legend()
    plt.savefig(join(dirname_,'Corr_sex.png'))
    plt.close() 

    ## plot corr term
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, eduy_train, label='train Corr Eduy')
    if is_val:
        plt.plot(x, eduy_vali, label='test Corr Eduy')

    plt.legend()
    plt.savefig(join(dirname_,'Corr_eduy.png'))
    plt.close() 

    ## plot corr term
    fig = plt.figure()
    fig.patch.set_facecolor('white')

    plt.xlabel('epoch')
    plt.plot(x, tgt_train, label='train Corr Gfactor')
    if is_val:
        plt.plot(x, tgt_vali, label='test Corr Gfactor')

    plt.legend()
    plt.savefig(join(dirname_,'Corr_gfactor.png'))
    plt.close() 
        
    return


def imsave(x, rec_x, path, row=2, col=2):
    """ Save the first row*col images """
    # Save dir
    #print(x.size())
    save_dir = dirname(path)
    if not exists(save_dir):
        makedirs(save_dir)

    # To cpu, numpy, uint8 format and (num, h, w, c) shape
    in_imgs = (x.clone().cpu().detach().numpy()[:row*col]*255)\
        .astype(np.uint8).transpose(0, 2, 3, 1)[:, :, :, ::-1]
    out_imgs = (rec_x.clone().cpu().detach().numpy()[:row*col]*255)\
        .astype(np.uint8).transpose(0, 2, 3, 1)[:, :, :, ::-1]
    #print(in_imgs.shape)
    # Reshape
    in_imgs = cv2.vconcat([cv2.hconcat([in_imgs[i+j] for j in range(0, col)])
        for i in range(0, row*col, col)])
    out_imgs = cv2.vconcat([cv2.hconcat([out_imgs[i+j] for j in range(0, col)])
        for i in range(0, row*col, col)])
    
    # Concat
    h, _,  = in_imgs.shape
    margin_shape = (h, 5, )
    margin = np.ones(margin_shape, np.uint8) * 255
    concat_imgs = cv2.hconcat([in_imgs, margin, out_imgs])

    # Save
    cv2.imwrite(path, concat_imgs)

def imsave_inp(x, path, row=1, col=11):
    """ Save the interpolated images """

    # Save dir
    save_dir = dirname(path)
    if not exists(save_dir):
        makedirs(save_dir)

    # To cpu, numpy, uint8 format and (num, h, w, c) shape
    in_imgs = (x.clone().cpu().detach().numpy()[:row*col]*255)\
        .astype(np.uint8).transpose(0, 2, 3, 1)[:, :, :, ::-1]

    # Reshape
    in_imgs = cv2.vconcat([cv2.hconcat([in_imgs[i+j] for j in range(0, col)])
        for i in range(0, row*col, col)])

    # Save
    cv2.imwrite(path, in_imgs)

# for visualization
def save_2Dimg(array, index, path_out):
    # load 3D scan
    # np.savez_compressed(join(save_pred_p, name_t), rec_x=rec_x)
    # shape = (1, 1, 160, 192, 144)
    # img = np.load(path_in)['rec_x'][0, 0, :,:,:]
    img = array[0, 0, :,:,:]
    
    # slice to print
    (x, y, z) = index
    if isinstance(z, int):
        #plt.plot(img[:,:,z])
        #plt.imshow(img[:,:,80],cmap='gray',norm=NoNorm())
        plt.imsave(path_out, img[:,:,z],cmap='gray', vmin=0, vmax=3)
    elif isinstance(y, int):
        plt.imsave(path_out, img[:,y,:],cmap='gray', vmin=0, vmax=3)
    elif isinstance(x, int):
        plt.imsave(path_out, img[x,:,:],cmap='gray', vmin=0, vmax=3)
    else:
        print("To plot a 2D slice in a 3D image, specify only one non-None value")
    # save the specified slice by the index
    # plt.plot(img[*index])
    # plt.savefig(path_out, bbox_inches='tight') #dpi=None, allows .eps format
    # plt.close()
    return 

def imsave_grey(x, rec_x, path, rec_path):
    """ Save the first row*col images """
    # Save dir
    #print(x.size())
    save_dir = dirname(path)
    if not exists(save_dir):
        makedirs(save_dir)

    # To cpu, numpy, uint8 format and (num, h, w, c) shape
    in_imgs  = x.clone().cpu().detach().numpy()[0, 0, :,:]
    out_imgs = rec_x.clone().cpu().detach().numpy()[0, 0, :, :]
    #print(in_imgs.shape)
    plt.imsave(path, in_imgs, cmap='gray', vmin=0, vmax=3)
    plt.imsave(rec_path, out_imgs, cmap='gray', vmin=0, vmax=3)  


class Logger:
    def __init__(self, path):
        self.f = open(path, 'w')

    def __del__(self):
        self.f.close()

    def write(self, text):
        self.f.write(text+"\n")
        self.f.flush()
        print(text)
