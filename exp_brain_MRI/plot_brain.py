import plotly.graph_objects as go
import numpy as np

import torch
from PIL import Image
from scipy.ndimage import zoom

from collections import defaultdict
import pprint


image_start = np.load('3Dimg_1.npy')
image_end = np.load('3Dimg_11.npy')


#-----------------------------------
img = Image.open("diff_1_n.png")
rgbimg = Image.new("RGBA", img.size)
rgbimg.paste(img)

found_colors = defaultdict(int)
for x in range(0, rgbimg.size[0]):
    for y in range(0, rgbimg.size[1]):
        pix_val = rgbimg.getpixel((x, y))
        found_colors[pix_val] += 1 
pprint.pprint(dict(found_colors))

#rgbimg.save('kEwfFs3.png')



r, g, b, a = rgbimg.split()

# Increase Reds
g = g.point(lambda i: i*0.5)

# Decrease Greens
b = b.point(lambda i: i * 1) 
r = r.point(lambda i: i * 0.5)

#mask = b.point(lambda i: i>0)
#g.paste(b)
#r.paste(b)
# Recombine back to RGB image

#--------------------

img = Image.open("diff_1_p.png")
rgbimg = Image.new("RGBA", img.size)
rgbimg.paste(img)

found_colors = defaultdict(int)
for x in range(0, rgbimg.size[0]):
    for y in range(0, rgbimg.size[1]):
        pix_val = rgbimg.getpixel((x, y))
        found_colors[pix_val] += 1 
pprint.pprint(dict(found_colors))

#rgbimg.save('kEwfFs3.png')


rp, gp, bp, ap = rgbimg.split()

# Increase Reds
gp = gp.point(lambda i: i * 0.5)

# Decrease Greens
bp = bp.point(lambda i: i * 0.5) 
rp = rp.point(lambda i: i * 1)

mask = b.point(lambda i: i>0 and 255)

gp.paste(g,None,mask)
rp.paste(r,None,mask)
bp.paste(b,None,mask)

# Recombine back to RGB image
result = Image.merge('RGB', (rp, gp, bp))
result.save('diff1_rgbw.png')


#----------------------------------------------------------------------------

#-----------------------------------
img = Image.open("diff_2_n.png")
rgbimg = Image.new("RGBA", img.size)
rgbimg.paste(img)

found_colors = defaultdict(int)
for x in range(0, rgbimg.size[0]):
    for y in range(0, rgbimg.size[1]):
        pix_val = rgbimg.getpixel((x, y))
        found_colors[pix_val] += 1 
pprint.pprint(dict(found_colors))

#rgbimg.save('kEwfFs3.png')



r, g, b, a = rgbimg.split()

# Increase Reds
g = g.point(lambda i: i*0.5)

# Decrease Greens
b = b.point(lambda i: i * 1) 
r = r.point(lambda i: i * 0.5)

#mask = b.point(lambda i: i>0)
#g.paste(b)
#r.paste(b)
# Recombine back to RGB image

#--------------------

img = Image.open("diff_2_p.png")
rgbimg = Image.new("RGBA", img.size)
rgbimg.paste(img)

found_colors = defaultdict(int)
for x in range(0, rgbimg.size[0]):
    for y in range(0, rgbimg.size[1]):
        pix_val = rgbimg.getpixel((x, y))
        found_colors[pix_val] += 1 
pprint.pprint(dict(found_colors))

#rgbimg.save('kEwfFs3.png')


rp, gp, bp, ap = rgbimg.split()

# Increase Reds
gp = gp.point(lambda i: i * 0.5)

# Decrease Greens
bp = bp.point(lambda i: i * 0.5) 
rp = rp.point(lambda i: i * 1)

mask = b.point(lambda i: i>0 and 255)

gp.paste(g,None,mask)
rp.paste(r,None,mask)
bp.paste(b,None,mask)

# Recombine back to RGB image
result = Image.merge('RGB', (rp, gp, bp))
result.save('diff2_rgbw.png')


#-------------------------------------------------------------------

#-----------------------------------
img = Image.open("diff_3_n.png")
rgbimg = Image.new("RGBA", img.size)
rgbimg.paste(img)

found_colors = defaultdict(int)
for x in range(0, rgbimg.size[0]):
    for y in range(0, rgbimg.size[1]):
        pix_val = rgbimg.getpixel((x, y))
        found_colors[pix_val] += 1 
pprint.pprint(dict(found_colors))

#rgbimg.save('kEwfFs3.png')



r, g, b, a = rgbimg.split()

# Increase Reds
g = g.point(lambda i: i*0.5)

# Decrease Greens
b = b.point(lambda i: i * 1) 
r = r.point(lambda i: i * 0.5)

#mask = b.point(lambda i: i>0)
#g.paste(b)
#r.paste(b)
# Recombine back to RGB image

#--------------------

img = Image.open("diff_3_p.png")
rgbimg = Image.new("RGBA", img.size)
rgbimg.paste(img)

found_colors = defaultdict(int)
for x in range(0, rgbimg.size[0]):
    for y in range(0, rgbimg.size[1]):
        pix_val = rgbimg.getpixel((x, y))
        found_colors[pix_val] += 1 
pprint.pprint(dict(found_colors))

#rgbimg.save('kEwfFs3.png')


rp, gp, bp, ap = rgbimg.split()

# Increase Reds
gp = gp.point(lambda i: i * 0.5)

# Decrease Greens
bp = bp.point(lambda i: i * 0.5) 
rp = rp.point(lambda i: i * 1)

mask = b.point(lambda i: i>0 and 255)

gp.paste(g,None,mask)
rp.paste(r,None,mask)
bp.paste(b,None,mask)

# Recombine back to RGB image
result = Image.merge('RGB', (rp, gp, bp))
result.save('diff3_rgbw.png')



exit(0)
##--------------------------------------------
#print(image_end.shape)
##exit(0)
image =   image_end-image_start  
image = np.squeeze(image,0)
image = np.transpose(image,(1,2,0))
print(image.shape)




#exit(0)
#-------------------mask-----------------
label_id_names = np.loadtxt('/home/lau/codes/mesh_VAE/vae_3d_brain_BL/Hammers_mith_atlases_structure_names_r83_structured.txt', dtype=str)
id_name_mask_dict = np.load('/home/lau/codes/mesh_VAE/vae_3d_brain_BL/Hammers_RSS160_id_name_mask.npz', allow_pickle=True)['id_name_mask']
#print(id_name_mask_dict)
id_name_mask_dict = id_name_mask_dict.item()
count_roi = 0
label_roi = np.zeros([len(label_id_names)])
image_all = image - image
for region in label_id_names:
    #if count_roi == 1:
    #    continue
    key  = str(region[0])
    name = region[1]
    mask = id_name_mask_dict[key][1]
    N_non = np.count_nonzero(mask)
    #print(N_non)
    #exit(0)
    #print(mask.shape)
    img_mask = image*mask
    #img_mask = np.ones([160,192,144])*mask
    image_all = image_all + mask *np.sum(np.abs(img_mask))/N_non
    #print(np.sum(np.abs(img_mask)))
    #exit(0)
    label_roi[count_roi] = np.sum(np.abs(img_mask))/N_non
    count_roi = count_roi + 1
    #break
print(label_roi)
#exit(0)
##-----
image = image_all

image_2d = image_all[:,:,70]
max_img = np.max(image_all)
min_img = np.min(image_all)
print(max_img, min_img)
image_2d = (image_2d -  min_img) / (max_img - min_img) * 255
image_2d = Image.fromarray(image_2d.astype(np.uint8))
image_2d.save('test_3.png')
exit(0)

image = zoom(image, (0.5, 0.5, 0.5))



print(image.shape)
X, Y, Z = np.mgrid[-10:10:80j, -12:12:96j, -9:9:72j]
#X, Y, Z = np.mgrid[-9:9:36j, -10:10:38j, -12:12:40j]
#X, Y, Z = np.mgrid[-9:9:72j, -10:10:80j, -12:12:96j]

print(X.flatten().shape, Y.shape, Z.shape)
values= image

fig = go.Figure(data=go.Volume(
    x=X.flatten(),
    y=Y.flatten(),
    z=Z.flatten(),
    value=values.flatten(),
    #isomin = 0.25,
    opacity=0.1, # needs to be small to see through all surfaces
    surface_count=22, # needs to be a large number for good volume rendering
    colorscale='Gray'
    ))
fig.show()
fig.write_image("fig_11.png")
