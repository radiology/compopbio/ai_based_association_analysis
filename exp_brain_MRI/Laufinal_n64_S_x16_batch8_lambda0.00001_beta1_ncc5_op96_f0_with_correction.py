"""
- For brain applications of the high-resolution autoencoder
supervised training; 
final setting
clean folds

author: BL; Sep 27, 2022
"""

# import os
from os.path import join, exists
from os import makedirs
import sys
#print("Experiment of fold %d/5"%int(sys.argv[1]))
print("Experiment of fold %d/5"%int(0))
import argparse
import numpy as np
import pandas as pd
import torch
import torch.optim as optim
# import torch.nn as nn
# import math
import torch.nn.functional as F
sys.path.append(".")
# from utils.data_GPUcluster import get_celeba_loaders
from data_GPUcluster_lau import  data_loaders_rssCognition_labeled, data_loaders_rssCognition_unlabeled
from vis import plot_loss_multi8, Logger, imsave #, plot_loss_multi #plot_loss,, , plot_loss_multi6_encoder
from loss_lau import ncc_loss, torch_corr #FLPLoss,KLDLoss,  
##TODO try linear upsampling
#VAE_3d_convTrans_x16_zp,
from simple_vae_3D_convTranse_linearUp_lau import VAE_3d_convTrans_x16, VAE_3d_linearUp_x16, VAE_3d_convTrans_x16_zp_semi, Encoder_3d_convTrans_x16
from simple_vae_3D_convTranse_linearUp_lau import VAE_3d_convTrans_x16_zp, VAE_3d_convTrans
# from Load_data_jy import get_list_label


# 5-folds cross-validation; select 0-4 int
# fold = sys.argv[1]
# fold = int(fold)
## TODO, run the rest folds
fold = int(0)

parser = argparse.ArgumentParser(description='vae.pytorch')
# parser.add_argument('--fold', type=int, default=None,
#                     help='5-folds cross-validation; select 0-4')
# parser.add_argument('--logdir', type=str, 
#                     default="./log/RSS_cognition_beta0.4/fold_%s_semisupervised"%fold)
parser.add_argument('--batch_train', type=int, default=8) #8
parser.add_argument('--batch_vali', type=int, default=1) # 16 memory oom, 1 to loop over all samples
parser.add_argument('--batch_test', type=int, default=1) # 
parser.add_argument('--epochs', type=int, default=300)
parser.add_argument('--gpu', type=str, default="0")
parser.add_argument('--initial_lr', type=float, default=0.0005)
parser.add_argument('--grad_norm', type=float, default=1.0)
parser.add_argument('--lr_decay_exp', type=float, default=0.96)

parser.add_argument('--alpha', type=float, default=0.001,
                    help='hyperparameter for KLD term; the one for NCC is 1 by default') # 
parser.add_argument('--beta', type=float, default=1,
                    help='hyperparameter for intensity reconstruction term') #1.0, for intensity sim
parser.add_argument('--lambda_par', type=float, default=0.00001,
                    help='hyperparameter for correlation term')
# parser.add_argument('--model', type=str, default="pvae", choices=["vae-123", "vae-345", "pvae"])

## for semi setting, the model has to output zp along with rec_x
parser.add_argument('--model', type=str, default="convTrans", choices=["semi", "linearUP", "convTrans", "encoder", "convTrans_zp"]) #"convTrans"
parser.add_argument('--nz', type=int, default=64,
                    help='size of latent feature')
parser.add_argument('--nef', type=int, default=8,
                    help='nr of conv kernels')
parser.add_argument('--variational', type=bool, default=False)
parser.add_argument('--isize', type=list, default=[160,192,144],
                    help='input size')

parser.add_argument('--prepro', type=str, default="mod_GM", choices=["mod_GM", "mod_GM_s1"],
                    help='preprocess strategy to use, GM or GM_s1')

# parser.add_argument('--nr_test', type=int, default=1000, #128
#                     help='nr of images for validation')

args = parser.parse_args()

plot_slice = True

""" only if on local workstation and there are multiple GPUs installed.
# Set GPU (Single GPU usage is only supported so far)
# os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu
"""

# Device
torch.set_num_threads(1)
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
# print(torch.cuda.current_device())

# Dataloader #--------- added Oct 12
"""local
p = r'F:\Representation Learning\subject demography\CognitionRSS'
trainLabeled_p   = join(p, 'gFactor_5folds','TrainingFolds_scansWithGfactor.npy') 
trainUnlabeled_p = join(p, 'gFactor_5folds', 'TrainingFolds_VBMwithoutGfactor.npy')
testLabeled_p    = join(p, 'gFactor_5folds', 'TestFolds_scansWithGfactor.npy')
testUnlabeled_p  = join(p, 'gFactor_5folds', 'TestFolds_VBMwithoutGfactor.npy')

df_p   = join(p, 'Gfactor_age_sex_eduyear.csv')
"""

"""GPU cluster"""
p = '/trinity/home/bli/Representation_learning/Subject_list/rs_cognition/'
# list_name = '/Total_RS+ADNI_fair_split_QC_%d'%int(fold)

GM_p   = '/data/scratch/bli/Representation_learning/data_3d_VBM_crop_160_192_144_GM'
GM_s_p = '/data/scratch/bli/Representation_learning/data_3d_VBM_crop_160_192_144_GM_s3'

if "mod_GM" == args.prepro:
    data_p = GM_p
    name_exd = '_GM.nii.gz'
elif "mod_GM_s1" == args.prepro:
    data_p = GM_s_p
    name_exd = '_GM_s1.nii.gz'


""" semi-supervised train, select one over five folds
"""
all_label = pd.read_csv(join(p, 'Gfactor_age_sex_eduyear.csv'))
all_label = all_label.set_index('bigrfullname')

trainLabeled_MRI   = np.load(join(p, 'TrainingFolds_scansWithGfactor_v2.npy'))[fold, 0, :]#[:1600] #n=1916
trainUnlabeled_MRI = np.load(join(p, 'TrainingFolds_VBMwithoutGfactor.npy'), allow_pickle=True)[fold] #n=7538
# print('training samples: {}, {}'.format(len(trainLabeled_MRI), len(trainUnlabeled_MRI)))
# int, 0 or >0


testLabeled_MRI   = np.load(join(p, 'TestFolds_scansWithGfactor_v2.npy'))[fold, 0, :]#[:400] #n=479
testUnlabeled_MRI = np.load(join(p, 'TestFolds_VBMwithoutGfactor.npy'), allow_pickle=True)[fold][:400] #n=1868
# print('validation samples: {}, {}'.format(len(testLabeled_MRI), len(testUnlabeled_MRI)))

dataloaders_labeled = data_loaders_rssCognition_labeled(args.batch_train, args.batch_vali, args.batch_test, data_p, name_exd, 
                     all_label, 
                     trainLabeled_MRI, testLabeled_MRI)
 
dataloaders_unlabeled = data_loaders_rssCognition_unlabeled(args.batch_train, args.batch_vali, args.batch_test, data_p, name_exd, 
                      all_label, 
                      trainUnlabeled_MRI, testUnlabeled_MRI)

## TODO iterate per epoch, not only one iteration over all epochs
# dataloaders_unlabeled_train = iter(dataloaders_unlabeled["train"])
# dataloaders_unlabeled_vali  = iter(dataloaders_unlabeled["validation"])

# Model
if args.model == "convTrans":
    model = VAE_3d_convTrans_x16(device=device, ndf=args.nef, nef=args.nef, nz=args.nz, 
                             isize=args.isize, variational=args.variational).to(device)
elif args.model == "linearUP":
    model = VAE_3d_linearUp_x16(device=device, ndf=args.nef, nef=args.nef, nz=args.nz, 
                            isize=args.isize, variational=args.variational).to(device)
elif args.model == "semi":
    # model = VAE_3d_convTrans_x16_zp(device=device, ndf=args.nef, nef=args.nef, nz=args.nz, 
    #                          isize=args.isize, variational=args.variational).to(device)
    model = VAE_3d_convTrans_x16_zp_semi(device=device, ndf=args.nef, nef=args.nef, nz=args.nz, 
                             isize=args.isize, variational=args.variational).to(device)
elif args.model == "encoder":
    model = Encoder_3d_convTrans_x16(device=device, ndf=args.nef, nef=args.nef, nz=args.nz, 
                             isize=args.isize, variational=args.variational).to(device)
elif args.model == "convTrans_zp":
    model = VAE_3d_convTrans_x16_zp(device=device, ndf=args.nef, nef=args.nef, nz=args.nz, 
                             isize=args.isize, variational=args.variational).to(device)    

# -----------------------------

# Reconstruction loss
# if args.model == "pvae":
#     reconst_criterion = nn.MSELoss(reduction='sum')
# elif args.model == "vae-123" or args.model == "vae-345":
#     reconst_criterion = FLPLoss(args.model, device, reduction='sum')
# KLD loss
# kld_criterion = KLDLoss(reduction='sum')

"""change by Bo, 1:5 -> 0.2:1"""
#loss =   args.beta * reconst_loss +5*ncc_loss(x, rec_x)

# Solver
optimizer = optim.Adam(model.parameters(), lr=args.initial_lr)
# Scheduler
##TODO optimize lr_scheduler: try Adam only, exp
## torch.optim.lr_scheduler.ReduceLROnPlateau: dynamic lr
## https://pytorch.org/docs/stable/optim.html
#scheduler = optim.lr_scheduler.StepLR(optimizer, 1, gamma=0.99)
scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=args.lr_decay_exp, 
                                             last_epoch=-1, verbose=True)
# Log
# logdir = "./log/RSS-ADNI_convTrans_GM_VAEx16_beta0.4/Unsupervised_allData"
logdir = "/trinity/home/xliu1/RSS_cognition_beta0.4/fold_%s_supervisedAE_all"%fold
if not exists(logdir):
    makedirs(logdir)

# Logger
logger = Logger(join(logdir, "log.txt"))
# History
history = {"train": [], "validation": []}

# Save config
logger.write('----- Options ------')
for k, v in sorted(vars(args).items()):
    logger.write('%s: %s' % (str(k), str(v)))


best_l1_vali   = 1000.
best_ncc_vali  = 1000.
best_loss_vali = 2000.
best_corr_vali = 2000.

# Start training
dataloaders_unlabeled_train = iter(dataloaders_unlabeled["train"])
for epoch in range(args.epochs):
    
    l1_vali   = []
    ncc_vali  = []
    loss_vali = []
    corr_vali = []
    
    # dataset sampling per epoch
    if epoch % 4 == 0:
   	 dataloaders_unlabeled_train = iter(dataloaders_unlabeled["train"])
    # dataloaders_unlabeled_vali  = iter(dataloaders_unlabeled["validation"])
    
    # TODO: "test" is unseen, not included; only for final modal
    ## TODO: rewrite the program into train and eval function to reduce alive variables inside the loop
    # https://discuss.pytorch.org/t/spliting-my-model-on-4-gpu-and-cuda-out-of-memory-problem/37311/2?u=ptrblck
    # https://discuss.pytorch.org/t/increase-the-cuda-memory-twice-then-stop-increasing/28462
    for phase in ["train", "validation"]:
        if phase == "train":
            model.train(True)
            logger.write(f"\n----- Epoch {epoch+1} -----")
            torch.autograd.set_detect_anomaly(True)
            all1 = args.batch_train            
        else:
            model.train(False)
            model.eval()
            all1 = args.batch_vali

        # for validation, compute correlation population-wise, separately    
        vali_age_run = []
        vali_sex_run = []
        vali_eduy_run= []
        vali_tgtV_run= []
        
        vali_zp_run = []
        
        # for training, compute running correlation
        age_run = []
        sex_run = []
        eduy_run= []
        tgtV_run= []
        
        zp_run = []
            
        # Loss
        running_loss    = 0.0
        running_reconst = 0.0
        running_ncc     = 0.0
        running_corr    = 0.0
        
        running_corr_age       = 0.0
        running_corr_sex       = 0.0
        running_corr_eduy      = 0.0
        running_corr_gfctor    = 0.0
        # Data num
        # data_num = 0

        # Train
        #for i, x in enumerate(dataloaders[phase]):
        ## TODO: two dataloaders, one for labeled, another for unlabeled
        # dataloaders_unlabeled = iter(dataloaders_unlabeled[phase])
        ## if skip var=0, i=-1; else, i=0
        i = 0  # 0
        for d, data in enumerate(dataloaders_labeled[phase]):
            #print(data.size())
            x      = data[0]
            #print(x.size(0),args.batch_train)
            age_t  = data[1]
            sex_t  = data[2]
            eduy_t = data[3]
            tgtv_t = data[4]
            
            # print(f"age_t, sex_t, eduy_t, tgtv_: {age_t:.4f}, {sex_t:d}, {eduy_t:.4f}, {tgtv_t:.4f}")
            # print("age_t, sex_t, eduy_t, tgtv_:", age_t, sex_t, eduy_t, tgtv_t)
            age_t  = age_t.to(device).float()#.squeeze(1)
            sex_t  = sex_t.to(device).float()#.squeeze(1)
            eduy_t = eduy_t.to(device).float()#.squeeze(1)
            tgtv_t = tgtv_t.to(device).float()#.squeeze(1)          
            
            # # prevent var=nan; count and average over the valid nr of batches            
            # if torch.tensor(0, device=device)==torch.sum(sex_t) or torch.tensor(all1, device=device)==torch.sum(sex_t):
            #     continue
            # else:
            #     i = i + 1
                
            #exit(0)
            # Optimize params

            if phase == "train":
                optimizer.zero_grad()
                if len(age_t) != args.batch_train: # change BL, 64
                    continue                
                
                # semi--------------------------------------------
                #data_semi = dataloaders_unlabeled_train.next()
                #x_semi    = data_semi[0].to(device) 
                #rec_x_semi, _ = model(x_semi)
                #recon_unlabeled  = F.smooth_l1_loss(rec_x_semi, x_semi, reduction='mean', beta=1.0)
                #ncc_unlabeled  = ncc_loss(x_semi, rec_x_semi, device)
                #loss_semi =  args.beta * recon_unlabeled +  5* ncc_unlabeled   
                #loss_semi.backward(retain_graph=False)
                #torch.nn.utils.clip_grad_norm_(model.parameters(), args.grad_norm) #enable for gf only; disable for all?
                #optimizer.step()
              
               # end semi-----------------------------------------              



                #optimizer.zero_grad()
                # Pass forward --------------------------------------labeled
                x = x.to(device)               
                
                rec_x, zp = model(x)
                zp = zp.clone().squeeze(1)
                
                # Pass backward ----------------------------------- labeled
                if 0==i:
                    train_zp_run   = zp.clone().detach()
                    train_age_run  = age_t.clone()
                    train_sex_run  = sex_t.clone()
                    train_eduy_run = eduy_t.clone()
                    train_tgtV_run = tgtv_t.clone()                  
                elif i>0:   
                    train_age_run  = torch.cat((train_age_run, age_t), 0)
                    train_sex_run  = torch.cat((train_sex_run, sex_t), 0)
                    train_eduy_run = torch.cat((train_eduy_run, eduy_t), 0)
                    train_tgtV_run = torch.cat((train_tgtV_run, tgtv_t), 0)                        
                    train_zp_run   = torch.cat((train_zp_run, zp.clone().detach()), 0)

                
                # Calculate loss
                """Correlation loss / batch_size"""
                ## TODO: weights between terms |corr(bias)| -  0.5*|corr(target)|
                corr_loss_age    = torch_corr(zp, age_t, device)
                corr_loss_sex    = torch_corr(zp, sex_t, device)
                corr_loss_eduy   = torch_corr(zp, eduy_t, device)
                corr_loss_tgtv   = torch_corr(zp, tgtv_t, device)                                
                corr_loss        = 2*(corr_loss_age + corr_loss_sex + corr_loss_eduy) - corr_loss_tgtv
                #corr_loss        = - corr_loss_tgtv

                # corr_loss = 2*torch_corr(zp, age_t, device) + 2*torch_corr(zp, sex_t, device) + 2*torch_corr(zp, eduy_t, device) - torch_corr(zp, tgtv_t, device)
                # print("Training corr age, sex, eduy, gfactor: %.4f, %.4f, %.4f, %.4f"%(corr_loss_age_.clone().cpu().detach().numpy(),
                #                                                                         corr_loss_sex_.clone().cpu().detach().numpy(),
                #                                                                         corr_loss_eduy_.clone().cpu().detach().numpy(),
                #                                                                         corr_loss_tgtv_.clone().cpu().detach().numpy()))                
                """Smooth L1 / 2*batch_size"""
                recon_labeled  = F.l1_loss(rec_x, x, reduction='mean')
                # print("Training L1 loss labeled: %f"%(recon_labeled.clone().cpu().detach().numpy()))
              
                                
                """NCC / 2*batch_size"""

                ncc_labeled  = ncc_loss(x, rec_x, device)

                                
                """Total loss"""                
                loss =  args.beta * recon_labeled + 5.0 * ncc_labeled + args.lambda_par * corr_loss
                ## TODO optimize weight for momentum
                # loss = corr_loss_run       #0.05* corr_loss  +    
                loss.backward(retain_graph=False)
                torch.nn.utils.clip_grad_norm_(model.parameters(), args.grad_norm)
                optimizer.step()

                # Visualize
                if plot_slice:
                #TODO why 64, is this the batch size?
                    if i == 0 and x.size(0) >= args.batch_train:
                       # x = x.view([args.batch_train, 160,192,144,1])
                        #rec_x = rec_x.view([args.batch_train, 160,192,144,1])
                        imsave(x[:,:,:,:,80], rec_x[:,:,:,:,80], 
                               join(logdir, f"epoch{epoch+1}", f"train-{i}.png"), 2, 2)
    
            elif phase == "validation":
                with torch.no_grad():
                    optimizer.zero_grad()

                    # Pass forward --------------------------------------labeled
                    ##TODO torch.cat(tensors, dim=0)
                    x = x.to(device)
    
                    rec_x, zp = model(x)
                    zp = zp.clone().squeeze(1)
    
                    # Calc loss
                    """Correlation loss / batch_size"""
                    if 0==i:
                        vali_zp_run   = zp.clone().detach()
                        vali_age_run  = age_t.clone()
                        vali_sex_run  = sex_t.clone()
                        vali_eduy_run = eduy_t.clone()
                        vali_tgtV_run = tgtv_t.clone()                  
                    elif i>0:   
                        vali_age_run  = torch.cat((vali_age_run, age_t), 0)
                        vali_sex_run  = torch.cat((vali_sex_run, sex_t), 0)
                        vali_eduy_run = torch.cat((vali_eduy_run, eduy_t), 0)
                        vali_tgtV_run = torch.cat((vali_tgtV_run, tgtv_t), 0)                        
                        vali_zp_run   = torch.cat((vali_zp_run, zp.clone().detach()), 0)

                    """Correlation loss / batch_size"""
                    corr_loss_age    = 0 #torch_corr(zp, age_t, device)
                    corr_loss_sex    = 0 #torch_corr(zp, sex_t, device)
                    corr_loss_eduy   = 0 #torch_corr(zp, eduy_t, device)
                    corr_loss_tgtv   = 0 #torch_corr(zp, tgtv_t, device)                                
                    #corr_loss        = 4*corr_loss_age + 2*corr_loss_sex + 2*corr_loss_eduy - corr_loss_tgtv
                    corr_loss        =  - corr_loss_tgtv
                        
                    # corr_loss = 2*torch_corr(zp, age_t, device) + 2*torch_corr(zp, sex_t, device) + 2*torch_corr(zp, eduy_t, device) - torch_corr(zp, tgtv_t, device)
                    
                    # corr_vali.append(corr_loss.clone().cpu().detach().numpy())

                    """L1 norm"""
                    recon_labeled  = F.l1_loss(rec_x, x, reduction='mean')
                    
                    l1_vali.append(recon_labeled.clone().cpu().detach().numpy())
                    
                    #kld_loss = kld_criterion(mean, logvar)
                    
                    """NCC"""
                    ncc_labeled  = ncc_loss(x, rec_x, device)
                    
                    ncc_vali.append(ncc_labeled.clone().cpu().detach().numpy())
                                                        
                    """save the best"""
                    loss =  args.beta * recon_labeled + 5*ncc_labeled #+ corr_loss #+ torch.tensor(corr_loss, device=device)
                    # loss =  torch.tensor(corr_loss, device=device)                    
                    loss_vali.append(loss.clone().cpu().detach().numpy())                     #.clone().cpu().detach().numpy()
                    
                    if plot_slice:
                        if x.size(0) >= args.batch_test:
                            imsave(x[:,:,:,:,80], rec_x[:,:,:,:,80], 
                                   join(logdir, f"epoch{epoch+1}", f"test-{i}.png"), 1, 1)
                            
            i = i + 1
            
            # Add stats
            # .item() converts it to float datatype
            running_loss += loss.item() # * x.size(0)
            
            """save additional metrics"""
            running_reconst += recon_labeled.item()
            running_ncc     += ncc_labeled.item()
            
            #if phase == "train":   
            #    running_corr        += corr_loss.item()                
            #    running_corr_age    += corr_loss_age.item()
            #    running_corr_eduy   += corr_loss_eduy.item()
            #    running_corr_sex    += corr_loss_sex.item()
            #    running_corr_gfctor += corr_loss_tgtv.item()
                
            ## TODO average over the nr of batch, rather than the nr of data
            # data_num += x.size(0)
            #print(data_num)

        # Log
        epoch_rec  = running_reconst / i #data_num
        epoch_ncc  = running_ncc / i #data_num
        
        if phase == "train":
            epoch_loss         = running_loss / i #data_num
            
            epoch_corr_age     = torch_corr(train_zp_run.detach(), train_age_run, device).item()
            epoch_corr_sex     = torch_corr(train_zp_run.detach(), train_sex_run, device).item()
            epoch_corr_eduy    = torch_corr(train_zp_run.detach(), train_eduy_run, device).item()
            epoch_corr_gfactor = torch_corr(train_zp_run.detach(), train_tgtV_run, device).item()                             
            epoch_corr         = -epoch_corr_gfactor                         
        else:
            # validation correlation dataset-wise
            epoch_corr_age     = torch_corr(vali_zp_run.detach(), vali_age_run, device).item()
            epoch_corr_sex     = torch_corr(vali_zp_run.detach(), vali_sex_run, device).item()
            epoch_corr_eduy    = torch_corr(vali_zp_run.detach(), vali_eduy_run, device).item()
            epoch_corr_gfactor = torch_corr(vali_zp_run.detach(), vali_tgtV_run, device).item()                             
            epoch_corr         = -epoch_corr_gfactor
            #epoch_corr         = - epoch_corr_gfactor
            ##TODO correlation loss was not added during validation, correct it here
            epoch_loss         =  args.lambda_par*epoch_corr + (running_loss / i)
        
        logger.write(f"{phase} Loss : {epoch_loss:.4f}")
        logger.write(f"{phase} Reconstruction loss : {epoch_rec:.4f}")
        logger.write(f"{phase} NCC loss : {epoch_ncc:.4f}")
        logger.write(f"{phase} Correlation loss : {epoch_corr:.4f}")

        logger.write(f"{phase} Correlation Age : {epoch_corr_age:.4f}")
        logger.write(f"{phase} Correlation Sex : {epoch_corr_sex:.4f}")
        logger.write(f"{phase} Correlation Eduy : {epoch_corr_eduy:.4f}")
        logger.write(f"{phase} Correlation Gfactor : {epoch_corr_gfactor:.4f}")        
        # history[phase].append(epoch_loss)

        """on GPU cluster"""
        # "can't convert cuda:0 device type tensor to numpy"
        # history[phase].append(epoch_loss.clone().cpu().detach().numpy())
        # history[phase].append(epoch_rec.clone().cpu().detach().numpy())
        # history[phase].append(epoch_ncc.clone().cpu().detach().numpy())
        # history[phase].append(epoch_corr.clone().cpu().detach().numpy())

        history[phase].append(epoch_loss)
        history[phase].append(epoch_rec)
        history[phase].append(epoch_ncc)        
        history[phase].append(epoch_corr)

        history[phase].append(epoch_corr_age)
        history[phase].append(epoch_corr_sex)
        history[phase].append(epoch_corr_eduy)
        history[phase].append(epoch_corr_gfactor)        

        if phase == "validation":
            ## TODO modify the plot based on the nr of metrics, nr epoch = len(history)/ nr_metrics
            # plot_loss_multi(logdir, history)
            # plot_loss_multi4(logdir, history, int(4))
            plot_loss_multi8(logdir, history, int(8))
            # plot_loss_multi6_encoder(logdir, history, int(6))
            
    """lr decay"""
    scheduler.step()
    
    """save the epoch of the best performance"""   
    
   # if np.nanmean(l1_vali) < best_l1_vali:
   #     best_l1_vali = np.nanmean(l1_vali)
   #     torch.save(model.state_dict(), join(logdir, "best_L1_validation.pth")) #validation-{epoch+1}

    #if np.nanmean(ncc_vali) < best_ncc_vali:
    #    best_ncc_vali = np.nanmean(ncc_vali)
    #    torch.save(model.state_dict(), join(logdir, "best_ncc_validation.pth"))    
    
   # if np.nanmean(loss_vali) + args.lambda_par*epoch_corr < best_loss_vali:
   #     best_loss_vali = np.nanmean(loss_vali)+ args.lambda_par*epoch_corr
   #     torch.save(model.state_dict(), join(logdir, "best_loss_validation.pth"))

    #if np.nanmean(epoch_corr) < best_corr_vali:
    #    best_corr_vali = np.nanmean(corr_vali)
    #    torch.save(model.state_dict(), join(logdir, "best_corr_validation.pth"))
        
    """Save the latest model after each epoch"""
    torch.save(model.state_dict(),\
        join(logdir, 'final_model.pth'))
    ## TODO save the model of the best metrics
    np.save(join(logdir, 'history'), history, allow_pickle=True)
    
