
import os
from os.path import join, exists
from os import makedirs
import sys
#print("Experiment of fold %d/5"%int(sys.argv[1]))
print("Experiment of fold %d/5"%int(0))
import argparse
import numpy as np
import pandas as pd
import torch
from PIL import Image
import torch.optim as optim
# import torch.nn as nn
# import math
import torch.nn.functional as F
sys.path.append(".")
# from utils.data_GPUcluster import get_celeba_loaders
from data_GPUcluster import  data_loaders_rssCognition_labeled #data_loaders_rssCognition_unlabeled,
from vis import plot_loss_multi8, Logger, imsave #, plot_loss_multi #plot_loss,, , plot_loss_multi6_encoder
##TODO try linear upsampling
#VAE_3d_convTrans_x16_zp,
from simple_vae_3D_convTranse_linearUp_lau import VAE_3d_convTrans_x16, VAE_3d_linearUp_x16, VAE_3d_convTrans_x16_zp_semi, Encoder_3d_convTrans_x16
from simple_vae_3D_convTranse_linearUp_lau import VAE_3d_convTrans_x16_zp, VAE_3d_convTrans
# from Load_data_jy import get_list_label

from sklearn.feature_selection import mutual_info_regression
import dcor

beta = 0.4
# 5-folds cross-validation; select 0-4 int
# fold = sys.argv[1]
# fold = int(fold)
fold = int(0)

parser = argparse.ArgumentParser(description='vae.pytorch')
# parser.add_argument('--fold', type=int, default=None,
#                     help='5-folds cross-validation; select 0-4')
# parser.add_argument('--logdir', type=str, 
#                     default="./log/RSS_cognition_beta0.4/fold_%s_semisupervised"%fold)
parser.add_argument('--batch_train', type=int, default=1) #8
parser.add_argument('--batch_vali', type=int, default=1) # 16 memory oom, 1 to loop over all samples
parser.add_argument('--batch_test', type=int, default=1) # 
parser.add_argument('--epochs', type=int, default=120)
parser.add_argument('--gpu', type=str, default="0")
parser.add_argument('--initial_lr', type=float, default=0.0001)
parser.add_argument('--grad_norm', type=float, default=1.0)
parser.add_argument('--lr_decay_exp', type=float, default=0.9)

parser.add_argument('--alpha', type=float, default=0.001,
                    help='hyperparameter for KLD term; the one for NCC is 1 by default') # 
parser.add_argument('--beta', type=float, default=12,
                    help='hyperparameter for intensity reconstruction term') #1.0, for intensity sim
parser.add_argument('--gamma', type=float, default=0.0005,
                    help='hyperparameter for correlation term')
# parser.add_argument('--model', type=str, default="pvae", choices=["vae-123", "vae-345", "pvae"])

## for semi setting, the model has to output zp along with rec_x
parser.add_argument('--model', type=str, default="convTrans", choices=["semi", "linearUP", "convTrans", "encoder", "convTrans_zp"]) #"convTrans"
parser.add_argument('--nz', type=int, default=64,
                    help='size of latent feature')
parser.add_argument('--nef', type=int, default=8,
                    help='nr of conv kernels')
parser.add_argument('--variational', type=bool, default=False)
parser.add_argument('--isize', type=list, default=[160,192,144],
                    help='input size')

parser.add_argument('--prepro', type=str, default="mod_GM", choices=["mod_GM", "mod_GM_s1"],
                    help='preprocess strategy to use, GM or GM_s1')

# parser.add_argument('--nr_test', type=int, default=1000, #128
#                     help='nr of images for validation')

args = parser.parse_args()

plot_slice = True

""" only if on local workstation and there are multiple GPUs installed.
# Set GPU (Single GPU usage is only supported so far)
# os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu
"""

# Device
torch.set_num_threads(1)
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
#device = torch.device("cpu")
# print(torch.cuda.current_device())

# Dataloader #--------- added Oct 12
"""local
p = r'F:\Representation Learning\subject demography\CognitionRSS'
trainLabeled_p   = join(p, 'gFactor_5folds','TrainingFolds_scansWithGfactor.npy') 
trainUnlabeled_p = join(p, 'gFactor_5folds', 'TrainingFolds_VBMwithoutGfactor.npy')
testLabeled_p    = join(p, 'gFactor_5folds', 'TestFolds_scansWithGfactor.npy')
testUnlabeled_p  = join(p, 'gFactor_5folds', 'TestFolds_VBMwithoutGfactor.npy')

df_p   = join(p, 'Gfactor_age_sex_eduyear.csv')
"""

"""GPU cluster"""
p = '/home/lau/codes/mesh_VAE/vae_brain_cluster/rs_cognition/'
# list_name = '/Total_RS+ADNI_fair_split_QC_%d'%int(fold)

GM_p   = '/media/lau/HDD_storage/data_brain_cluster/data_3d_VBM_crop_160_192_144_GM'
GM_s_p = 'null'

if "mod_GM" == args.prepro:
    data_p = GM_p
    name_exd = '_GM.nii.gz'
elif "mod_GM_s1" == args.prepro:
    data_p = GM_s_p
    name_exd = '_GM_s1.nii.gz'


""" semi-supervised train, select one over five folds
"""
all_label = pd.read_csv(join(p, 'Gfactor_age_sex_eduyear.csv'))
all_label = all_label.set_index('bigrfullname')

trainLabeled_MRI   = np.load(join(p, 'TrainingFolds_scansWithGfactor_v2.npy'))[fold, 0, :] #n=1916
trainUnlabeled_MRI = np.load(join(p, 'TrainingFolds_VBMwithoutGfactor.npy'), allow_pickle=True)[fold] #n=7538
print('training samples: {}, {}'.format(len(trainLabeled_MRI), len(trainUnlabeled_MRI)))
# int, 0 or >0
run_cap = 400


testLabeled_MRI   = np.load(join(p, 'TestFolds_scansWithGfactor_v2.npy'))[fold, 0, :] #n=479
testUnlabeled_MRI = np.load(join(p, 'TestFolds_VBMwithoutGfactor.npy'), allow_pickle=True)[fold] #n=1868
print('validation samples: {}, {}'.format(len(testLabeled_MRI), len(testUnlabeled_MRI)))

dataloaders_labeled = data_loaders_rssCognition_labeled(args.batch_train, args.batch_vali, args.batch_test, data_p, name_exd, 
                     all_label, 
                     trainLabeled_MRI, testLabeled_MRI)
 
# dataloaders_unlabeled = data_loaders_rssCognition_unlabeled(args.batch_train, args.batch_vali, args.batch_test, data_p, name_exd, 
#                      all_label, 
#                      trainUnlabeled_MRI, testUnlabeled_MRI)

## TODO iterate per epoch, not only one iteration over all epochs
# dataloaders_unlabeled_train = iter(dataloaders_unlabeled["train"])
# dataloaders_unlabeled_vali  = iter(dataloaders_unlabeled["validation"])

# Model
if args.model == "convTrans":
    model = VAE_3d_convTrans_x16(device=device, ndf=args.nef, nef=args.nef, nz=args.nz, 
                             isize=args.isize, variational=args.variational,is_train=False).to(device)
elif args.model == "linearUP":
    model = VAE_3d_linearUp_x16(device=device, ndf=args.nef, nef=args.nef, nz=args.nz, 
                            isize=args.isize, variational=args.variational).to(device)
elif args.model == "semi":
    # model = VAE_3d_convTrans_x16_zp(device=device, ndf=args.nef, nef=args.nef, nz=args.nz, 
    #                          isize=args.isize, variational=args.variational).to(device)
    model = VAE_3d_convTrans_x16_zp_semi(device=device, ndf=args.nef, nef=args.nef, nz=args.nz, 
                             isize=args.isize, variational=args.variational).to(device)
elif args.model == "encoder":
    model = Encoder_3d_convTrans_x16(device=device, ndf=args.nef, nef=args.nef, nz=args.nz, 
                             isize=args.isize, variational=args.variational).to(device)
elif args.model == "convTrans_zp":
    model = VAE_3d_convTrans_x16_zp(device=device, ndf=args.nef, nef=args.nef, nz=args.nz, 
                             isize=args.isize, variational=args.variational).to(device)    

try:
    model.load_state_dict(torch.load('path_to_the_trained_model'+str(fold)+'_all.pth'))
    #model.load_state_dict(torch.load('/home/lau/codes/mesh_VAE/vae_brain_cluster/log/RSS_cognition_beta0.4/fold_0_supervisedAE_tgt_batch8_gamma0.25_beta2.5_ncc2.5/final_model.pth'))
except:
    print("Invalid weight path.")
# -----------------------------


# logdir = "./log/RSS-ADNI_convTrans_GM_VAEx16_beta0.4/Unsupervised_allData"
logdir = "./log/RSS_cognition_beta0.4/fold_%s_supervisedAE_tgt_batch8_gamma0.25_beta2.5_ncc2.5"%fold
if not exists(logdir):
    makedirs(logdir)

# Logger
logger = Logger(join(logdir, "log.txt"))
# History
history = {"train": [], "validation": []}

# Save config
logger.write('----- Options ------')
for k, v in sorted(vars(args).items()):
    logger.write('%s: %s' % (str(k), str(v)))


def torch_corr(input1, input2, device):

    mean_1 = torch.mean(input1)
    mean_2 = torch.mean(input2)
    var_1 = torch.var(input1)
    var_2 = torch.var(input2)
    vector_mean1 = mean_1*torch.ones([len(input1)]).to(device)
    vector_mean2 = mean_2*torch.ones([len(input1)]).to(device)

    diff_1 = input1 - vector_mean1
    diff_2 = input2 - vector_mean2
    exp = torch.mul(diff_1,diff_2)
    exp = torch.sum(exp)   

    #exp = 0
    #for i in range(len(input1)):
    #    exp = exp + (input1[i]-mean_1)*(input2[i]-mean_2)  
    #print(exp)
    #exit(0)
    exp = exp / len(input1)
    exp = exp / torch.sqrt(var_1*var_2)
    return exp

with torch.no_grad():
    #model.eval()
    model.train(False)
    print(model.pe.weight)
    #exit(0)
    z_list, zp_list, age_list, gender_list,edu_list, gf_list = [], [], [], [],[],[]
    for data in dataloaders_labeled["validation"]:
        x      = data[0]
        #print(x.size(0),args.batch_train)
        age_t  = data[1]
        sex_t  = data[2]
        eduy_t = data[3]
        tgtv_t = data[4]

        x = x.to(device)
        age_t  = age_t.to(device).float()#.squeeze(1)
        sex_t  = sex_t.to(device).float()#.squeeze(1)
        eduy_t = eduy_t.to(device).float()#.squeeze(1)
        tgtv_t = tgtv_t.to(device).float()#.squeeze(1)  

        latent_z = model.encode(x)
        #print(latent_z)
        zp = model.pe(latent_z)

        latent_z = latent_z.squeeze(0)
        zp_list.append(zp.unsqueeze(1))
        age_list.append(age_t.unsqueeze(0))
        gender_list.append(sex_t.unsqueeze(0))
        edu_list.append(eduy_t.unsqueeze(0))
        gf_list.append(tgtv_t.unsqueeze(0))
        z_list.append(latent_z.unsqueeze(0))

    z_list_tensor = torch.cat(z_list).squeeze(0)
    age_list_tensor = torch.cat(age_list).squeeze(1)
    gender_list_tensor = torch.cat(gender_list).squeeze(1)
    edu_list_tensor = torch.cat(edu_list).squeeze(1)
    gf_list_tensor = torch.cat(gf_list).squeeze(1)
    zp_list_tensor = torch.cat(zp_list).squeeze(1)
    zp_list_tensor = zp_list_tensor.squeeze(1)
    age_sqr_list_tensor = torch.square(age_list_tensor)
 
  
    corr_age_sqr = torch_corr(age_sqr_list_tensor, zp_list_tensor, device)
    corr_age = torch_corr(age_list_tensor, zp_list_tensor, device)
    corr_gender = torch_corr(gender_list_tensor, zp_list_tensor, device)
    corr_gf = torch_corr(gf_list_tensor, zp_list_tensor, device)
    corr_edu = torch_corr(edu_list_tensor, zp_list_tensor, device)

    zp_list_numpy = zp_list_tensor.cpu().detach().numpy()
    age_list_numpy = age_list_tensor.cpu().detach().numpy()
    age_sqr_list_numpy = age_sqr_list_tensor.cpu().detach().numpy()
    gender_list_numpy = gender_list_tensor.cpu().detach().numpy()
    edu_list_numpy = edu_list_tensor.cpu().detach().numpy()
    gf_list_numpy = gf_list_tensor.cpu().detach().numpy()
    #print(zp_list_numpy.reshape(-1,1).shape)
    #print(age_list_numpy.reshape(-1).shape)
    #exit(0)
    mi_gf = mutual_info_regression(zp_list_numpy.reshape(-1,1), gf_list_numpy.reshape(-1))
    mi_age = mutual_info_regression(zp_list_numpy.reshape(-1,1), age_list_numpy.reshape(-1))
    mi_age_sqr = mutual_info_regression(zp_list_numpy.reshape(-1,1), age_sqr_list_numpy.reshape(-1))
    mi_sex = mutual_info_regression(zp_list_numpy.reshape(-1,1), gender_list_numpy.reshape(-1))
    mi_edu = mutual_info_regression(zp_list_numpy.reshape(-1,1), edu_list_numpy.reshape(-1))

    zp_list_numpy_64 = zp_list_numpy.astype(np.float64)
    age_list_numpy_64 = age_list_numpy.astype(np.float64)
    age_sqr_list_numpy_64 = age_sqr_list_numpy.astype(np.float64)
    gender_list_numpy_64 = gender_list_numpy.astype(np.float64)
    edu_list_numpy_64 = edu_list_numpy.astype(np.float64)
    gf_list_numpy_64 = gf_list_numpy.astype(np.float64)

    dcor_age = dcor.u_distance_correlation_sqr(zp_list_numpy_64.reshape(-1, 1), age_list_numpy_64)
    dcor_age_sqr = dcor.u_distance_correlation_sqr(zp_list_numpy_64.reshape(-1, 1), age_sqr_list_numpy_64)
    dcor_sex = dcor.u_distance_correlation_sqr(zp_list_numpy_64.reshape(-1, 1), gender_list_numpy_64)
    dcor_edu = dcor.u_distance_correlation_sqr(zp_list_numpy_64.reshape(-1, 1), edu_list_numpy_64)
    dcor_gf = dcor.u_distance_correlation_sqr(zp_list_numpy_64.reshape(-1, 1), gf_list_numpy_64)
    print('corr_age, corr_gender, corr_edu corr_gf, corr_age_sqr:', corr_age.item(), corr_gender.item(),corr_edu.item(),corr_gf.item(),corr_age_sqr.item())


    print('dcor_gf',dcor_gf)
    print('dcor_age',dcor_age)
    print('dcor_age_sqr',dcor_age_sqr)
    print('dcor_sex',dcor_sex)
    print('dcor_edu',dcor_edu)

    print('mi_gf',mi_edu)
    print('mi_age',mi_age)
    print('mi_age_sqr',mi_age_sqr)
    print('mi_sex',mi_sex)
    print('mi_edu',mi_edu)


    print(age_list_tensor.size())
    print(gender_list_tensor.size())
    exit(0)
    print(zp_list_tensor.size())
    z_mean = torch.mean(z_list_tensor,dim=0)
    z_project_max,_ = torch.max(zp_list_tensor,dim=0)
    z_project_min,_ = torch.min(zp_list_tensor,dim=0)
    z_project_std = torch.std(zp_list_tensor)
    z_project_mean = torch.mean(zp_list_tensor)
    print(z_project_max, z_project_min,z_project_mean,z_project_std)
    
    #TODO change image size
    rec_tensors = torch.zeros((11,160, 192, 144))
    np.save('pre_list.npy',zp_list_tensor.cpu().numpy())
    np.save('num_list.npy',gf_list_tensor.cpu().numpy())
    np.save('col_list.npy',age_list_tensor.cpu().numpy())
    exit(0)
    for i in range(11): #0.02 0.26
        middle = z_mean + (5-i) * model.pe.weight*38
        middle_project = model.pe(middle)
        print(middle_project)
        rec_tensors[i] = model.decode(middle)
        max_img = torch.max(rec_tensors[0] ).detach().cpu().numpy()
        min_img = torch.min(rec_tensors[0] ).detach().cpu().numpy()

#----------------------
        temp_2d = rec_tensors[i,80,:,:].detach().numpy()
        #print(temp_2d.shape)
        #exit(0)
        temp_2d = (temp_2d -  min_img) / (max_img - min_img) * 255
        img_2d = Image.fromarray(temp_2d.astype(np.uint8))

        img_2d.save(os.path.join(logdir, f"0_{i+1}.png"))


#----------------------------
        temp_2d = rec_tensors[i,:,96,:].detach().numpy()
        #print(temp_2d.shape)
        #exit(0)
        temp_2d = (temp_2d -  min_img) / (max_img - min_img) * 255
        img_2d = Image.fromarray(temp_2d.astype(np.uint8))

        img_2d.save(os.path.join(logdir, f"1_{i+1}.png"))
#-------------------------------
        temp_2d = rec_tensors[i,:,:,70].detach().numpy()
        #print(temp_2d.shape)
        #exit(0)
        temp_2d = (temp_2d -  min_img) / (max_img - min_img) * 255
        img_2d = Image.fromarray(temp_2d.astype(np.uint8))

        img_2d.save(os.path.join(logdir, f"2_{i+1}.png"))
        np.save(os.path.join(logdir, f"3Dimg_{i+1}.npy"), rec_tensors[i].detach().numpy())
#------------------------------
        continue
        imsave_inp(rec_tensors[:,:,80,:,:], os.path.join(logdir, f"{args.attr}-{i+1}.png"))
        imsave_inp(rec_tensors[:,:,:,80,:], os.path.join(logdir, f"{args.attr}-{i+1}_2.png"))
        imsave_inp(rec_tensors[:,:,:,:,96], os.path.join(logdir, f"{args.attr}-{i+1}_3.png"))
        np.save(os.path.join(logdir, f"{args.attr}-{i+1}.npy"), rec_tensors[i].detach().numpy())

