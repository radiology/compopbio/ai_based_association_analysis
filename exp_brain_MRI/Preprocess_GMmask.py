# -*- coding: utf-8 -*-
"""
apply GM mask on VBM data to remove outlier noise in WM region

Created on Sat Oct  9 18:19:49 2021

@author: BL
"""


from os.path import join, exists
from os import makedirs
# import matplotlib
# matplotlib.use('Agg')
# from matplotlib import pyplot as plt
# from matplotlib.colors import NoNorm
#import glob
import numpy as np
import nibabel as nib
import subprocess
#import pandas as pd




def python_apply_mask(path_in, mask, path_out):
    """img to be masked is given by a path
       mask img is np.array to be efficient
    """
    # load 3D scan
    img = nib.load(path_in)
    aff = img.affine
    img = img.get_fdata().astype(dtype='float64')

    # masking
    img = np.multiply(img, mask)
    
    # save new img into nifty
    new = nib.Nifti1Image(img, aff)
    nib.save(new, path_out) 
    
    return 

def fsl_smooth_GMmask(path_in, mask_p, s, path_out_GM, path_out_GM_s):
    """-mas: apply a (gray matter) mask
    -s: smooth
    s: int, kernel size (mm) of Gaussian smooth"""
    ## two process in one step
    #command = "fslmaths %s -mas %s -s %d %s"%(path_in, mask_p, s, path_out)
    
    ## separate steps to save results separately
    qsub_commands = []
    GM_command = "fslmaths %s -mas %s %s"%(path_in, mask_p, path_out_GM)
    print(GM_command)
    qsub_commands.append(GM_command)
    
    s_command  = "fslmaths %s -s %d %s"%(path_out_GM, s, path_out_GM_s)
    print(s_command)
    qsub_commands.append(s_command)
    
    a = ("%s; " * len(qsub_commands)) % tuple(qsub_commands)
    process = subprocess.Popen(a[:-2], shell=True, stdout=subprocess.PIPE)
    process.wait()     
    return    

def fsl_roi_GMmask(path_in, path_out_roi, mask_p, path_out_GM):
    """-mas: apply a (gray matter) mask
    
    """
    ## separate steps to save results separately
    qsub_commands = []    
    ## crop
    command = "fslroi %s %s 19 160 21 192 16 144"%(path_in, path_out_roi)
    print(command)
    qsub_commands.append(command)    

    GM_command = "fslmaths %s -mas %s %s"%(path_out_roi, mask_p, path_out_GM)
    print(GM_command)
    qsub_commands.append(GM_command)
    
    rm_command = "rm %s"%path_out_roi
    print(rm_command)
    qsub_commands.append(rm_command)
    
    a = ("%s; " * len(qsub_commands)) % tuple(qsub_commands)
    process = subprocess.Popen(a[:-2], shell=True, stdout=subprocess.PIPE)
    process.wait()     
    return 

## initial setting
data_p = '/data/scratch/bli/Representation_learning/data_3d_VBM_crop_160_192_144'
""""save in two separate folders to ease remove"""
save1_p = '/data/scratch/bli/Representation_learning/data_3d_VBM_crop_160_192_144_GM'
if not exists(save1_p):
  makedirs(save1_p) 
save2_p = '/data/scratch/bli/Representation_learning/data_3d_VBM_crop_160_192_144_GM_s3'
if not exists(save2_p):
  makedirs(save2_p)
adni_p  = '/data/scratch/bli/Representation_learning/ADNI/ADNI_1_2'

## RSS list
sublist_p = '/trinity/home/bli/Representation_learning/Subject_list/filenames.txt'
## adni_list
adni_list = r'/data/scratch/bli/Representation_learning/ADNI/filenames.txt'

sublist   = np.loadtxt(adni_list, dtype=str)

s = int(1)
## mni_icbm152_gm_kNN.nii
mask_p = '/trinity/home/bli/Representation_learning/mni_icbm152_gm_kNN_160_192_144.nii.gz'
#mask = nib.load(mask_p).get_fdata()
# image size (160, 192, 144)
# index = (None, None, 80) # x-y plane, z=80

for i, ids in enumerate(sublist):
  print("%05d, %s"%(i, ids[:-7]))
  """RSS"""
  # img_p = join(data_p, ids)
  # out1_p = join(save1_p, ids[:-7]+'_GM.nii.gz') 
  # out2_p = join(save2_p, ids[:-7]+'_GM_s%d.nii.gz'%s) 
#  python_apply_mask(img_p, mask, out_p)
  # fsl_smooth_GMmask(img_p, mask_p, s, out1_p, out2_p)
  """ADNI"""
  img_p   = join(adni_p, ids)
  out_roi = join(adni_p, ids[:-7]+'_roi.nii.gz')
  out_GM  = join(save1_p, ids[:-7]+'_GM.nii.gz')
  fsl_roi_GMmask(img_p, out_roi, mask_p, out_GM)
  #break
  
  