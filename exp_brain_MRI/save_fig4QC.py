"""Loop over the dataset to save 2D image of a certain slice for visual QC
author: BL, created on Fri Oct 08, 2021
"""

from os.path import join, exists
from os import makedirs
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from matplotlib.colors import NoNorm
#import glob
import numpy as np
import nibabel as nib
#import pandas as pd



def save_2Dimg(path_in, index, path_out):
    # load 3D scan
    img = nib.load(path_in)
    img = img.get_fdata().astype(dtype='float32')

    # slice to print
    (x, y, z) = index
    if isinstance(z, int):
        #plt.plot(img[:,:,z])
        #plt.imshow(img[:,:,80],cmap='gray',norm=NoNorm())
        plt.imsave(path_out, img[:,:,z],cmap='gray', vmin=0, vmax=3)
    elif isinstance(y, int):
        plt.imsave(path_out, img[:,y,:],cmap='gray', vmin=0, vmax=3)
    elif isinstance(x, int):
        plt.imsave(path_out, img[x,:,:],cmap='gray', vmin=0, vmax=3)
    else:
        print("To plot a 2D slice in a 3D image, specify only one non-None value")
    # save the specified slice by the index
    #plt.plot(img[*index])
    #plt.savefig(path_out, bbox_inches='tight') #dpi=None, allows .eps format
    #plt.close()
    return 

# only this function is used during training

data_p = '/data/scratch/bli/Representation_learning/data_3d_VBM_crop_160_192_144'
save_p = '/data/scratch/bli/Representation_learning/QC_2dSlices'
if not exists(save_p):
  makedirs(save_p)
sublist_p = '/trinity/home/bli/Representation_learning/Subject_list/filenames.txt'
sublist = np.loadtxt(sublist_p, dtype=str)

# image size (160, 192, 144)
index = (None, None, 80) # x-y plane, z=80

for i, ids in enumerate(sublist):
  print("%03d, %s"%(i, ids))
  img_p = join(data_p, ids)
  out_p = join(save_p, ids[:-7]+'.png')
  save_2Dimg(img_p, index, out_p)
  #break
  
  