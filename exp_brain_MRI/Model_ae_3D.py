"""
Two 3D VAE models for brain MRI analysis, using convolutional transpose (i.e., VAE_3d_convTrans)
and trilinear upsampling (i.e., VAE_3d_linearUp) in the decoder, respectively.

tensorflow and keras based

author: BL, created on Jan 13, 2022, 11:52
"""

from keras.layers import Input, concatenate,Conv3D,Add,Conv3DTranspose, Activation,Reshape
from keras.layers import BatchNormalization, MaxPooling3D, UpSampling3D, Lambda, GlobalMaxPool3D
from keras.layers.advanced_activations import LeakyReLU
# from keras.activations import exponential, ReLU
#MaxPooling2D, Conv2D, UpSampling2D, GlobalMaxPool2D,
from keras.layers import  Dense, Flatten, ReLU
from keras.models import Model
# from keras.regularizers import l1, l2
# import keras.backend as K
# from keras import Sequential
from keras.utils.training_utils import multi_gpu_model

class ModelMGPU(Model):
    def __init__(self, ser_model, gpus):
        pmodel = multi_gpu_model(ser_model, gpus)
        self.__dict__.update(pmodel.__dict__)
        self._smodel = ser_model

    def __getattribute__(self, attrname):
        '''Override load and save methods to be used from the serial-model. The
        serial-model holds references to the weights in the multi-gpu model.
        '''
        # return Model.__getattribute__(self, attrname)
        # if 'load' in attrname or 'save' in attrname:
        if 'save' in attrname:
            return getattr(self._smodel, attrname)

        return super(ModelMGPU, self).__getattribute__(attrname)
    
# class Encoder_x16(Model):
#     def __init__(self, nc, nef, nz, isize, device, variational):
#         super(Encoder_x16, self).__init__()

#         # Device
#         # self.device = device
#         self.variational = variational
#         # Encoder: (nc, isize, isize) -> (nef*8, isize//16, isize//16)
#         # originally kernel size (4,4), stride 2, nef=32
#         # https://pytorch.org/docs/stable/generated/torch.nn.Conv3d.html#torch.nn.Conv3d
#         self.encoder = Sequential(
#             # layer 1
#             #Conv3D(conv_channels[2], (3, 3, 3), padding='same', strides=1)(down_3)
#             Conv3D(nef, 3, strides=1, padding='same'),
#             # LeakyReLU(alpha=alpha)(conv_41)
#             LeakyReLU(alpha=0.2),
#             # BatchNormalization(epsilon=0.001, weights=None, momentum=0.9)(conv_42)
#             BatchNormalization(epsilon=0.001, weights=None, momentum=0.9),
#             Conv3D(nef*2, 3, strides=2, padding='valid'),
#             LeakyReLU(alpha=0.2),
#             BatchNormalization(epsilon=0.001, weights=None, momentum=0.9),
#             # layer 2
#             Conv3D(nef*2, 3, strides=1, padding='same'),
#             LeakyReLU(alpha=0.2),
#             BatchNormalization(epsilon=0.001, weights=None, momentum=0.9),
#             Conv3D(nef*4, 3, strides=2, padding='valid'),
#             LeakyReLU(alpha=0.2),
#             BatchNormalization(epsilon=0.001, weights=None, momentum=0.9),
#             # layer 3
#             Conv3D(nef*4, 3, strides=1, padding='same'),
#             LeakyReLU(alpha=0.2),
#             BatchNormalization(epsilon=0.001, weights=None, momentum=0.9),
#             Conv3D(nef*8, 3, strides=2, padding='valid'),
#             LeakyReLU(alpha=0.2),
#             BatchNormalization(epsilon=0.001, weights=None, momentum=0.9),
#             # layer 4
#             Conv3D(nef*8, 3, strides=1, padding='same'),
#             LeakyReLU(alpha=0.2),
#             BatchNormalization(epsilon=0.001, weights=None, momentum=0.9),
#             Conv3D(nef*16, 3, strides=2, padding='valid'),
#             LeakyReLU(alpha=0.2),
#             BatchNormalization(epsilon=0.001, weights=None, momentum=0.9), 
#             # layer 5, flat bootle neck, no more downsamples
#             Conv3D(nef*16, 3, strides=1, padding='same'),
#             LeakyReLU(alpha=0.2),
#             BatchNormalization(epsilon=0.001, weights=None, momentum=0.9)
#         )

#         # Map the encoded feature map to the latent vector of mean, (log)variance
#         out_size = (isize[0] // 16) * (isize[1] // 16) * (isize[2] // 16)
#         # self.mean = nn.Linear(nef*16*out_size, nz)
#         self.mean   = Dense(nz)
#         self.logvar = Dense(nz)
#         # self.logvar = nn.Linear(nef*16*out_size, nz)

#     # def reparametrize(self, mean, logvar):
#     #     # std = logvar.mul(0.5).exp_()
#     #     std = exponential(0.5*logvar)
#     #     multi_norm = torch.FloatTensor(std.size()).normal_().to(self.device)
#     #     multi_norm = Variable(multi_norm)
#     #     return multi_norm.mul(std).add_(mean)

#     def call(self, inputs):
#         # Batch size
#         batch_size = inputs.size(0)
#         # Encoded feature map
#         hidden = self.encoder(inputs)
#         # Reshape
#         hidden = hidden.view(batch_size, -1)
#         # Calculate mean and (log)variance
#         mean, logvar = self.mean(hidden), self.logvar(hidden)
#         # Sample
#         # if self.variational == True:
#         #     latent_z = self.reparametrize(mean, logvar)
#         # else:
#         #     latent_z = mean
#         latent_z = mean
        
#         return latent_z, mean, logvar


# class Decoder_x16(Model):
#     def __init__(self, nc, ndf, nz, isize):
#         super(Decoder_x16, self).__init__()

#         # Map the latent vector to the feature map space
#         #TODO attension, the following code indicates that ndf has to equal to nef
#         self.ndf = ndf
#         self.isize = isize
#         self.out_size = (isize[0] // 16) * (isize[1] // 16) * (isize[2] // 16)
#         self.decoder_dense = Sequential(
#             Dense(ndf*16*self.out_size),
#             ReLU()
#         )
#         # Decoder: (ndf*8, isize//16, isize//16) -> (nc, isize, isize)
#         # https://pytorch.org/docs/stable/generated/torch.nn.ConvTranspose3d.html#torch.nn.ConvTranspose3d
#         self.decoder_conv = Sequential(
#             #nn.UpsamplingNearest3d(scale_factor=2),
#             Conv3DTranspose(ndf*16, 2, stride=2, padding='valid'),
#             LeakyReLU(0.2),
#             BatchNormalization(),            
#             Conv3D(ndf*8, 3, strides=1, padding='same'),
#             LeakyReLU(0.2),
#             BatchNormalization(),

#             #nn.UpsamplingNearest3d(scale_factor=2),
#             Conv3DTranspose(ndf*8, 3, stride=2, padding='valid'),            
#             LeakyReLU(0.2),
#             BatchNormalization(),
#             Conv3D(ndf*4, 3, strides=1, padding='same'),
#             LeakyReLU(0.2),
#             BatchNormalization(),
        
#             #nn.UpsamplingNearest3d(scale_factor=2),
#             Conv3DTranspose(ndf*4, 3, stride=2, padding='valid'),
#             LeakyReLU(0.2),
#             BatchNormalization(),                               
#             Conv3D(ndf*2, 3, strides=1, padding='same'),
#             LeakyReLU(0.2),
#             BatchNormalization(),

#             #nn.UpsamplingNearest3d(scale_factor=2),
#             Conv3DTranspose(ndf*2, 3, stride=2, padding='valid'),
#             LeakyReLU(0.2),
#             BatchNormalization(),                               
#             Conv3D(ndf, 3, strides=1, padding='same'),
#             LeakyReLU(0.2),
#             BatchNormalization(),            
            
#             Conv3D(nc, 3, strides=1, padding='same'),
            
#             ReLU() # input image range [0, n)
#         )

#     def call(self, input):
#         batch_size = input.size(0)
#         hidden = self.decoder_dense(input).view(
#             batch_size, self.ndf*16, self.isize[0]//16, self.isize[1]//16, self.isize[2]//16) #TODO tuple
#         output = self.decoder_conv(hidden)
#         return output


# class VAE_3d_convTrans_x16(Model):
#     """
#     nc: number output channel
#     ndf: number of filters in decoder
#     nef: number of filters in encoder
#     nz: size of latent features
#     isize: input and output image size
#     """
# #    def __init__(self, nc=1, ndf=32, nef=32, nz=100, isize=64, device=torch.device("cuda:0"), is_train=True):
# #        super(VAE, self).__init__()
#     # ndf=16 or 8, test memory need
#     def __init__(self, nc=1, ndf=16, nef=16, nz=256, isize=[160,192,144], #nz=64
#                  device=torch.device("cuda"), is_train=True, variational=False):
#         super(VAE_3d_convTrans_x16, self).__init__()
#         self.nz = nz
#         # Encoder
#         self.encoder = Encoder_x16(nc=nc, nef=nef, nz=nz, isize=isize, device=device, variational=variational)
#         # Decoder
#         self.decoder = Decoder_x16(nc=nc, ndf=ndf, nz=nz, isize=isize)
#         self.pe = nn.Linear(nz, 1)
#         if is_train == False:
#             for param in self.encoder.parameters():
#                 param.requires_grad = False
#             for param in self.decoder.parameters():
#                 param.requires_grad = False

#     def forward(self, x):
#         latent_z, mean, logvar = self.encoder(x)
#         rec_x = self.decoder(latent_z)
#         zp = self.pe(latent_z)
#         return rec_x
    
#     def encode(self, x):
#         latent_z, _, _ = self.encoder(x)
#         return latent_z

#     def decode(self, z):
#         return self.decoder(z)

#Encoder: (dim, nc) -> (nef*8, dim//16)
def encoder(dim, nef, nz, nc):
    # (batch, x, y, z, 1), dim=[160,192,144]
    # nef=16, nc=1
    inp = Input(shape=dim+(nc,))
    
    x = Conv3D(nef, 3, strides=1, padding='same')(inp)
    x = LeakyReLU(alpha=0.2)(x)
    x = BatchNormalization(epsilon=0.001, weights=None, momentum=0.9)(x)
    x = Conv3D(nef*2, 2, strides=2, padding='valid')(x)
    x = LeakyReLU(alpha=0.2)(x)
    x = BatchNormalization(epsilon=0.001, weights=None, momentum=0.9)(x)
    # layer 2
    x = Conv3D(nef*2, 3, strides=1, padding='same')(x)
    x = LeakyReLU(alpha=0.2)(x)
    x = BatchNormalization(epsilon=0.001, weights=None, momentum=0.9)(x)
    x = Conv3D(nef*4, 2, strides=2, padding='valid')(x)
    x = LeakyReLU(alpha=0.2)(x)
    x = BatchNormalization(epsilon=0.001, weights=None, momentum=0.9)(x)
    # layer 3
    x = Conv3D(nef*4, 3, strides=1, padding='same')(x)
    x = LeakyReLU(alpha=0.2)(x)
    x = BatchNormalization(epsilon=0.001, weights=None, momentum=0.9)(x)
    x = Conv3D(nef*8, 2, strides=2, padding='valid')(x)
    x = LeakyReLU(alpha=0.2)(x)
    x = BatchNormalization(epsilon=0.001, weights=None, momentum=0.9)(x)
    # layer 4
    x = Conv3D(nef*8, 3, strides=1, padding='same')(x)
    x = LeakyReLU(alpha=0.2)(x)
    x = BatchNormalization(epsilon=0.001, weights=None, momentum=0.9)(x)
    x = Conv3D(nef*16, 2, strides=2, padding='valid')(x)
    x = LeakyReLU(alpha=0.2)(x)
    x = BatchNormalization(epsilon=0.001, weights=None, momentum=0.9)(x) 
    # layer 5, flat bootle neck, no more downsamples
    x = Conv3D(nef*16, 3, strides=1, padding='same')(x)
    x = LeakyReLU(alpha=0.2)(x)
    x = BatchNormalization(epsilon=0.001, weights=None, momentum=0.9)(x)  
    
    x = Flatten(data_format='channels_last')(x)
    x = Dense(nz)(x)
    # new, Jan 17, 2022
    x = LeakyReLU(alpha=0.2)(x)
    
    model = Model(inputs=inp, outputs=x)
    return model
    
def decoder(dim, ndf, nz, nc):
    # ndf=nef=16
    inp = Input(shape=(nz,))
    
    imsize = (dim[0] // 16) * (dim[1] // 16) * (dim[2] // 16)
    x      = Dense(imsize*ndf*16)(inp)
    x      = LeakyReLU(alpha=0.2)(x)
    x      = Reshape((dim[0] // 16, dim[1] // 16, dim[2] // 16, ndf*16))(x)
    
    x = Conv3DTranspose(ndf*16, 2, strides=2, padding='valid')(x)
    x = LeakyReLU(0.2)(x)
    x = BatchNormalization(epsilon=0.001, weights=None, momentum=0.9)(x)           
    x = Conv3D(ndf*8, 3, strides=1, padding='same')(x)
    x = LeakyReLU(0.2)(x)
    x = BatchNormalization(epsilon=0.001, weights=None, momentum=0.9)(x)

    #nn.UpsamplingNearest3d(scale_factor=2),
    x = Conv3DTranspose(ndf*8, 2, strides=2, padding='valid')(x)          
    x = LeakyReLU(0.2)(x)
    x = BatchNormalization(epsilon=0.001, weights=None, momentum=0.9)(x)
    x = Conv3D(ndf*4, 3, strides=1, padding='same')(x)
    x = LeakyReLU(0.2)(x)
    x = BatchNormalization(epsilon=0.001, weights=None, momentum=0.9)(x)

    #nn.UpsamplingNearest3d(scale_factor=2),
    x = Conv3DTranspose(ndf*4, 2, strides=2, padding='valid')(x)
    x = LeakyReLU(0.2)(x)
    x = BatchNormalization(epsilon=0.001, weights=None, momentum=0.9)(x)                            
    x = Conv3D(ndf*2, 3, strides=1, padding='same')(x)
    x = LeakyReLU(0.2)(x)
    x = BatchNormalization(epsilon=0.001, weights=None, momentum=0.9)(x)

    #nn.UpsamplingNearest3d(scale_factor=2),
    x = Conv3DTranspose(ndf*2, 2, strides=2, padding='valid')(x)
    x = LeakyReLU(0.2)(x)
    x = BatchNormalization(epsilon=0.001, weights=None, momentum=0.9)(x)                              
    x = Conv3D(ndf, 3, strides=1, padding='same')(x)
    x = LeakyReLU(0.2)(x)
    x = BatchNormalization(epsilon=0.001, weights=None, momentum=0.9)(x)         
    
    x = Conv3D(nc, 3, strides=1, padding='same')(x)    
    x = ReLU()(x) # input image range [0, n)    
    
    model = Model(inputs=inp, outputs=x)
    return model

def bae(dim, nef, ndf, nz, nc=1):
    inp = Input(shape=dim+(nc,))
    z_brain = encoder(dim, nef, nz, nc)(inp)
    z_brain = Lambda(lambda x: x, name='z_brain')(z_brain)
    
    rec_x   = decoder(dim, ndf, nz, nc)(z_brain)
    rec_x   = Lambda(lambda x: x, name='rec_x')(rec_x)
    
    model = Model(inputs=inp, outputs=rec_x) 
    return model
    
    