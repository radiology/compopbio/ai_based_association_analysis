# -*- coding: utf-8 -*-
"""
Created on Wed Jan 12 23:33:51 2022
tensorflow and kearas-based main train function for brain autoencoder

@author: BL
"""

# import os
"""No need for jobs on cluster"""
G = 2
# os.environ["CUDA_VISIBLE_DEVICES"]="1"
from os.path import join, exists
from os import makedirs
import numpy as np
#import csv
# from keras.callbacks import ReduceLROnPlateau,EarlyStopping 
from keras.callbacks import ModelCheckpoint, Callback, CSVLogger, LearningRateScheduler
#import time
from keras.optimizers import Adam
# from keras.optimizers.schedules import ExponentialDecay
import keras.backend as K
#from keras.utils import plot_model
import math
import functools
import pandas as pd
from DataGenerator_VBM_Cov import DataGenerator_VBM
from Model_ae_3D import bae, ModelMGPU 
from loss_tf import rec_loss

"""No need for job on cluster"""
# GPU Memory Allocation
# nIfGPUAllocation = True
# import tensorflow as tf
# from keras.backend.tensorflow_backend import set_session
# config = tf.ConfigProto()
# config.gpu_options.allow_growth = nIfGPUAllocation
# set_session(tf.Session(config = config))


## TODO setting
# nr_fold = 1
# print("Experiment of fold %d/10"%nr_fold)
#----------------------------------------------------------
GM_p   = '/data/scratch/bli/Representation_learning/data_3d_VBM_crop_160_192_144_GM'
data_p = GM_p

inc_cov = False

save_path = '/trinity/home/bli/Representation_learning/Script/log/RSS-ADNI_convTrans_GM_VAEx16_beta0.4'
save_folder = join(save_path, 'Unsupervised_allData_n256_lkrelu_tf')
if not exists(save_folder):
    makedirs(save_folder)

json_path                = join(save_folder, 'model_inputNone.json')
check_path = join(save_folder, 'valiLossWeights.{epoch:02d}-{val_loss:.2f}.hdf5') #src
check_path2 = join(save_folder, 'valiNCCWeights.{epoch:02d}-{val_loss:.2f}.hdf5') #mvd
check_path3 = join(save_folder, 'valiMAEWeights.{epoch:02d}-{val_loss:.2f}.hdf5')
weight_path_in = join(save_folder, 'model_weight_out.h5')
weight_path_out =join(save_folder, 'model_weight_out.h5')
#weight_path_out_para = join(save_path, 'parallel_model_weight.h5')
train_history_path = join(save_folder, "train_history.csv") 
batch_history_path = join(save_folder, "train_history_perBatch")
#---------------------------------------------------------------------------
p = '/trinity/home/bli/Representation_learning/Subject_list/rs_adni_excMCI'
# list_name = '/Total_RS+ADNI_fair_split_QC_%d'%int(fold)
""" unsupervised train on all data
"""
all_MRI   = np.load(join(p, 'RSS_ADNI_allDATA_allFold.npy'))
all_label = pd.read_csv(join(p, 'RSS_ADNI_allDATA_allFold_label.csv'))
all_label = all_label.set_index('bigrfullname')
print('all samples: {}'.format(len(all_MRI)))

vali_MRI   = np.load(join(p, 'RSS_ADNI_allVALI_allFold.npy'))
vali_label = pd.read_csv(join(p, 'RSS_ADNI_allVALI_allFold_label.csv'))
vali_label = vali_label.set_index('bigrfullname')
print('validation samples: {}'.format(len(vali_MRI)))

train_index = all_MRI
vali_index  = vali_MRI

#----------------------------------------------------------------------------

resume_pretrained = False
only_evaluate = False # load model_weight, but not train, evaluate on other dataset, set n_epoch = 0
save_this_weight = True  # if just test, choose False, keep pretrained weight

#https://stackoverflow.com/questions/57092637/how-to-fit-keras-imagedatagenerator-for-large-data-sets-using-batches
n_epoch = 300



# https://stats.stackexchange.com/questions/324896/training-loss-increases-with-time
para_decay_auto = {'initial_lr': 0.0001,
                   'drop_percent': 0.9,
                   'before_drop': 5, #30,3 #drop per 5 epochs
                   'patience': 3, 
                   'threshold_epsilon': 0.0} # number of eposchs before lr decay, new added


# Parameters
params_train = {'dim_xyz': (160,192,144),
          'batch_size': 8, 
          'shuffle': True}
params_vali = {'dim_xyz': (160,192,144),
          'batch_size': 1, 
          'shuffle': False}

"""only generator or data augmentation"""
# part_index, data_p, df, inc_cov
train_generator      = DataGenerator_VBM(**params_train).generate(train_index, data_p, all_label, inc_cov) 

validation_generator = DataGenerator_VBM(**params_vali).generate(vali_index, data_p, vali_label, inc_cov)

# Construct the model
print("Construct model")
model = bae(dim=params_train['dim_xyz'], nef=2, ndf=2, nz=256, nc=1)

if G <= 1:
    print("train with %d GPU" % G)
    model = model
else:
    print("train with %d GPU" % G)
    #with tf.device("/cpu:0"):
    model = ModelMGPU(model, gpus=G)

Model_Summary = model.summary()

model_json = model.to_json()
with open(json_path, "w") as json_file:
    json_file.write(model_json)
    
if resume_pretrained:
    model.load_weights(weight_path_in)
    print("Load pretrained model from disk!")
else:
    print('training a new model!')


#plot_model(parallel_model, to_file=r'C:\Users\Erasmus MC\Desktop\model.png', show_shapes=True)
opt = Adam(lr=para_decay_auto['initial_lr'], clipnorm=1.)

model.compile(optimizer=opt, loss={'rec_x': rec_loss}, metrics={'rec_x': 'mae'})
# model.compile(optimizer=opt, loss={'rec_x': rec_loss, 'z_brain':cov_loss}, 
                       # metrics={'rec_x': , 'z_brain':}, loss_weights=[10, 1])

# learning rate schedule  
def step_decay(epoch):

    initial_lrate = para_decay_auto['initial_lr']
    drop          = para_decay_auto['drop_percent']
    epochs_drop   = para_decay_auto['before_drop'] #20/10/5, overfitting start from 5th epoch
    lrate = initial_lrate * math.pow(drop, math.floor((1+epoch)/epochs_drop))

    return lrate
lr_decrease    = LearningRateScheduler(functools.partial(step_decay))

# def exp_decay(epoch):
#   return para_decay_auto['initial_lr'] * 0.9 ^ (epoch / 5)

# lr_decrease    = LearningRateScheduler(functools.partial(exp_decay))

# lr_decrease    = ExponentialDecay(para_decay_auto['initial_lr'], decay_steps=5,
#                                   decay_rate=0.9, staircase=False)


# Train model on dataset
print("Step1: generate images, while Train and Validation")

# auto_decay = ReduceLROnPlateau(monitor='val_loss', factor=para_decay_auto['drop_percent'], 
                               # patience=para_decay_auto['patience'], verbose=1, 
                               # mode='min', epsilon=para_decay_auto['threshold_epsilon'],
                               # cooldown=0, min_lr=1e-6) # the nin for combined training can be higher

# early_stopping = EarlyStopping(patience=20) #monitor="val_loss"

class LossHistory_auto(Callback):
    def on_train_begin(self, logs={}):
        self.losses = []
#       self.accuracy = []
        self.lr = []

    def on_batch_end(self, batch, logs={}):
        # append loss of batches 
        self.losses.append(logs.get('loss'))
#        self.accuracy.append(logs.get('accuracy'))


    def on_epoch_end(self, epoch, logs={}):
        self.lr.append(step_decay) #auto_decay, exp_decay
        print('lr: ', K.eval(self.model.optimizer.lr))
        # save loss of batches per epoch
#        with open(batch_history_path+str(epoch)+'.csv', 'w') as f:
#             wr = csv.writer(f, dialect='excel')
#             wr.writerow(self.losses)

loss_history = LossHistory_auto()

check = ModelCheckpoint(check_path, monitor='val_loss', 
                             verbose=1, save_best_only=True, save_weights_only=True, mode='min', period=1)
# check2 = ModelCheckpoint(check_path2, monitor='val_seg_sensitivity_v_softmax', 
                              # verbose=1, save_best_only=True, save_weights_only=True, mode='max', period=1)
# check3 = ModelCheckpoint(check_path3, monitor='val_count_loss', 
                              # verbose=1, save_best_only=True, save_weights_only=True, mode='min', period=1)

check_eachEpoch = ModelCheckpoint(weight_path_out, monitor='val_loss', 
                                       verbose=1, save_best_only=False, save_weights_only=False, mode='min', period=1)
csv_logger   = CSVLogger(train_history_path, separator=',', append=True)


callbacks_list = [check, check_eachEpoch, #check2, auto_decay, , early_stopping, 
                  loss_history, csv_logger, lr_decrease] #, , check2, check3

history = model.fit_generator(generator=train_generator,
                    steps_per_epoch=len(train_index)//params_train['batch_size'], epochs=n_epoch,
                    verbose=2, callbacks=callbacks_list,
                    validation_data=validation_generator,
                    validation_steps=len(vali_index)//params_vali['batch_size'], initial_epoch=0)
