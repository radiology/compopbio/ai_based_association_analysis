# -*- coding: utf-8 -*-
"""
Created on Mon Jan 03 12:05:19 2022

- To evalute the accuracy of Autoencoder in VBM reconstruction, compute the
correlation of regional volume between the input and recontructed VBM using
Hammer's atlas of n30r83

- Changed the path to be able to implement on the GPU cluster 

- Gender-specific correlation

- 10 folds, rss+ADNI, for dementia risk prediction

@author: BL
"""
import sys
import matplotlib
matplotlib.use("Agg")
import numpy as np
import nibabel as nib
# import nibabel.processing
from os.path import join, exists
from os import makedirs, listdir
from scipy.stats import pearsonr
import matplotlib.pyplot as plt
import fnmatch
# import csv
import pandas as pd
from Load_data_jy import get_list_label

# https://github.com/nilearn/nilearn/blob/master/nilearn/image/image.py
def compute_bbox(img, rtol=1e-8, copy=True, pad=True, return_offset=False):

#    img = check_niimg(img)
    data = img.get_fdata()
    infinity_norm = max(-data.min(), data.max())
    passes_threshold = np.logical_or(data < -rtol * infinity_norm,
                                     data > rtol * infinity_norm)

    if data.ndim == 4:
        passes_threshold = np.any(passes_threshold, axis=-1)
    coords = np.array(np.where(passes_threshold))

    # Sets full range if no data are found along the axis
    if coords.shape[1] == 0:
        start, end = [0, 0, 0], list(data.shape)
    else:
        start = coords.min(axis=1)
        end = coords.max(axis=1) + 1

    # pad with one voxel to avoid resampling problems
    if pad:
        start = np.maximum(start - 1, 0)
        end = np.minimum(end + 1, data.shape[:3])

    slices = [slice(s, e) for s, e in zip(start, end)][:3]

#    cropped_im = _crop_img_to(img, slices, copy=copy)
#    return cropped_im if not return_offset else (cropped_im, tuple(slices))
    return tuple(slices) #(x(start, end), y(start, end), z(start, end))

# done
def structure_hammer_names(file):
    #https://stackoverflow.com/questions/35871920/numpy-loadtxt-valueerror-wrong-number-of-columns
    # solution 1
    id_name_list = np.genfromtxt(file,delimiter='\t',dtype=str) 
    # solution 2
    with open(file) as f:
        a = f.read().splitlines()
        id_name_list = a
    # do somethings to replace ' ' by '_', ', ' by '-'
    return id_name_list
# done
def split_atlas_regions(atlas, label_id_names):
    # join(p, 'Evaluation', 'Hammers_RSS192_id_name_mask')
    # id_name_mask=id_name_mask
    """input: atlas, np.array
              label_id_names, array of size (83, 2), 
              e.g., array[0]=['1', 'Hippocampus_right']
    """
    id_name_mask = dict()
    #loop over n=83 regions
    for region in label_id_names:        
        key  = region[0]
        name = region[1]
        id_name_mask[key]=[name]
        
        print("---> region %s: %s"%(key, name))
        # split
        mask = np.where(atlas==int(key), 1, 0)
        id_name_mask[key].append(mask)
        print("region volume: ", np.sum(mask))        
    # np.savez_compressed(r'F:\Representation Learning\VBM\Evaluation\Hammers_RSS160_id_name_mask.npz', 
    #                     id_name_mask=id_name_mask)

    return id_name_mask


def regional_vol(img, id_name_mask_dict, label_id_names):
    """input: img, np.array
              id_name_mask_dict, 
              e.g., dict['1']=[Hippocampus_right', mask_array]
    """    
    vol_list = []
    for region in label_id_names:
        key  = region[0]
        name = region[1]    
        mask = id_name_mask_dict[key][1]        
        # VBM sum_intensity within the region mask
        # shapes (160,192,144) (197,233,189) 
        # fslroi 19 160 21 192 16 144
        vol = np.sum(np.multiply(img, mask))
        vol_list.append(vol)
        print("---> region %s: %s"%(key, name))
        print("Sum of intensity within the region: %.3f"%vol)

    return vol_list    

def plot_peasonR(x, y, name, save_p, r, p_value, name_nr):
    """ Plot region-wise """
    
    # 
    # fig = plt.figure()
    # fig.patch.set_facecolor('white')
    plt.figure()
    fig, ax = plt.subplots(frameon=True)
    # ax.spines['right'].set_visible(False)
    # ax.spines['top'].set_visible(False)

    ax.scatter(x, y, label="peasonR=%.3f, \np_value=%.3f"%(r, p_value))
    ax.set_xlabel('x, input')
    ax.set_ylabel('y, output')

    plt.legend()
    plt.title(name)
    plt.savefig(join(save_p, name_nr+'_'+name+'_peasonR.png'))
    plt.close()
    return

def plot_peasonR_gender(x1, y1, x2, y2, name, save_p, r1, p_value1, r2, p_value2, name_nr):
    """ Plot region-wise 
    gendered group
    """
    
    # 
    # fig = plt.figure()
    # fig.patch.set_facecolor('white')
    plt.figure()
    fig, ax = plt.subplots(frameon=True)
    # ax.spines['right'].set_visible(False)
    # ax.spines['top'].set_visible(False)
    
    # female
    ax.scatter(x1, y1, label="Female peasonR=%.3f, \np_value=%.3f"%(r1, p_value1))
    ax.scatter(x2, y2, label="Male peasonR=%.3f, \np_value=%.3f"%(r2, p_value2))
    
    ax.set_xlabel('x, input')
    ax.set_ylabel('y, output')
    plt.legend()
    plt.title(name)
    plt.savefig(join(save_p, name_nr+'_'+name+'_peasonR_gender.png'))
    plt.close()
    return


##TODO check the name of the files
def save_region_vol_datasets(test_on, data_p, save_p, hammer_cnn_p, name_structured_p, name_exd):
    """load info of Hammer; or compute with the functions defined above"""
    label_id_names = np.loadtxt(name_structured_p, dtype=str)
    
    # load or compute
    atlas = nib.load(hammer_cnn_p).get_fdata()
    id_name_mask_dict = split_atlas_regions(atlas, label_id_names)
    # id_name_mask_dict = np.load(hammer_RSS_regions_p, allow_pickle=True)['id_name_mask']
    # id_name_mask_dict = id_name_mask_dict.item()
    
    """loop and save"""
    for i, sub in enumerate(test_on):
        print("#%04d, %s"%(i, sub))
        # out_file = join(save_p, sub[:-7]+'.npy')
        out_file = join(save_p, sub+'_GM_to_template_GM_mod.npy')
        if not exists(out_file):        
            # load img
            ## TODO check if file extension is included in file name
            # e.g., "ergmri_3909_m_3149707_15997_GM_to_template_GM_mod.nii.gz"
            # img_p = join(data_p, sub)    
            # img_p = join(data_p, sub[:-7]+'_GM.nii.gz')         
            if '.npz' == name_exd:                            
                # img_p = join(data_p, sub[:-29]+name_exd) 
                img_p = join(data_p, sub+name_exd)
            else:
                # img_p = join(data_p, sub[:-7]+name_exd)
                img_p = join(data_p, sub+name_exd)
            if exists(img_p):
                if '.npz' == name_exd: 
                    img = np.load(img_p)['rec_x']                
                else:
                    img = nib.load(img_p).get_fdata().astype('float32')

                # volume list of length 83
                sub_vol = regional_vol(img, id_name_mask_dict, label_id_names)
                # save
                # np.save(join(save_p, sub[:-7]+'.npy'), sub_vol)
                np.save(out_file, sub_vol)
            else:
                continue
        else:
            continue
        # break
    return

def regional_pearsonR(test_on, x_p, y_p, name_structured_p, save_p, n_test, df):
    
    # genders = demography['']
    # id, name of Hammer's regions
    label_id_names = np.loadtxt(name_structured_p, dtype=str)
    # size=(83, 2, n)
    
    # based on x_input:
    # n = len(test_on)
    
    #based on y_input, m < n
    # https://stackoverflow.com/questions/2632205/how-to-count-the-number-of-files-in-a-directory-using-python
    # m = len(fnmatch.filter(listdir(y_p), '*.npy'))
    # if not 'all' == n_test:
    #     # test on the given number and test_on list, e.g., validation
    #     n=n_test
    # else:
    #     # test on all files in the folder
    #     n=m 
    
    if n_test in ['train', 'validation', 'test', 'all']:
        n=int(len(test_on))
    
    regional_x_y = np.zeros(shape=(83, 2, n)) 
    k=0    
    regional_r   = []
    empty_y = []
    
    female_x = []
    female_y = []
    male_x   = []
    male_y   = []
    female_r = []
    male_r   = []
    
    """region-wise paired list of x (input) and y (prediction)"""
    print("Reformat region-wise datapoints (n=%d)"%n)
    for i, sub in enumerate(test_on):
        if k > n-1:
            break
        print("#%04d, %s"%(i, sub))  
        
        # gender = df[df.index==sub[:-29]]['sex'].values
        gender = df[df.index==sub]['sex'].values
        print('Gender (1-female, 0-male): ', gender)
        # if temp_sex == ' ':
        if len(gender) == 0:
            continue
        
        # size (83,)
        # check first the y, if not exist, put none in x
        # use the same size for all methods so that they are comparable
        # but pearsonr gives error: "array must not contain infs or NaNs"
        # file_y = join(y_p, sub[:-7]+'.npy')
        file_y = join(y_p, sub+'_GM_to_template_GM_mod.npy')
        if not exists(file_y):
            # regional_x_y[:, 1, i] = np.full(83, np.nan)
            # regional_x_y[:, 0, i] = np.full(83, np.nan)
            empty_y.append(sub)
            continue
        else:
            y = np.load(file_y) 
            # regional_x_y[:, 1, i] = y
            regional_x_y[:, 1, k] = y 

            # x = np.load(join(x_p, sub[:-7]+'.npy')) 
            x = np.load(join(x_p, sub+'_GM_to_template_GM_mod.npy'))
            # regional_x_y[:, 0, i] = x
            regional_x_y[:, 0, k] = x
            k +=1
            
            if 0 == gender:
                # final shape: (n, 83)
                male_x.append(x)
                male_y.append(y)
            elif 1 == gender:
                female_x.append(x)
                female_y.append(y)                
            
    assert k==n, "k: %d, n: %d"%(k, n)
    
    male_x    = np.asarray(male_x)
    male_y    = np.asarray(male_y)
    female_x  = np.asarray(female_x)
    female_y  = np.asarray(female_y)
    
    print("Nr of female subjects: %d"%(female_x.shape[0]))
    print("Nr of male subjects: %d"%(male_x.shape[0]))    
    
    print("Nr of empty predictions: %d"%len(empty_y))
    np.savetxt(join(save_p,'empty_predictions_noAgeGender.txt'), empty_y, fmt='%s')        

    """region-wise peasonR"""
    print("Compute region-wise peasonR")
    for region in label_id_names:
        key_index  = int(region[0]) - 1
        name       = region[1]  
        print(region[0], name)
        
        r, p_value = pearsonr(x=regional_x_y[key_index, 0, :], y=regional_x_y[key_index, 1, :])
        print("---> peasonR: %.3f, p_value: %.3f"%(r, p_value))
                
        # save results
        regional_r.append([r, p_value])
        
        # plot results
        plot_peasonR(regional_x_y[key_index, 0, :], regional_x_y[key_index, 1, :],
                     name, save_p, r, p_value, str(key_index+1))


        """gender-specific"""
        # female
        r1, p_value1 = pearsonr(x=female_x[:, key_index], y=female_y[:, key_index])
        print("---> Female peasonR: %.3f, p_value: %.3f"%(r, p_value))
        
        # save results
        female_r.append([r1, p_value1])
        
        # male
        r2, p_value2 = pearsonr(x=male_x[:, key_index], y=male_y[:, key_index])
        print("---> Male peasonR: %.3f, p_value: %.3f"%(r, p_value))
        
        # save results
        male_r.append([r2, p_value2])        
        
        # plot results       
        # x1 - female
        plot_peasonR_gender(female_x[:, key_index], female_y[:, key_index],
                            male_x[:, key_index], male_y[:, key_index], 
                            name, save_p, r1, p_value1, r2, p_value2, str(key_index+1))
        
    new_list = [j for i, j in enumerate(regional_r) if i not in [16, 17, 18, 73, 74]]
    new_list = np.asarray(new_list)
    mean_r = np.mean(new_list[:,0])
    print("Average peasonR over 78 regions is %.3f"%mean_r)
    np.savetxt(join(save_p,'regional_peasonR_average_%.3f.txt'%mean_r), regional_r, fmt='%s')

    new_f = [j for i, j in enumerate(female_r) if i not in [16, 17, 18, 73, 74]]
    new_f = np.asarray(new_f)
    Fmean_r = np.mean(new_f[:,0])
    print("Female average peasonR over 78 regions is %.3f"%Fmean_r)
    np.savetxt(join(save_p,'Female_regional_peasonR_average_%.3f.txt'%Fmean_r), female_r, fmt='%s')
    
    new_m = [j for i, j in enumerate(male_r) if i not in [16, 17, 18, 73, 74]]
    new_m = np.asarray(new_m)
    Mmean_r = np.mean(new_m[:,0])
    print("Male average peasonR over 78 regions is %.3f"%Mmean_r)
    np.savetxt(join(save_p,'Male_regional_peasonR_average_%.3f.txt'%Mmean_r), male_r, fmt='%s')
    
    return
   
## initial setting
p = '/trinity/home/bli/Representation_learning'
## Hammer official
name_structured_p = join(p, 'Hammers_mith_atlases_structure_names_r83_structured.txt')

## Hammer registered to RSS
# # fslroi 19 160 21 192 16 144
hammer_cnn_p          = join(p, 'Hammer_1mm_MNI_RSS_CNN160.nii.gz')
hammer_cnn_regions_p  = join(p, 'Hammers_RSS160_id_name_mask.npz')

## RSS
# RSS_GM_p    = join(p, 'VBM_mask_Gena', 'mni_icbm152_gm_kNN.nii.gz')
# RSS_brain_p = join(p, 'VBM_mask_Gena', 'brain_mask_1mm.nii.gz')
scratch_p = '/data/scratch/bli/Representation_learning' 
GM_p      = join(scratch_p, 'data_3d_VBM_crop_160_192_144_GM')
GM_s_p    = join(scratch_p, 'data_3d_VBM_crop_160_192_144_GM_s3')
results_p = join(p, 'Script','log')
## subject list
"""
list_p     = join(p, 'Subject_list')
sub_list_p = join(list_p, 'qualified_filenames.txt')
df_p       = join(list_p, 'RS_general_data.csv')
# statistic_save_p = join(results_p, 'PeasonR', 'GM_GM-s1')
## TODO check, subjects without demography info were excluded from training
# therefore the list of input data and pred data can be different    
test_on = np.loadtxt(sub_list_p, dtype=str)
"""

fold = int(sys.argv[3])
list_p = join(p, 'Subject_list', 'rs_adni_excMCI')
list_name = '/Total_RS+ADNI_fair_split_QC_%d'%(fold)

train_MRI, train_label, vali_MRI, vali_label, test_MRI, test_label = get_list_label(list_p, list_name)
print("--- Fold %d"%fold)
print("train samples: {}".format(len(train_MRI)))
print("validation samples: {}".format(len(vali_MRI)))
print("test samples: {}".format(len(test_MRI)))

#-----------------------------------------------------------------------------------------
"""initial setting"""

# 'x_vol'--input, 'y_vol'--prediction, 'peasonr'
todo = sys.argv[1]
    
# "mod_GM", or "mod_GM_s1" 
# x_input_type = sys.argv[2]
x_input_type = "mod_GM"

# y_pred_method = "RSS-ADNI_convTrans_GM_VAEx16_beta0.4/fold_1_unsupervised'
y_pred_base = 'RSS-ADNI_convTrans_GM_VAEx16_beta0.4'
y_pred_method = sys.argv[2]
y_pred_method = join(y_pred_base, y_pred_method)
# x_input_type  = "mod_GM_s1"
# y_pred_method = "convTrans_GM_s1_VAEx16" #"linearUP_GM_s1_VAEx16"



# 'train', 'validation', 'test', 'all'
n_test = sys.argv[4]
if 'train' == n_test:
    statistic_save_p = join(results_p, y_pred_method, 'PeasonR_trainingSet')
    test_on = train_MRI
    df      = train_label
elif 'validation' == n_test:
    statistic_save_p = join(results_p, y_pred_method, 'PeasonR_validationSet')
    test_on = vali_MRI
    df      = vali_label
elif 'test' == n_test:
    statistic_save_p = join(results_p, y_pred_method, 'PeasonR_testSet')
    test_on = test_MRI
    df      = test_label 
elif 'all' == n_test:
    statistic_save_p = join(results_p, y_pred_method, 'PeasonR_allSet')
    test_on = np.append(train_MRI, vali_MRI)
    test_on = np.append(test_on, test_MRI)
    # https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.append.html
    df = train_label.append(vali_label)
    df = df.append(test_label)    
else:
    print("5th arg: either 'training', 'validation', 'test', or 'all'")    

#------------------------------------------------------------------------------------------    

 
""" step 1: compute regional volume, input or prediction. Input only once"""
if 'x_vol' == todo:
    print("Compute regional volume of the input VBM data")
    if "mod_GM" == x_input_type:
        data_p = GM_p
        # name_exd = '_GM.nii.gz'
        name_exd = '_GM_to_template_GM_mod_GM.nii.gz'        
       
    elif "mod_GM_s1" == x_input_type:
        data_p = GM_s_p
        # name_exd = '_GM_s1.nii.gz'    
        name_exd = '_GM_to_template_GM_mod_GM_s1.nii.gz'
    
    save_p = join(results_p, 'Regional_vol_input_%s'%x_input_type)
    if not exists(save_p):
        makedirs(save_p)     
    
    save_region_vol_datasets(test_on, data_p, save_p, hammer_cnn_p, name_structured_p, name_exd)
elif 'y_vol' == todo:    
    print("Compute regional volume of the output reconstructed VBM data")
    
    data_p = join(scratch_p, y_pred_method, 'prediction') #'z_brain'
    name_exd = '.npz'
    
    save_p = join(results_p, y_pred_method, 'Regional_vol_output')
    if not exists(save_p):
        makedirs(save_p)
    save_region_vol_datasets(test_on, data_p, save_p, hammer_cnn_p, name_structured_p, name_exd)
        
    """ step 2: compute correlation between volumes"""
    # for each of the 83 regions, compute a pearsonr of all datapoints, i.e., scans
elif 'peasonr' == todo:
    if not exists(statistic_save_p):
        makedirs(statistic_save_p)    
    x_p = join(results_p, 'Regional_vol_input_%s'%x_input_type)
    y_p = join(results_p, y_pred_method, 'Regional_vol_output')

    # y_p = join(results_p, 'Regional_vol_input_mod_GM_s1')
    
    # regional_pearsonR(test_on, x_p, y_p, name_structured_p, statistic_save_p, n_test)
    # df = pd.read_csv(df_p, index_col='bigrfullname')
               
    regional_pearsonR(test_on, x_p, y_p, name_structured_p, statistic_save_p, n_test, df)



"""## step 0: correctly pad the hammer atlas to correspond to the RSS template"""
# # although both images are in the MNI 1mm space, they have different matrix size
# # MNI (182, 218, 182); hammer (181, 217, 181); RSS (197, 233, 189)
# hammer_brain = nib.load(hammer_brain_p)
# # (slice(16, 163, None), slice(21, 199, None), slice(5, 153, None))
# hammer       = nib.load(hammer_p)
# # (slice(16, 163, None), slice(21, 199, None), slice(5, 153, None))
# RSS_brain    = nib.load(RSS_brain_p)
# # (slice(19, 178, None), slice(19, 217, None), slice(0, 162, None))
# RSS_GM       = nib.load(RSS_GM_p)
# # (slice(23, 174, None), slice(26, 211, None), slice(16, 157, None))

# ## fslstats icbm452_atlas_probability_GM.nii.gz -w
# #0 149 0 188 0 148 0 1

# ## fslstats /cm/shared/apps/fsl/5.0.2.2/data/standard/MNI152lin_T1_1mm_brain.nii.gz -w
# # 15 147 17 185 0 158 0 1

# ## fslstats /cm/shared/apps/fsl/5.0.2.2/data/standard/MNI152_T1_1mm_brain_mask.nii.gz -w
# # 18 143 20 180 0 154 0 1