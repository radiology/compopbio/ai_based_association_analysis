# import os
from os.path import join, exists
from os import makedirs
import sys
import argparse
import numpy as np
import torch
import torch.optim as optim
# import torch.nn as nn
# import math
import torch.nn.functional as F
sys.path.append(".")
# from utils.data_GPUcluster import get_celeba_loaders
from data_GPUcluster import get_celeba_loaders
from vis import plot_loss, imsave, Logger
from loss import ncc_loss, KLDLoss #FLPLoss, , 
##TODO try linear upsampling
from simple_vae_3D_convTranse_linearUp import VAE_3d_convTrans_x16, VAE_3d_linearUp_x16

scratch_p = '/data/scratch/bli/Representation_learning'

parser = argparse.ArgumentParser(description='vae.pytorch')
parser.add_argument('--logdir', type=str, default="./log/linearUP_GM_VAEx16")
parser.add_argument('--preddir', type=str, default=join(scratch_p,"linearUP_GM_VAEx16"))
parser.add_argument('--batch_train', type=int, default=1) # test bn=8
parser.add_argument('--batch_test', type=int, default=1) # 16 memory oom
parser.add_argument('--epochs', type=int, default=300)
parser.add_argument('--gpu', type=str, default="0")
parser.add_argument('--initial_lr', type=float, default=0.0001)
parser.add_argument('--grad_norm', type=float, default=1.0)
parser.add_argument('--lr_decay_exp', type=float, default=0.9)

parser.add_argument('--alpha', type=float, default=0.001,
                    help='hyperparameter for KLD term; the one for NCC is 1 by default') # 
parser.add_argument('--beta', type=float, default=0.04,
                    help='hyperparameter for intensity reconstruction term') #1.0, for intensity sim
# parser.add_argument('--model', type=str, default="pvae", choices=["vae-123", "vae-345", "pvae"])

parser.add_argument('--model', type=str, default="linearUP", choices=["linearUP", "convTrans"])
parser.add_argument('--nz', type=int, default=256,
                    help='size of latent feature')
parser.add_argument('--nef', type=int, default=16,
                    help='nr of conv kernels')
parser.add_argument('--variational', type=bool, default=False)
parser.add_argument('--isize', type=list, default=[160,192,144],
                    help='input size')

parser.add_argument('--prepro', type=str, default="mod_GM", choices=["mod_GM", "mod_GM_s1"],
                    help='preprocess strategy to use, GM or GM_s1')

parser.add_argument('--nr_test', type=int, default=128,
                    help='nr of images for validation')

args = parser.parse_args()

plot_slice = True

""" only if on local workstation and there are multiple GPUs installed.
# Set GPU (Single GPU usage is only supported so far)
# os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu
"""

# Device
torch.set_num_threads(1)
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
# device = torch.device('cpu')
# print(torch.cuda.current_device())

# Dataloader #--------- added Oct 12
"""local
p = r'F:\Representation Learning'
list_p = join(p, 'subject demography','subject_list', 'local_test_175.txt') 
df_p   = join(p, 'subject demography','subject_list', 'RS_general_data.csv')
GM_p   = join(p, 'VBM','data_3d_VBM_crop_160_192_144_GM')
GM_s_p = join(p, 'VBM','data_3d_VBM_crop_160_192_144_GM_s3')
"""

"""GPU cluster"""
p = '/trinity/home/bli/Representation_learning/Subject_list/'
list_p = join(p, 'qualified_filenames.txt') #'filenames.txt'
df_p   = join(p, 'RS_general_data.csv')
# scratch_p = '/data/scratch/bli/Representation_learning'
GM_p   = join(scratch_p, 'data_3d_VBM_crop_160_192_144_GM')
GM_s_p = join(scratch_p, 'data_3d_VBM_crop_160_192_144_GM_s3')

if "mod_GM" == args.prepro:
    data_p = GM_p
    name_exd = '_GM.nii.gz'
elif "mod_GM_s1" == args.prepro:
    data_p = GM_s_p
    name_exd = '_GM_s1.nii.gz'

dataloaders = get_celeba_loaders(args.batch_train, args.batch_test, args.nr_test,
                                 list_p, df_p, data_p, name_exd)



# dataloaders = get_celeba_loaders(args.batch_train, args.batch_test)
# Model
if args.model == "convTrans":
    model = VAE_3d_convTrans_x16(device=device, ndf=args.nef, nef=args.nef, nz=args.nz, 
                             isize=args.isize, variational=args.variational).to(device)
elif args.model == "linearUP":
    model = VAE_3d_linearUp_x16(device=device, ndf=args.nef, nef=args.nef, nz=args.nz, 
                            isize=args.isize, variational=args.variational).to(device)


# -----------------------------

# Reconstruction loss
# if args.model == "pvae":
#     reconst_criterion = nn.MSELoss(reduction='sum')
# elif args.model == "vae-123" or args.model == "vae-345":
#     reconst_criterion = FLPLoss(args.model, device, reduction='sum')
# KLD loss


# Log
logdir = args.logdir
if not exists(logdir):
    makedirs(logdir)
# Logger
logger = Logger(join(logdir, "log_test.txt"))
# History
history = {"train": [], "test": []}

# Save config
logger.write('----- Options ------')
for k, v in sorted(vars(args).items()):
    logger.write('%s: %s' % (str(k), str(v)))


weight_p = join(logdir, 'final_model.pth')
try:
    model.load_state_dict(torch.load(weight_p)) #, map_location=device
except:
    print("Invalid weight path.")
    
# model.load_state_dict(torch.load(weight_p))
# https://stackoverflow.com/questions/66952664/how-do-i-predict-using-a-pytorch-model
model.eval()


# Start test
save_z_p = join(args.preddir,'z_brain')
if not exists(save_z_p):
    makedirs(save_z_p)
    
save_pred_p = join(args.preddir,'prediction')
if not exists(save_pred_p):
    makedirs(save_pred_p)
  
for data in dataloaders["train"]:
    #print(data.size())
    x = data[0]
    #print(x.size(0),args.batch_train)
    #age_t = data[1].to(device)
    #gender_t = data[2].to(device)
    name_t = str(data[3])
     
    name_t = name_t.replace('(','')
    name_t = name_t.replace(')','')
    name_t = name_t.replace('\'','')
    name_t = name_t.replace(',','')            
 
    #print(age_t, gender_t)
    #exit(0)
    # Optimize params
    x = x.to(device)
    latent_z, _, _ = model.encoder(x)
    save_z = latent_z.detach().cpu().numpy()
    np.save(join(save_z_p, name_t), save_z)
    rec_x = model(x)
    rec_x = rec_x.detach().cpu().numpy()
    np.savez_compressed(join(save_pred_p, name_t), rec_x=rec_x)

for data in dataloaders["test"]:
    #print(data.size())
    x = data[0]
    #print(x.size(0),args.batch_train)
    #age_t = data[1].to(device)
    #gender_t = data[2].to(device)
    name_t = str(data[3])
     
    name_t = name_t.replace('(','')
    name_t = name_t.replace(')','')
    name_t = name_t.replace('\'','')
    name_t = name_t.replace(',','')            
 
    #print(age_t, gender_t)
    #exit(0)
    # Optimize params
    x = x.to(device)
    latent_z, _, _ = model.encoder(x)
    save_z = latent_z.detach().cpu().numpy()
    np.save(join(save_z_p, name_t), save_z)
    rec_x = model(x)
    rec_x = rec_x.detach().cpu().numpy()
    np.savez_compressed(join(save_pred_p, name_t), rec_x=rec_x)
    