import plotly.graph_objects as go
import numpy as np

import torch
from PIL import Image
from scipy.ndimage import zoom

from collections import defaultdict
import pprint


image_start = np.load('3Dimg_11.npy')
image_end = np.load('3Dimg_1.npy')
print(image_end.shape)



#exit(0)
##-----
image = image_end -image_start 

image[image>0] = 0
image = np.abs(image)

image_2d = image[:,:,72]
max_img = np.max(image_2d)
min_img = np.min(image_2d)

print(max_img, min_img)
image_2d = (image_2d -  min_img) / (max_img - min_img) * 255
image_2d = Image.fromarray(image_2d.astype(np.uint8))
image_2d.save('diff_3_n.png')



image = image_end -image_start 

image[image<0] = 0
image = np.abs(image)

image_2d = image[:,:,72]
max_img = np.max(image_2d)
min_img = np.min(image_2d)

print(max_img, min_img)
image_2d = (image_2d -  min_img) / (max_img - min_img) * 255
image_2d = Image.fromarray(image_2d.astype(np.uint8))
image_2d.save('diff_3_p.png')






#-----------------------------
image = image_end -image_start 

image[image>0] = 0
image = np.abs(image)

image_2d = image[:,90,:]
max_img = np.max(image_2d)
min_img = np.min(image_2d)

print(max_img, min_img)
image_2d = (image_2d -  min_img) / (max_img - min_img) * 255
image_2d = Image.fromarray(image_2d.astype(np.uint8))
image_2d.save('diff_2_n.png')


image = image_end -image_start 

image[image<0] = 0
image = np.abs(image)

image_2d = image[:,90,:]
max_img = np.max(image_2d)
min_img = np.min(image_2d)

print(max_img, min_img)
image_2d = (image_2d -  min_img) / (max_img - min_img) * 255
image_2d = Image.fromarray(image_2d.astype(np.uint8))
image_2d.save('diff_2_p.png')


#--------------------

image = image_end -image_start 

image[image>0] = 0
image = np.abs(image)

image_2d = image[80,:,:]
max_img = np.max(image_2d)
min_img = np.min(image_2d)

print(max_img, min_img)
image_2d = (image_2d -  min_img) / (max_img - min_img) * 255
image_2d = Image.fromarray(image_2d.astype(np.uint8))
image_2d.save('diff_1_n.png')


image = image_end -image_start 

image[image<0] = 0
image = np.abs(image)

image_2d = image[80,:,:]
max_img = np.max(image_2d)
min_img = np.min(image_2d)

print(max_img, min_img)
image_2d = (image_2d -  min_img) / (max_img - min_img) * 255
image_2d = Image.fromarray(image_2d.astype(np.uint8))
image_2d.save('diff_1_p.png')



exit(0)























image = zoom(image, (0.5, 0.5, 0.5))



print(image.shape)
X, Y, Z = np.mgrid[-10:10:80j, -12:12:96j, -9:9:72j]
#X, Y, Z = np.mgrid[-9:9:36j, -10:10:38j, -12:12:40j]
#X, Y, Z = np.mgrid[-9:9:72j, -10:10:80j, -12:12:96j]

print(X.flatten().shape, Y.shape, Z.shape)
values= image

fig = go.Figure(data=go.Volume(
    x=X.flatten(),
    y=Y.flatten(),
    z=Z.flatten(),
    value=values.flatten(),
    #isomin = 0.25,
    opacity=0.1, # needs to be small to see through all surfaces
    surface_count=22, # needs to be a large number for good volume rendering
    colorscale='Gray'
    ))
fig.show()
fig.write_image("fig_11.png")
