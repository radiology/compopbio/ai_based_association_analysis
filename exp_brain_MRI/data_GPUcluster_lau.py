from os.path import join
#import glob
import numpy as np
from PIL import Image#, ImageOps
#import cv2
#import imgaug as ia
# from imgaug import augmenters as iaa
import torch
#import torchvision
from torchvision import transforms #datasets, 
from torch.utils.data import Dataset, DataLoader#,TensorDataset
import nibabel as nib
import pandas as pd
# import cv2

# class ImgAugTransform:
#   def __init__(self):
#     self.aug = iaa.Sequential([
#         iaa.Fliplr(0.5),
#         iaa.Sometimes(0.4,\
#             iaa.ContrastNormalization((0.8, 1.2), per_channel=0.5)),
#         iaa.Sometimes(0.4,\
#             iaa.GaussianBlur(sigma=(0, 0.2))),
#     ])
      
#   def __call__(self, img):
#     img = np.array(img)
#     img = self.aug.augment_image(img)
#     return img



class ImageDataset(Dataset):
    def __init__(self, paths_image, age, gender, img_name, is_aug=True):
        super(ImageDataset, self).__init__()

        # Length
        self.length = len(paths_image)
        # Image path
        self.paths_image = paths_image
        self.age = age
        self.gender = gender
        self.img_name = img_name
        # Augment
        self.is_aug = is_aug
        self.transform = transforms.Compose([
            transforms.ColorJitter(brightness=0.2, contrast=0.2, saturation=0.2, hue=0.1),
            # ImgAugTransform(),
            lambda x: Image.fromarray(x)
        ])
        # Preprocess
        """ToTensor converts a PIL image or NumPy ndarray into a FloatTensor. 
            and scales the image’s pixel intensity values in the range [0., 1.]
            """
        self.output = transforms.Compose([
           
            # transforms.Resize((64, 64)),
            ## TODO add flip
            #transforms.RandomApply(torch.flip(input, dims), p=0.3),
            #transforms.RandomHorizontalFlip(p=0.2),

            transforms.ToTensor()
            ])

    def __len__(self):
        return self.length

    def __getitem__(self, idx):
        # Image
        #print(idx)
        img = nib.load(self.paths_image[idx]) #.replace('\n','')
        img = img.get_fdata().astype(dtype='float32')
        age = self.age[idx]
        gender = self.gender[idx]
        name   = self.img_name[idx]
        
        # file_name = self.paths_image[idx].replace('\n','')
        # split_file_name = file_name.split('/')
        # name_final = split_file_name[len(split_file_name)-1].replace('_GM_to_template_GM_mod','').replace('.nii.gz','')
        # name_final_split = name_final.split('_')
        # print(name_final_split[1])        
        
        
        #str_name = self.paths_image[idx].replace('\n','')
        #str_name_s = str_name.split('/')
        #name_final = str_name_s[len(str_name_s)-1]
        #print(name_final)
        #exit(0)
        #img = img.reshape([1,160,192,144])
        #print(img.shape)
     
        # intensity normalize
        # remove
        #img = (img -  np.min(img)) / (np.max(img) - np.min(img)) * 255
        #img = Image.fromarray(img) for 3D
        #img = img.convert("L")   for 3D

        #exit(0)
        
        #img = img.unsqueeze(-1) # reshape [160,192,144] to [1, 144,160, 192]
        img = np.expand_dims(img, axis=0)
        #img = self.output(img) #[1, 160, 192, 144]
        """transforms is not used, as it supports only 2/3D image"""
        img = torch.as_tensor(img)
        ##print(img.size())
        #img = img[0,80,:,:]
        #print(img.size())
        #cv2.imwrite('qc/'+name_final.replace('.gz','.png'),img.cpu().numpy())
        #exit(0)
        #print(img.size())
        # Preprocess
        #img = self.output(img)
        #print(self.paths_image[idx].replace('\n',''), age,gender)
        return img, age ,gender, name

class ImageDataset_labeled(Dataset):
    def __init__(self, paths_image, age, sex, eduy, tgtv, img_name, is_aug=True):
        super(ImageDataset_labeled, self).__init__()

        # Length
        self.length = len(paths_image)
        # Image path
        self.paths_image = paths_image
        self.age  = age
        self.sex  = sex
        self.eduy = eduy
        self.tgtv = tgtv
        self.img_name = img_name
        # Augment
        self.is_aug = is_aug
        self.transform = transforms.Compose([
            transforms.ColorJitter(brightness=0.2, contrast=0.2, saturation=0.2, hue=0.1),
            # ImgAugTransform(),
            lambda x: Image.fromarray(x)
        ])
        # Preprocess
        """ToTensor converts a PIL image or NumPy ndarray into a FloatTensor. 
            and scales the image’s pixel intensity values in the range [0., 1.]
            """
        self.output = transforms.Compose([
           
            # transforms.Resize((64, 64)),
            ## TODO add flip
            #transforms.RandomApply(torch.flip(input, dims), p=0.3),
            #transforms.RandomHorizontalFlip(p=0.2),

            transforms.ToTensor()
            ])

    def __len__(self):
        return self.length

    def __getitem__(self, idx):
        # Image
        #print(idx)
        img   = nib.load(self.paths_image[idx]) #.replace('\n','')
        img   = img.get_fdata().astype(dtype='float32')
        age_  = self.age[idx]
        sex_  = self.sex[idx]
        eduy_ = self.eduy[idx]
        tgty_ = self.tgtv[idx]
        name  = self.img_name[idx]
        
        #img = img.unsqueeze(-1) # reshape [160,192,144] to [1, 144,160, 192]
        img = np.expand_dims(img, axis=0)
        #img = self.output(img) #[1, 160, 192, 144]
        """transforms is not used, as it supports only 2/3D image"""
        img = torch.as_tensor(img)

        return img, age_, sex_, eduy_, tgty_, name

class ImageDataset_unlabeled(Dataset):
    def __init__(self, paths_image, img_name, is_aug=True):
        super(ImageDataset_unlabeled, self).__init__()

        # Length
        self.length = len(paths_image)
        # Image path
        self.paths_image = paths_image
        # self.age  = age
        # self.sex  = sex
        # self.eduy = eduy
        # self.tgtv = tgtv
        self.img_name = img_name
        # Augment
        self.is_aug = is_aug
        self.transform = transforms.Compose([
            transforms.ColorJitter(brightness=0.2, contrast=0.2, saturation=0.2, hue=0.1),
            # ImgAugTransform(),
            lambda x: Image.fromarray(x)
        ])
        # Preprocess
        """ToTensor converts a PIL image or NumPy ndarray into a FloatTensor. 
            and scales the image’s pixel intensity values in the range [0., 1.]
            """
        self.output = transforms.Compose([
           
            # transforms.Resize((64, 64)),
            ## TODO add flip
            #transforms.RandomApply(torch.flip(input, dims), p=0.3),
            #transforms.RandomHorizontalFlip(p=0.2),

            transforms.ToTensor()
            ])

    def __len__(self):
        return self.length

    def __getitem__(self, idx):
        # Image
        #print(idx)
        img   = nib.load(self.paths_image[idx]) #.replace('\n','')
        img   = img.get_fdata().astype(dtype='float32')
        # age_  = self.age[idx]
        # sex_  = self.sex[idx]
        # eduy_ = self.eduy[idx]
        # tgty_ = self.tgtv[idx]
        name  = self.img_name[idx]
        
        #img = img.unsqueeze(-1) # reshape [160,192,144] to [1, 144,160, 192]
        img = np.expand_dims(img, axis=0)
        #img = self.output(img) #[1, 160, 192, 144]
        """transforms is not used, as it supports only 2/3D image"""
        img = torch.as_tensor(img)

        return img, name
    
# only this function is used during training   
# RSS only
def get_celeba_loaders(batch_train, batch_test, nr_test, list_p, df_p, data_p, name_exd):
    """
    list_p: path for subject list
    df_p: path for demography info such as age and gender
    data_p: path (a folder) for all images 
    """


    #print(batch_train)
    ##exit(0)
    test_num = nr_test # first test_num samples for validation
    #images = glob.glob(os.path.join(".", "data", "celeba", "images", "*.jpg"))
    # f = open('/home/lau/codes/data/MRI/gz_3d_list.txt','r')      # paths for input
    # file_list = f.readlines()
    # df = pd.read_csv(r'RS_general_data.csv') #TODO prepare a table for labels
    # id_list = df['bigrfullname'].values
    # age_list = df['age'].values
    # gender_list = np.array(df['sex'].values)
    
    file_list = open(list_p).readlines()
    df = pd.read_csv(df_p, index_col='bigrfullname')
    
    #print(id_list, id_list.shape)
    #exit(0)
   # file_list = file_list.replace('\n','')
    out_age_list, out_gender_list, clean_img_list, img_name_list = [], [], [], []
    count_abort = 0
    for i, file_name in enumerate(file_list):
        # if i > 200:
        #     break
        print('-----------%5d'%i)
        file_name = file_name.replace('\n','')
        #split_file_name = file_name.split('/')
        #print(split_file_name[len(split_file_name)-1].replace('_GM_to_template_GM_mod','').replace('.nii.gz',''))
        #index_id = np.argwhere(id_list==split_file_name[len(split_file_name)-1].replace('_GM_to_template_GM_mod','').replace('.nii.gz',''))
        #print(id_list[1],index_id)
        #exit(0)
        
        ##TODO file name for Bo's local: replace('_GM_to_template_GM_mod_GM','')
        ## for cluster: replace('_GM_to_template_GM_mod','')
        index_id = str(file_name.replace('_GM_to_template_GM_mod','').replace('.nii.gz',''))
        # print('index_id: ', index_id)
        abs_file = join(data_p, index_id + '_GM_to_template_GM_mod' + name_exd)
        # print('abs_file: ', abs_file)
        
        
        # if len(index_id) == 0:
        #     #print('a')
        #     count_abort = count_abort + 1
        #     continue

        # temp_age = str(age_list[index_id[0]])
        # temp_age = temp_age.replace('\'','')
        # temp_age = temp_age.replace('[','')
        # temp_age = temp_age.replace(']','')
        # #print(temp_age)

        temp_age = df[df.index==index_id]['age'].values      
        # print('temp_age: ', temp_age)
        if len(temp_age) == 0:
            #print('a')
            count_abort = count_abort + 1
            continue        
        else:
            temp_age = temp_age[0]

        # temp_gender = str(gender_list[index_id[0]])

        # temp_gender = temp_gender.replace('\'','')
        # temp_gender = temp_gender.replace('[','')
        # temp_gender = temp_gender.replace(']','')
        # if temp_gender == ' ':
        #     count_abort = count_abort + 1
        #     continue
        temp_sex = df[df.index==index_id]['sex'].values 
        # print('temp_sex: ', temp_sex)
        # if temp_sex == ' ':
        if len(temp_sex) == 0:
            count_abort = count_abort + 1
            continue
        else:
            temp_sex = temp_sex[0]
        

        out_age_list    = np.append(out_age_list, float(temp_age) )
        out_gender_list = np.append(out_gender_list, int(temp_sex))
        clean_img_list  = np.append(clean_img_list, abs_file) #file_name
        img_name_list   = np.append(img_name_list, index_id)
        #print(img)
        #exit(0)



    print('number of all sample',len(out_age_list))
   # out_age_list_tensor = torch.from_numpy(out_age_list)
   # out_gender_list_tensor = torch.from_numpy(out_gender_list)
    #img_list_tensor = torch.from_numpy(img_list)


    datasets = {
        "train":  ImageDataset(clean_img_list[test_num:], out_age_list[test_num:],out_gender_list[test_num:], img_name_list[test_num:], is_aug=True),
        "test": ImageDataset(clean_img_list[:test_num], out_age_list[:test_num],out_gender_list[:test_num], img_name_list[:test_num], is_aug=False)
    }
    print('number of training sample', len(out_age_list[test_num:] ))
    print('number of validation sample', len(out_age_list[:test_num] ))
    dataloaders = {
        "train": DataLoader(  dataset=datasets["train"],  batch_size=batch_train, shuffle=True),
        "test": DataLoader( dataset=datasets["test"], batch_size=batch_test, shuffle=False)
    }

    return dataloaders

def data_loaders_rssAdni(batch_train, batch_vali, batch_test, data_p, name_exd, train_MRI, train_label, 
                         vali_MRI, vali_label, test_MRI, test_label):
    """
    list_p: path for subject list
    df_p: path for demography info such as age and gender
    data_p: path (a folder) for all images 
    """

    train_img, train_age, train_gender, train_names, train_dem, train_apoe = [], [], [], [], [], []
    vali_img, vali_age, vali_gender, vali_names, vali_dem, vali_apoe       = [], [], [], [], [], []
    test_img, test_age, test_gender, test_names, test_dem, test_apoe       = [], [], [], [], [], []

    for i, file_name in enumerate(train_MRI):
        # if i > 200:
        #     break
        print('-----------%5d'%i)
        train_names   = np.append(train_names, file_name)
        abs_file = join(data_p, file_name + '_GM_to_template_GM_mod' + name_exd)
        train_img    = np.append(train_img, abs_file) #file_name
        
        temp_age  = train_label[train_label.index==file_name]['age'].values      
        # print('temp_age: ', temp_age)
        temp_age  = temp_age[0]
        train_age = np.append(train_age, float(temp_age) )
        
        temp_sex     = train_label[train_label.index==file_name]['sex'].values 
        temp_sex     = temp_sex[0]
        train_gender = np.append(train_gender, int(temp_sex))
        
        dementia   = train_label[train_label.index==file_name]['dementia'].values 
        dementia   = dementia[0]        
        train_dem  = np.append(train_dem, int(dementia))

        apoe       = train_label[train_label.index==file_name]['ApoE4'].values
        apoe       = apoe[0]
        train_apoe = np.append(train_apoe, int(apoe))                

    print('number of train sample',len(train_names))

    for i, file_name in enumerate(vali_MRI):
        # if i > 200:
        #     break
        print('-----------%5d'%i)
        vali_names   = np.append(vali_names, file_name)
        abs_file = join(data_p, file_name + '_GM_to_template_GM_mod' + name_exd)
        vali_img    = np.append(vali_img, abs_file) #file_name
        
        temp_age  = vali_label[vali_label.index==file_name]['age'].values      
        # print('temp_age: ', temp_age)
        temp_age  = temp_age[0]
        vali_age = np.append(vali_age, float(temp_age) )
        
        temp_sex     = vali_label[vali_label.index==file_name]['sex'].values 
        temp_sex     = temp_sex[0]
        vali_gender  = np.append(vali_gender, int(temp_sex))
        
        dementia   = vali_label[vali_label.index==file_name]['dementia'].values 
        dementia   = dementia[0]        
        vali_dem   = np.append(vali_dem, int(dementia))

        apoe       = vali_label[vali_label.index==file_name]['ApoE4'].values
        apoe       = apoe[0]
        vali_apoe  = np.append(vali_apoe, int(apoe))                

    print('number of validation sample',len(vali_names))

    for i, file_name in enumerate(test_MRI):
        # if i > 200:
        #     break
        print('-----------%5d'%i)
        test_names   = np.append(test_names, file_name)
        abs_file = join(data_p, file_name + '_GM_to_template_GM_mod' + name_exd)
        test_img    = np.append(test_img, abs_file) #file_name
        
        temp_age  = test_label[test_label.index==file_name]['age'].values      
        # print('temp_age: ', temp_age)
        temp_age  = temp_age[0]
        test_age = np.append(test_age, float(temp_age) )
        
        temp_sex     = test_label[test_label.index==file_name]['sex'].values 
        temp_sex     = temp_sex[0]
        test_gender  = np.append(test_gender, int(temp_sex))
        
        dementia   = test_label[test_label.index==file_name]['dementia'].values 
        dementia   = dementia[0]        
        test_dem   = np.append(test_dem, int(dementia))

        apoe       = test_label[test_label.index==file_name]['ApoE4'].values
        apoe       = apoe[0]
        test_apoe  = np.append(test_apoe, int(apoe))                

    print('number of test sample',len(test_names))

    datasets = {
        "train":  ImageDataset(train_img, train_age, train_gender, train_names, is_aug=True),
        "validation": ImageDataset(vali_img, vali_age, vali_gender, vali_names, is_aug=True),
        "test": ImageDataset(test_img, test_age, test_gender, test_names, is_aug=False)
    }

    dataloaders = {
        "train": DataLoader(  dataset=datasets["train"],  batch_size=batch_train, shuffle=True),
        "validation": DataLoader(  dataset=datasets["validation"],  batch_size=batch_vali, shuffle=False),
        "test": DataLoader( dataset=datasets["test"], batch_size=batch_test, shuffle=False)
    }

    return dataloaders

## Two dataloaders, one for labeled, another for unlabeled
def data_loaders_rssCognition_labeled(batch_train, batch_vali, batch_test, data_p, name_exd, all_label, 
                     trainLabeled_MRI, testLabeled_MRI):
    """
    list_p: path for subject list
    df_p: path for demography info such as age and gender
    data_p: path (a folder) for all images 
    """

    train_img, train_age, train_sex, train_names, train_tgt, train_eduy = [], [], [], [], [], []
    vali_img, vali_age, vali_sex, vali_names, vali_tgt, vali_eduy       = [], [], [], [], [], []
    # test_img, test_age, test_sex, test_names, test_dem, test_apoe       = [], [], [], [], [], []

    for i, file_name in enumerate(trainLabeled_MRI):
        # if i > 200:
        #     break
        # print('-----------%5d'%i)
        train_names  = np.append(train_names, file_name)
        abs_file     = join(data_p, file_name + '_GM_to_template_GM_mod' + name_exd)
        train_img    = np.append(train_img, abs_file) #file_name
        
        ## confounders        
        temp_age     = all_label[all_label.index==file_name]['Age_scan'].values      #'age'
        # print('temp_age: ', temp_age)
        temp_age     = temp_age[0]
        train_age    = np.append(train_age, float(temp_age) )
        
        temp_sex     = all_label[all_label.index==file_name]['sex'].values 
        temp_sex     = temp_sex[0]
        train_sex    = np.append(train_sex, int(temp_sex))

        eduy         = all_label[all_label.index==file_name]['eduyears'].values
        eduy         = eduy[0]
        train_eduy   = np.append(train_eduy, int(eduy)) 
        
        ## TODO: target variable, e.g., 'dementia', 'Gfactor'
        targetV      = all_label[all_label.index==file_name]['Gfactor'].values 
        targetV      = targetV[0]        
        train_tgt    = np.append(train_tgt, float(targetV))

        # apoe       = all_label[all_label.index==file_name]['ApoE4'].values
        # apoe       = apoe[0]
        # train_apoe = np.append(train_apoe, int(apoe))                

    print('number of train sample',len(train_names))

    for i, file_name in enumerate(testLabeled_MRI):
        # if i > 200:
        #     break
        # print('-----------%5d'%i)
        vali_names   = np.append(vali_names, file_name)
        abs_file     = join(data_p, file_name + '_GM_to_template_GM_mod' + name_exd)
        vali_img     = np.append(vali_img, abs_file) #file_name

        ## confounders        
        temp_age     = all_label[all_label.index==file_name]['Age_scan'].values      
        # print('temp_age: ', temp_age)
        temp_age     = temp_age[0]
        vali_age     = np.append(vali_age, float(temp_age) )
        
        temp_sex     = all_label[all_label.index==file_name]['sex'].values 
        temp_sex     = temp_sex[0]
        vali_sex     = np.append(vali_sex, int(temp_sex))

        eduy         = all_label[all_label.index==file_name]['eduyears'].values
        eduy         = eduy[0]
        vali_eduy    = np.append(vali_eduy, int(eduy)) 
        
        ## TODO: target variable, e.g., 'dementia', 'Gfactor'        
        targetV      = all_label[all_label.index==file_name]['Gfactor'].values 
        targetV      = targetV[0]        
        vali_tgt     = np.append(vali_tgt, float(targetV))

        # apoe         = all_label[all_label.index==file_name]['ApoE4'].values
        # apoe         = apoe[0]
        # vali_apoe    = np.append(vali_apoe, int(apoe))                

    print('number of validation sample',len(vali_names))


    datasets = {
        "train":      ImageDataset_labeled(train_img, train_age, train_sex, train_eduy, train_tgt, train_names, is_aug=True),
        "validation": ImageDataset_labeled(vali_img, vali_age, vali_sex, vali_eduy, vali_tgt, vali_names, is_aug=False)#,
        # "test": ImageDataset(test_img, test_age, test_sex, test_names, is_aug=False)
    }

    dataloaders = {
        "train":      DataLoader(dataset=datasets["train"],  batch_size=batch_train, shuffle=True),
        "validation": DataLoader(dataset=datasets["validation"],  batch_size=batch_vali, shuffle=False)#,
        # "test": DataLoader( dataset=datasets["test"], batch_size=batch_test, shuffle=False)
    }

    return dataloaders

def data_loaders_rssCognition_unlabeled(batch_train, batch_vali, batch_test, data_p, name_exd, all_label, 
                     trainUnLabeled_MRI, testUnLabeled_MRI):
    """
    list_p: path for subject list
    df_p: path for demography info such as age and gender
    data_p: path (a folder) for all images 
    """

    train_img, train_names = [], []
    vali_img,  vali_names  = [], []

    for i, file_name in enumerate(trainUnLabeled_MRI):
        # if i > 200:
        #     break
        # print('-----------%5d'%i)
        train_names  = np.append(train_names, file_name)
        abs_file     = join(data_p, file_name + '_GM_to_template_GM_mod' + name_exd)
        train_img    = np.append(train_img, abs_file) #file_name               
    print('number of train sample',len(train_names))

    for i, file_name in enumerate(testUnLabeled_MRI):
        # if i > 200:
        #     break
        # print('-----------%5d'%i)
        train_names  = np.append(train_names, file_name)
        abs_file     = join(data_p, file_name + '_GM_to_template_GM_mod' + name_exd)
        train_img    = np.append(train_img, abs_file) #file_name              
    print('number of train sample',len(train_names))


    for i, file_name in enumerate(testUnLabeled_MRI):
        # if i > 200:
        #     break
        # print('-----------%5d'%i)
        vali_names   = np.append(vali_names, file_name)
        abs_file     = join(data_p, file_name + '_GM_to_template_GM_mod' + name_exd)
        vali_img     = np.append(vali_img, abs_file) #file_name              
    print('number of validation sample',len(vali_names))

    datasets = {
        "train":      ImageDataset_unlabeled(train_img, train_names, is_aug=True),
        "validation": ImageDataset_unlabeled(vali_img, vali_names, is_aug=True)#,
        # "test": ImageDataset(test_img, test_age, test_sex, test_names, is_aug=False)
    }

    dataloaders = {
        "train":      DataLoader( dataset=datasets["train"],  batch_size=batch_train, shuffle=True),
        "validation": DataLoader( dataset=datasets["validation"],  batch_size=batch_vali, shuffle=False)#,
        # "test": DataLoader( dataset=datasets["test"], batch_size=batch_test, shuffle=False)
    }

    return dataloaders
