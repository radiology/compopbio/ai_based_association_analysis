# -*- coding: utf-8 -*-
"""
Created on Sat Sep 11 16:21:55 2021

preprocess the VBM data from RSS (original size: 197,233, 189) to a smaller and
CNN-usable size

@author: Bo Li
"""

import numpy as np
from os.path import join, exists
from os import makedirs
import subprocess
#import nibabel as nib
#import matplotlib.pyplot as plt

def compute_bbox(data, rtol=1e-8, copy=True, pad=True, return_offset=False):

#    img = check_niimg(img)
#    data = img.get_fdata()
    infinity_norm = max(-data.min(), data.max())
    passes_threshold = np.logical_or(data < -rtol * infinity_norm,
                                     data > rtol * infinity_norm)

    if data.ndim == 4:
        passes_threshold = np.any(passes_threshold, axis=-1)
    coords = np.array(np.where(passes_threshold))

    # Sets full range if no data are found along the axis
    if coords.shape[1] == 0:
        start, end = [0, 0, 0], list(data.shape)
    else:
        start = coords.min(axis=1)
        end = coords.max(axis=1) + 1

    # pad with one voxel to avoid resampling problems
    if pad:
        start = np.maximum(start - 1, 0)
        end = np.minimum(end + 1, data.shape[:3])

    slices = [slice(s, e) for s, e in zip(start, end)][:3]

#    cropped_im = _crop_img_to(img, slices, copy=copy)
#    return cropped_im if not return_offset else (cropped_im, tuple(slices))
    return tuple(slices) #(x(start, end), y(start, end), z(start, end))

"""
p   = r'F:\Representation Learning'
eg = 'ergo5mri_1000_v_8294664_7567_GM_to_template_GM_mod.nii.gz'

img  = nib.load(join(p, eg))
data = img.get_fdata()
plt.hist(data.flatten(),bins=50)

#compute_bbox(data)
#Out[9]: (slice(24, 173, None), slice(25, 210, None), slice(18, 157, None))
"""

"""crop to size (160, 192, 144),
fslroi ergo5mri_1000_v_8294664_7567_GM_to_template_GM_mod.nii.gz test.nii.gz 19 160 21 192 16 144 
"""

# module load Python/3.7.4-GCCcore-8.3.0

cluster_p = '/data/scratch/bli/Representation_learning'
data_p = join(cluster_p, 'data_3d_VBM')
tgt_p  = join(cluster_p, 'data_3d_VBM_crop_160_192_144')
if not exists(tgt_p):
    makedirs(tgt_p)

filelist = np.loadtxt(join(data_p, 'filenames.txt'), dtype='str')

for i, name in enumerate(filelist):
    print("--- %05d, %s"%(i, name))
    
    file_p   = join(data_p, name)
    output_p = join(tgt_p, name)
    command = 'fslroi %s %s 19 160 21 192 16 144'%(file_p, output_p)
    print(command)
    # option 1:
    #os.popen(add_command)
    # option 2:
    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    process.wait()     
    
    # check results
#    c = 'fslinfo %s'%output_p
#    print(c)
#    process = subprocess.Popen(c, shell=True, stdout=subprocess.PIPE)
#    process.wait()     
    
    
#    break