import math
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import torchvision.models as models

layer_names = ['conv1_1', 'relu1_1', 'conv1_2', 'relu1_2', 'pool1',
               'conv2_1', 'relu2_1', 'conv2_2', 'relu2_2', 'pool2',
               'conv3_1', 'relu3_1', 'conv3_2', 'relu3_2', 'conv3_3', 'relu3_3', 'conv3_4', 'relu3_4', 'pool3',
               'conv4_1', 'relu4_1', 'conv4_2', 'relu4_2', 'conv4_3', 'relu4_3', 'conv4_4', 'relu4_4', 'pool4',
               'conv5_1', 'relu5_1', 'conv5_2', 'relu5_2', 'conv5_3', 'relu5_3', 'conv5_4', 'relu5_4', 'pool5']
vae123_layers = ['relu1_1', 'relu2_1', 'relu3_1']
vae345_layers = ['relu3_1', 'relu4_1', 'relu5_1']

class _VGG(nn.Module):
    def __init__(self, model):
        super(_VGG, self).__init__()

        # Load pretrained model
        features = models.vgg19(pretrained=True).features

        # Rename layers
        self.features = nn.Sequential()
        for i, module in enumerate(features):
            name = layer_names[i]
            self.features.add_module(name, module)

        # Disable autograd
        for param in self.features.parameters():
            param.requires_grad = False

        # Content layers
        if model == "vae-123":
            self.content_layers = vae123_layers
        elif model == "vae-345":
            self.content_layers = vae345_layers
        

    def forward(self, inputs):
        batch_size = inputs.size(0)
        all_outputs = []
        output = inputs
        for name, module in self.features.named_children():
            output = module(output)
            if name in self.content_layers:
                all_outputs.append(output.view(batch_size, -1))
        return all_outputs

class KLDLoss(nn.Module):
    def __init__(self, reduction='sum'):
        super(KLDLoss, self).__init__()
        self.reduction = reduction

    def forward(self, mean, logvar):
        # KLD loss
        kld_loss = -0.5 * torch.sum(1 + logvar - mean.pow(2) - logvar.exp(), 1)
        # Size average
        if self.reduction == 'mean':
            kld_loss = torch.mean(kld_loss)
        elif self.reduction == 'sum':
            kld_loss = torch.sum(kld_loss)
        return kld_loss

class FLPLoss(nn.Module):
    def __init__(self, model, device, reduction='sum'):
        super(FLPLoss, self).__init__()
        self.criterion = nn.MSELoss(reduction=reduction)
        self.pretrained = _VGG(model).to(device)
    
    def forward(self, x, recon_x):
        x_f = self.pretrained(x)
        recon_f = self.pretrained(recon_x)
        return self._fpl(recon_f, x_f)

    def _fpl(self, recon_f, x_f):
        fpl = 0
        for _r, _x in zip(recon_f, x_f):
            fpl += self.criterion(_r, _x)
        return fpl


def ncc_loss(y_true, y_pred):

    Ii = y_true
    Ji = y_pred

    # get dimension of volume
    # assumes Ii, Ji are sized [batch_size, *vol_shape, nb_feats]
    ndims = len(list(Ii.size())) - 2
    assert ndims in [1, 2, 3], "volumes should be 1 to 3 dimensions. found: %d" % ndims

    # set window size
    win = [9] * ndims

    # compute filters
    sum_filt = torch.ones([1, 1, *win]).to("cuda")

    pad_no = math.floor(win[0] / 2)
    # pad_no = torch.floor(win[0] / 2)

    if ndims == 1:
        stride = (1)
        padding = (pad_no)
    elif ndims == 2:
        stride = (1, 1)
        padding = (pad_no, pad_no)
    else:
        stride = (1, 1, 1)
        padding = (pad_no, pad_no, pad_no)

    # get convolution function
    conv_fn = getattr(F, 'conv%dd' % ndims)

    # compute CC squares
    I2 = Ii * Ii
    J2 = Ji * Ji
    IJ = Ii * Ji

    I_sum  = conv_fn(Ii, sum_filt, stride=stride, padding=padding)
    J_sum  = conv_fn(Ji, sum_filt, stride=stride, padding=padding)
    I2_sum = conv_fn(I2, sum_filt, stride=stride, padding=padding)
    J2_sum = conv_fn(J2, sum_filt, stride=stride, padding=padding)
    IJ_sum = conv_fn(IJ, sum_filt, stride=stride, padding=padding)

    win_size = np.prod(win)
    # win_size = torch.prod(win)
    
    u_I = I_sum / win_size
    u_J = J_sum / win_size

    cross = IJ_sum - u_J * I_sum - u_I * J_sum + u_I * u_J * win_size
    I_var = I2_sum - 2 * u_I * I_sum + u_I * u_I * win_size
    J_var = J2_sum - 2 * u_J * J_sum + u_J * u_J * win_size

    cc = cross * cross / (I_var * J_var + 1e-5)

    # return -torch.mean(cc)
    ncc = torch.mean(cc)
    return -ncc



def torch_corr(input1, input2, device):

    mean_1 = torch.mean(input1)
    mean_2 = torch.mean(input2)
    var_1 = torch.var(input1) + 1e-5
    var_2 = torch.var(input2) + 1e-5
    vector_mean1 = mean_1*torch.ones([len(input1)]).to(device)
    vector_mean2 = mean_2*torch.ones([len(input2)]).to(device)
    #print(input2, vector_mean2)
    diff_1 = input1 - vector_mean1
    diff_2 = input2 - vector_mean2

    exp = torch.mul(diff_1, diff_2)
    exp = torch.sum(exp)   

    exp = exp / len(input1)
    exp = exp / (torch.sqrt(var_1*var_2) + 1e-5)
    
    return abs(exp)


def correlation_coefficient_loss(y_true, y_pred):
    x = y_true
    y = y_pred
    mx = torch.mean(x)
    my = torch.mean(y)
    xm, ym = x-mx, y-my
    r_num = torch.sum(xm*ym)
    r_den = torch.sqrt(torch.sum(torch.square(xm)) * torch.sum(torch.square(ym))) + 1e-5
    r = r_num / r_den

    if r > 1.0:
        r = 1.0
    if r < -1.0:
        r = -1.0
    return torch.square(r)

# def safe_normalize(x):
#     """Normalize risk scores to avoid exp underflowing.

#     Note that only risk scores relative to each other matter.
#     If minimum risk score is negative, we shift scores so minimum
#     is at zero.
#     """
#     x_min = tf.reduce_min(x, axis=0)
#     c = tf.zeros_like(x_min)
#     norm = tf.where(x_min < 0, -x_min, c)
#     return x + norm

# def CoxPH_loss(y_true, y_pred):
#     """
#     Calculates the Negative Partial Log Likelihood:
    
#     L = sum(h_i - log(sum(e^h_j)))
    
#     where;
#         h_i = risk prediction of patient i
#         h_j is risk prediction of patients j at risk of developing dementia (T_j>=T_i)
    
#     Inputs: 
#     - y_true = label data composed of 
#         y_event: A 1 or 0 indicating if the patient developed dementia or not, and
#         y_riskset:(set of patients j which are at risk dependent on patient i (Tj>=Ti)). 
        
#     - y_pred = the risk prediction of the network
    
#     Output:
#     - loss = the loss used to optimize the network
    
#     """
#     event = y_true[:,0]
#     event = tf.reshape(event,(-1,1))

#     riskset_loss = y_true[:,1:]
#     predictions = y_pred
#     predictions = tf.cast(predictions,tf.float32)
#     riskset_loss = tf.cast(riskset_loss,tf.bool)
#     event = tf.cast(event, predictions.dtype)
#     predictions = safe_normalize(predictions)


#     pred_t = tf.transpose(predictions)
#     # compute log of sum over risk set for each row
#     rr = logsumexp_masked(pred_t, riskset_loss, axis=1, keep_dims=True)
#     loss = tf.multiply(event, rr - predictions)
    
#     return tf.reduce_mean(loss)

def safe_normalize_torch(x):
    """Normalize risk scores to avoid exp underflowing.

    Note that only risk scores relative to each other matter.
    If minimum risk score is negative, we shift scores so minimum
    is at zero.
    """
    ## TODO be careful with axis when convert tf function to pytorch
    ## their tensors have different format
    ## tf: batch, x, y, z, feature
    ## torch: batch, feature, x, y, z
    # https://pytorch.org/docs/stable/generated/torch.min.html
    x_min, indices = torch.min(x, dim=0)
    c     = torch.zeros_like(x_min)
    norm  = torch.where(x_min < 0, -x_min, c)
    return x + norm

def logsumexp_masked_torch(risk_scores, mask, axis=0, keep_dims=None):
    """
    Computes the log of the sum of the exponent of the predictions across `axis` 
    for all entries where `mask` (riskset) is true:
    
    log(sum(e^h_j))
    
    where h_j are the predictions of patients at risk of developing dementia (T_j>=T_i)
    
    Inputs:
    - risk_scores = the predictions from the network of patients h_j
    - mask = a mask to select which patients are at risk
    
    Output:
    - output = right hand part of the NPLL (Negative Partial Log Likelihood)

    """
    risk_scores.shape.assert_same_rank(mask.shape)

    # with tf.name_scope("logsumexp_masked"):
    risk_scores = risk_scores.type(torch.FloatTensor)
    mask_f      = mask.type(torch.FloatTensor)
    risk_scores_masked = torch.mul(risk_scores, mask_f)

    #for numerical stability, substract the maximum value
    #before taking the exponential
    amax = torch.max(risk_scores_masked, dim=axis, keepdim=keep_dims)
    risk_scores_shift = risk_scores_masked - amax
    exp_masked = torch.mul(torch.exp(risk_scores_shift), mask_f)
    exp_sum = torch.sum(exp_masked, dim=axis, keepdim=keep_dims)
    
    #turn 0's to 1's to get rid of inf loss (log(0) = inf)
    condition = torch.ne(exp_sum, 0)   
    exp_sum_clean = torch.where(condition, exp_sum, torch.ones_like(exp_sum))

    output = amax + torch.log(exp_sum_clean)
    if not keep_dims:
        output = torch.squeeze(output, dim=axis)

    return output


def CoxPH_loss_torch(y_true, y_pred):
    """
    Calculates the Negative Partial Log Likelihood:
    
    L = sum(h_i - log(sum(e^h_j)))
    
    where;
        h_i = risk prediction of patient i
        h_j is risk prediction of patients j at risk of developing dementia (T_j>=T_i)
    
    Inputs: 
    - y_true = label data composed of 
        y_event: A 1 or 0 indicating if the patient developed dementia or not, and
        y_riskset:(set of patients j which are at risk dependent on patient i (Tj>=Ti)). 
        
    - y_pred = the risk prediction of the network
    
    Output:
    - loss = the loss used to optimize the network
    
    """
    event = y_true[:,0]
    event = torch.reshape(event,(-1,1))

    riskset_loss = y_true[:,1:]
    predictions  = y_pred.type(torch.FloatTensor)
    #https://stackoverflow.com/questions/59132647/tf-cast-equivalent-in-pytorch
    #https://pytorch.org/docs/stable/tensors.html
    # predictions  = predictions.type(torch.FloatTensor)
    riskset_loss = riskset_loss.type(torch.bool)
    event        = event.type(torch.FloatTensor)
    predictions  = safe_normalize_torch(predictions)

    # https://pytorch.org/docs/stable/generated/torch.transpose.html
    pred_t = torch.transpose(predictions, 0, 1)
    # compute log of sum over risk set for each row
    rr = logsumexp_masked_torch(pred_t, riskset_loss, axis=1, keep_dims=True)
    loss = torch.mul(event, rr - predictions)
    
    return torch.mean(loss)