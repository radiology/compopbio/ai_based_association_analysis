"""
Created on Wed Jan 12 23:30:51 2022
tensorflow-based loss function for brain autoencoder

@author: BL
"""
# import math
import numpy as np
import tensorflow as tf
import keras.backend as K
# from tensorflow.keras.losses import Huber

# NCC
class NCC:
    """
    Local (over window) normalized cross correlation loss.
    """

    def __init__(self, win=None, eps=1e-5):
        self.win = win
        self.eps = eps

    def ncc(self, Ii, Ji):
        # get dimension of volume
        # assumes Ii, Ji are sized [batch_size, *vol_shape, nb_feats]
        ndims = len(Ii.get_shape().as_list()) - 2
        assert ndims in [1, 2, 3], "volumes should be 1 to 3 dimensions. found: %d" % ndims

        # set window size
        if self.win is None:
            self.win = [9] * ndims
        elif not isinstance(self.win, list):  # user specified a single number not a list
            self.win = [self.win] * ndims

        # get convolution function
        conv_fn = getattr(tf.nn, 'conv%dd' % ndims)

        # compute CC squares
        I2 = Ii * Ii
        J2 = Ji * Ji
        IJ = Ii * Ji

        # compute filters
        in_ch = Ji.get_shape().as_list()[-1]
        sum_filt = tf.ones([*self.win, in_ch, 1])
        strides = 1
        if ndims > 1:
            strides = [1] * (ndims + 2)

        # compute local sums via convolution
        padding = 'SAME'
        I_sum = conv_fn(Ii, sum_filt, strides, padding)
        J_sum = conv_fn(Ji, sum_filt, strides, padding)
        I2_sum = conv_fn(I2, sum_filt, strides, padding)
        J2_sum = conv_fn(J2, sum_filt, strides, padding)
        IJ_sum = conv_fn(IJ, sum_filt, strides, padding)

        # compute cross correlation
        win_size = np.prod(self.win) * in_ch
        u_I = I_sum / win_size
        u_J = J_sum / win_size

        # TODO: simplify this
        cross = IJ_sum - u_J * I_sum - u_I * J_sum + u_I * u_J * win_size
        cross = tf.maximum(cross, self.eps)
        I_var = I2_sum - 2 * u_I * I_sum + u_I * u_I * win_size
        I_var = tf.maximum(I_var, self.eps)
        J_var = J2_sum - 2 * u_J * J_sum + u_J * u_J * win_size
        J_var = tf.maximum(J_var, self.eps)

        # cc = (cross * cross) / (I_var * J_var)
        cc = (cross / I_var) * (cross / J_var)

        # return mean cc for each entry in batch
        return tf.reduce_mean(K.batch_flatten(cc), axis=-1)

    def loss(self, y_true, y_pred):
        return - self.ncc(y_true, y_pred)

def cross_corr(I,J):
    eps=1e-5
    ndims = len(I.get_shape().as_list()) - 2
    assert ndims in [1, 2, 3], "volumes should be 1 to 3 dimensions. found: %d" % ndims
    win = [9] * ndims
    conv_fn = getattr(tf.nn, 'conv%dd' % ndims)

    # compute CC squares
    I2 = I*I
    J2 = J*J
    IJ = I*J

    # compute filters
    #TODO fileter=(9,9,9,1,1), maybe should be (1,9,9,9,1)
    sum_filt = tf.ones([*win, 1, 1])
    strides = [1] * (ndims + 2)
    padding = 'SAME'

    # compute local sums via convolution
    I_sum = conv_fn(I, sum_filt, strides, padding)
    J_sum = conv_fn(J, sum_filt, strides, padding)
    I2_sum = conv_fn(I2, sum_filt, strides, padding)
    J2_sum = conv_fn(J2, sum_filt, strides, padding)
    IJ_sum = conv_fn(IJ, sum_filt, strides, padding)
        
    win_size = np.prod(win)
    numerator   = win_size*IJ_sum - I_sum*J_sum
    denom1_     = win_size*I2_sum - I_sum*I_sum
    denom2_     = win_size*J2_sum - J_sum*J_sum

    corr = numerator/tf.sqrt(denom1_*denom2_ + eps)

    # return negative corr.
    return - tf.reduce_mean(corr)

def rec_loss(y_true, y_pred):
    # https://stackoverflow.com/questions/44130871/keras-smooth-l1-loss
    # Smooth L1 in Pytorch is equivalent to Huber in Keras when delta=1
    # #"mean" is not an option, either "auto" or "sum"
    huber = tf.keras.losses.Huber(delta=1.0, reduction="auto") 
    # mae = tf.keras.losses.MeanAbsoluteError(reduction="auto")
    intensity_sim = huber(y_true, y_pred)
    ncc = NCC().loss
    structure_sim = ncc(y_true, y_pred)
    
    return 0.4*intensity_sim + structure_sim

##TODO
# def torch_corr(input1, input2, device):

#     mean_1 = torch.mean(input1)
#     mean_2 = torch.mean(input2)
#     var_1 = torch.var(input1)
#     var_2 = torch.var(input2)
#     vector_mean1 = mean_1*torch.ones([len(input1)]).to(device)
#     vector_mean2 = mean_2*torch.ones([len(input2)]).to(device)
#     #print(input2, vector_mean2)
#     diff_1 = input1 - vector_mean1
#     diff_2 = input2 - vector_mean2

#     exp = torch.mul(diff_1,diff_2)
#     exp = torch.sum(exp)   


#     exp = exp / len(input1)
#     exp = abs(exp / torch.sqrt(var_1*var_2))
#     return exp

##TODO
# def correlation_coefficient_loss(y_true, y_pred):
#     x = y_true
#     y = y_pred
#     mx = torch.mean(x)
#     my = torch.mean(y)
#     xm, ym = x-mx, y-my
#     r_num = torch.sum(xm*ym)
#     r_den = torch.sqrt(torch.sum(torch.square(xm)) * torch.sum(torch.square(ym))) + 1e-5
#     r = r_num / r_den

#     if r > 1.0:
#         r = 1.0
#     if r < -1.0:
#         r = -1.0
#     return torch.square(r)

# def CoxPH_loss(y_true, y_pred):
#     """
#     Calculates the Negative Partial Log Likelihood:
    
#     L = sum(h_i - log(sum(e^h_j)))
    
#     where;
#         h_i = risk prediction of patient i
#         h_j is risk prediction of patients j at risk of developing dementia (T_j>=T_i)
    
#     Inputs: 
#     - y_true = label data composed of 
#         y_event: A 1 or 0 indicating if the patient developed dementia or not, and
#         y_riskset:(set of patients j which are at risk dependent on patient i (Tj>=Ti)). 
        
#     - y_pred = the risk prediction of the network
    
#     Output:
#     - loss = the loss used to optimize the network
    
#     """
#     event = y_true[:,0]
#     event = tf.reshape(event,(-1,1))

#     riskset_loss = y_true[:,1:]
#     predictions = y_pred
#     predictions = tf.cast(predictions,tf.float32)
#     riskset_loss = tf.cast(riskset_loss,tf.bool)
#     event = tf.cast(event, predictions.dtype)
#     predictions = safe_normalize(predictions)


#     pred_t = tf.transpose(predictions)
#     # compute log of sum over risk set for each row
#     rr = logsumexp_masked(pred_t, riskset_loss, axis=1, keep_dims=True)
#     loss = tf.multiply(event, rr - predictions)
    
#     return tf.reduce_mean(loss)

def generate_riskset(event_times):
    """
    Generates the riskset for every individual. Riskset is the set of individuals that have a 
    longer event time and are thus at risk of experiencing the event : Tj>=Ti
    
    Input:
    - label_data = dataframe with file name, event times and other labels that do not get used
    
    Output:
    - riskset = square matrix in which row i is the riskset of individual i compared to all 
    individuals j. Entry is true if Tj>=Ti, so individual j is 'at risk'.
    """
    
    o = np.argsort(-event_times, kind="mergesort")
    n_samples = len(event_times)
    risk_set = np.zeros((n_samples, n_samples), dtype=np.bool_)
    for i_org, i_sort in enumerate(o):
        ti = event_times[i_sort]
        k = i_org
        while k < n_samples and ti == event_times[o[k]]:
            k += 1
        risk_set[i_sort, o[:k]] = True
    return risk_set

# **Function to normalize risk scores**​
def safe_normalize(x):
    """Normalize risk scores to avoid exp underflowing.
​
    Note that only risk scores relative to each other matter.
    If minimum risk score is negative, we shift scores so minimum
    is at zero.
    """
    x_min = tf.reduce_min(x, axis=0)
    c = tf.zeros_like(x_min)
    norm = tf.where(x_min < 0, -x_min, c)
    return x + norm

# **Function to calculate log of sum of exponent of predictions** (right hand side of equation 1.1)
def logsumexp_masked(risk_scores, mask, axis = 0, keepdims= None):
    
    """
    Computes the log of the sum of the exponent of the predictions across `axis` 
    for all entries where `mask` (riskset) is true:
    
    log(sum(e^h_j))
    
    where h_j are the predictions of patients at risk of developing dementia (T_j>=T_i)
    
    Inputs:
    - risk_scores = the predictions from the network of patients h_j
    - mask = a mask to select which patients are at risk
    
    Output:
    - output = right hand part of the NPLL (Negative Partial Log Likelihood)
​
    """
    
    risk_scores.shape.assert_same_rank(mask.shape)
        
    with tf.name_scope("logsumexp_masked"):
        risk_scores = tf.cast(risk_scores,tf.float32)
        mask_f = tf.cast(mask, risk_scores.dtype)
        risk_scores_masked = tf.math.multiply(risk_scores, mask_f)
        
        #for numerical stability, substract the maximum value
        #before taking the exponential
        amax = tf.reduce_max(risk_scores_masked, axis=axis, keepdims=True)
        risk_scores_shift = risk_scores_masked - amax
        exp_masked = tf.math.multiply(tf.math.exp(risk_scores_shift), mask_f)
        exp_sum = tf.reduce_sum(exp_masked, axis=axis, keepdims=True)
        
        #turn 0's to 1's to get rid of inf loss (log(0) = inf)
        condition = tf.not_equal(exp_sum, 0)   
        exp_sum_clean = tf.where(condition, exp_sum, tf.ones_like(exp_sum))
        
        output = amax + tf.math.log(exp_sum_clean)
        if not keepdims:
            output = tf.squeeze(output, axis=axis)
            
    return output

def CoxPH_loss(y_true, y_pred):
    """
    Calculates the Negative Partial Log Likelihood:
    
    L = sum(h_i - log(sum(e^h_j)))
    
    where;
        h_i = risk prediction of patient i
        h_j is risk prediction of patients j at risk of developing dementia (T_j>=T_i)
    
    Inputs: 
    - y_true = label data composed of 
        y_event: A 1 or 0 indicating if the patient developed dementia or not, and
        y_riskset:(set of patients j which are at risk dependent on patient i (Tj>=Ti)). 
        
    - y_pred = the risk prediction of the network
    
    Output:
    - loss = the loss used to optimize the network
    
    """
    event = y_true[:,0]
    event = tf.reshape(event,(-1,1))
    
    riskset_loss = y_true[:,1:]
    predictions = y_pred
    predictions = tf.cast(predictions,tf.float32)
    riskset_loss = tf.cast(riskset_loss,tf.bool)
    event = tf.cast(event, predictions.dtype)
    predictions = safe_normalize(predictions)

#    with tf.name_scope("assertions"):
#        assertions = (
#            tf.debugging.assert_less_equal(event, 1.),
#            tf.debugging.assert_greater_equal(event, 0.),
#            tf.debugging.assert_type(riskset_loss, tf.bool)
#        )
    # move batch dimension to the end so predictions get broadcast
    # row-wise when multiplying by riskset
    pred_t = tf.transpose(predictions)
    # compute log of sum over risk set for each row
    rr = logsumexp_masked(pred_t, riskset_loss, axis=1, keepdims=True)
#    print(predictions.shape.as_list())
#    assert rr.shape.as_list() == predictions.shape.as_list()

    loss = tf.math.multiply(event, rr - predictions)
    
    return loss
