"""
Two 3D VAE models for brain MRI analysis, using convolutional transpose (i.e., VAE_3d_convTrans)
and trilinear upsampling (i.e., VAE_3d_linearUp) in the decoder, respectively.

author: BL, created on Sep 28, 2021, 23:00
"""

import torch
import torch.nn as nn
# import torch.nn.functional as F
from torch.autograd import Variable
# import torchvision.models as models

class Encoder_x16(nn.Module):
    def __init__(self, nc, nef, nz, isize, device, variational):
        super(Encoder_x16, self).__init__()

        # Device
        self.device = device
        self.variational = variational
        # Encoder: (nc, isize, isize) -> (nef*8, isize//16, isize//16)
        # originally kernel size (4,4), stride 2, nef=32
        # https://pytorch.org/docs/stable/generated/torch.nn.Conv3d.html#torch.nn.Conv3d
        self.encoder = nn.Sequential(
            # layer 1
            nn.Conv3d(nc, nef, 3, 1, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef),
            nn.Conv3d(nef, nef*2, 4, 2, padding=1), # has to be 4
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef*2),
            # layer 2
            nn.Conv3d(nef*2, nef*2, 3, 1, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef*2),
            nn.Conv3d(nef*2, nef*4, 4, 2, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef*4),
            # layer 3
            nn.Conv3d(nef*4, nef*4, 3, 1, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef*4),
            nn.Conv3d(nef*4, nef*8, 4, 2, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef*8),
            # layer 4
            nn.Conv3d(nef*8, nef*8, 3, 1, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef*8),
            nn.Conv3d(nef*8, nef*16, 4, 2, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef*16), 
            # layer 5, flat bootle neck, no more downsamples
            nn.Conv3d(nef*16, nef*16, 3, 1, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef*16)
        )

        # Map the encoded feature map to the latent vector of mean, (log)variance
        out_size = (isize[0] // 16) * (isize[1] // 16) * (isize[2] // 16)
        self.mean = nn.Linear(nef*16*out_size, nz)
        self.logvar = nn.Linear(nef*16*out_size, nz)

    def reparametrize(self, mean, logvar):
        std = logvar.mul(0.5).exp_()
        multi_norm = torch.FloatTensor(std.size()).normal_().to(self.device)
        multi_norm = Variable(multi_norm)
        return multi_norm.mul(std).add_(mean)

    def forward(self, inputs):
        # Batch size
        batch_size = inputs.size(0)
        # Encoded feature map
        hidden = self.encoder(inputs)
        # Reshape
        hidden = hidden.view(batch_size, -1)
        # Calculate mean and (log)variance
        mean, logvar = self.mean(hidden), self.logvar(hidden)
        # Sample
        if self.variational == True:
            latent_z = self.reparametrize(mean, logvar)
        else:
            latent_z = mean
        
        return latent_z, mean, logvar

class Encoder_x16_lkrelu(nn.Module):
    def __init__(self, nc, nef, nz, isize, device, variational):
        super(Encoder_x16_lkrelu, self).__init__()

        # Device
        self.device = device
        self.variational = variational
        # Encoder: (nc, isize, isize) -> (nef*8, isize//16, isize//16)
        # originally kernel size (4,4), stride 2, nef=32
        # https://pytorch.org/docs/stable/generated/torch.nn.Conv3d.html#torch.nn.Conv3d
        self.encoder = nn.Sequential(
            # layer 1
            nn.Conv3d(nc, nef, 3, 1, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef),
            nn.Conv3d(nef, nef*2, 4, 2, padding=1), # has to be 4
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef*2),
            # layer 2
            nn.Conv3d(nef*2, nef*2, 3, 1, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef*2),
            nn.Conv3d(nef*2, nef*4, 4, 2, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef*4),
            # layer 3
            nn.Conv3d(nef*4, nef*4, 3, 1, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef*4),
            nn.Conv3d(nef*4, nef*8, 4, 2, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef*8),
            # layer 4
            nn.Conv3d(nef*8, nef*8, 3, 1, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef*8),
            nn.Conv3d(nef*8, nef*16, 4, 2, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef*16), 
            # layer 5, flat bootle neck, no more downsamples
            nn.Conv3d(nef*16, nef*16, 3, 1, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef*16)
        )

        # Map the encoded feature map to the latent vector of mean, (log)variance
        out_size = (isize[0] // 16) * (isize[1] // 16) * (isize[2] // 16)
        self.mean = nn.Linear(nef*16*out_size, nz)
        self.logvar = nn.Linear(nef*16*out_size, nz)

    def reparametrize(self, mean, logvar):
        std = logvar.mul(0.5).exp_()
        multi_norm = torch.FloatTensor(std.size()).normal_().to(self.device)
        multi_norm = Variable(multi_norm)
        return multi_norm.mul(std).add_(mean)

    def forward(self, inputs):
        # Batch size
        batch_size = inputs.size(0)
        # Encoded feature map
        hidden = self.encoder(inputs)
        # Reshape
        hidden = hidden.view(batch_size, -1)
        # Calculate mean and (log)variance
        mean, logvar = self.mean(hidden), self.logvar(hidden)
        # Sample
        if self.variational == True:
            latent_z = self.reparametrize(mean, logvar)
        else:
            # new Jan 17, 2022
            latent_z = nn.LeakyReLU(0.2, True)(mean)
        
        return latent_z, mean, logvar


class Encoder(nn.Module):
    def __init__(self, nc, nef, nz, isize, device, variational):
        super(Encoder, self).__init__()

        # Device
        self.device = device
        self.variational = variational
        # Encoder: (nc, isize, isize) -> (nef*8, isize//16, isize//16)
        # originally kernel size (4,4), stride 2, nef=32
        # https://pytorch.org/docs/stable/generated/torch.nn.Conv3d.html#torch.nn.Conv3d
        self.encoder = nn.Sequential(
            # layer 1
            nn.Conv3d(nc, nef, 3, 1, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef),
            nn.Conv3d(nef, nef*2, 4, 2, padding=1), # has to be 4
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef*2),
            # layer 2
            nn.Conv3d(nef*2, nef*2, 3, 1, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef*2),
            nn.Conv3d(nef*2, nef*4, 4, 2, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef*4),
            # layer 3
            nn.Conv3d(nef*4, nef*4, 3, 1, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef*4),
            nn.Conv3d(nef*4, nef*8, 4, 2, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef*8),
            # layer 4
            nn.Conv3d(nef*8, nef*8, 3, 1, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef*8),
            nn.Conv3d(nef*8, nef*8, 4, 2, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef*8), 
            # layer 5, flat bootle neck, no more downsamples
            nn.Conv3d(nef*8, nef*8, 3, 1, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(nef*8)
        )

        # Map the encoded feature map to the latent vector of mean, (log)variance
        out_size = (isize[0] // 16) * (isize[1] // 16) * (isize[2] // 16)
        self.mean = nn.Linear(nef*8*out_size, nz)
        self.logvar = nn.Linear(nef*8*out_size, nz)

    def reparametrize(self, mean, logvar):
        std = logvar.mul(0.5).exp_()
        multi_norm = torch.FloatTensor(std.size()).normal_().to(self.device)
        multi_norm = Variable(multi_norm)
        return multi_norm.mul(std).add_(mean)

    def forward(self, inputs):
        # Batch size
        batch_size = inputs.size(0)
        # Encoded feature map
        hidden = self.encoder(inputs)
        # Reshape
        hidden = hidden.view(batch_size, -1)
        # Calculate mean and (log)variance
        mean, logvar = self.mean(hidden), self.logvar(hidden)
        # Sample
        if self.variational == True:
            latent_z = self.reparametrize(mean, logvar)
        else:
            latent_z = mean
        
        return latent_z, mean, logvar

class Decoder(nn.Module):
    def __init__(self, nc, ndf, nz, isize):
        super(Decoder, self).__init__()

        # Map the latent vector to the feature map space
        #TODO attension, the following code indicates that ndf has to equal to nef
        self.ndf = ndf
        self.isize = isize
        self.out_size = (isize[0] // 16) * (isize[1] // 16) * (isize[2] // 16)
        self.decoder_dense = nn.Sequential(
            nn.Linear(nz, ndf*8*self.out_size),
            nn.ReLU(True)
        )
        # Decoder: (ndf*8, isize//16, isize//16) -> (nc, isize, isize)
        # https://pytorch.org/docs/stable/generated/torch.nn.ConvTranspose3d.html#torch.nn.ConvTranspose3d
        self.decoder_conv = nn.Sequential(
            #nn.UpsamplingNearest3d(scale_factor=2),
            nn.ConvTranspose3d(ndf*8, ndf*8, 3, stride=2, padding=1, 
                               output_padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(ndf*8, 1.e-3),            
            nn.Conv3d(ndf*8, ndf*4, 3, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(ndf*4, 1.e-3),

            #nn.UpsamplingNearest3d(scale_factor=2),
            nn.ConvTranspose3d(ndf*4, ndf*4, 3, stride=2, padding=1, 
                               output_padding=1),            
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(ndf*4, 1.e-3),
            nn.Conv3d(ndf*4, ndf*2, 3, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(ndf*2, 1.e-3),
        
            #nn.UpsamplingNearest3d(scale_factor=2),
            nn.ConvTranspose3d(ndf*2, ndf*2, 3, stride=2, padding=1, 
                               output_padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(ndf*2, 1.e-3),                               
            nn.Conv3d(ndf*2, ndf, 3, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(ndf, 1.e-3),

            #nn.UpsamplingNearest3d(scale_factor=2),
            nn.ConvTranspose3d(ndf, ndf, 3, stride=2, padding=1, 
                               output_padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(ndf, 1.e-3),                               
            nn.Conv3d(ndf, nc, 3, padding=1),
            
            nn.ReLU(True) # input image range [0, n)
        )

    def forward(self, input):
        batch_size = input.size(0)
        hidden = self.decoder_dense(input).view(
            batch_size, self.ndf*8, self.isize[0]//16, self.isize[1]//16, self.isize[2]//16) #TODO tuple
        output = self.decoder_conv(hidden)
        return output


class Decoder_x16(nn.Module):
    def __init__(self, nc, ndf, nz, isize):
        super(Decoder_x16, self).__init__()

        # Map the latent vector to the feature map space
        #TODO attension, the following code indicates that ndf has to equal to nef
        self.ndf = ndf
        self.isize = isize
        self.out_size = (isize[0] // 16) * (isize[1] // 16) * (isize[2] // 16)
        self.decoder_dense = nn.Sequential(
            nn.Linear(nz, ndf*16*self.out_size),
            nn.ReLU(True)
        )
        # Decoder: (ndf*8, isize//16, isize//16) -> (nc, isize, isize)
        # https://pytorch.org/docs/stable/generated/torch.nn.ConvTranspose3d.html#torch.nn.ConvTranspose3d
        self.decoder_conv = nn.Sequential(
            #nn.UpsamplingNearest3d(scale_factor=2),
            nn.ConvTranspose3d(ndf*16, ndf*16, 3, stride=2, padding=1, 
                               output_padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(ndf*16, 1.e-3),            
            nn.Conv3d(ndf*16, ndf*8, 3, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(ndf*8, 1.e-3),

            #nn.UpsamplingNearest3d(scale_factor=2),
            nn.ConvTranspose3d(ndf*8, ndf*8, 3, stride=2, padding=1, 
                               output_padding=1),            
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(ndf*8, 1.e-3),
            nn.Conv3d(ndf*8, ndf*4, 3, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(ndf*4, 1.e-3),
        
            #nn.UpsamplingNearest3d(scale_factor=2),
            nn.ConvTranspose3d(ndf*4, ndf*4, 3, stride=2, padding=1, 
                               output_padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(ndf*4, 1.e-3),                               
            nn.Conv3d(ndf*4, ndf*2, 3, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(ndf*2, 1.e-3),

            #nn.UpsamplingNearest3d(scale_factor=2),
            nn.ConvTranspose3d(ndf*2, ndf*2, 3, stride=2, padding=1, 
                               output_padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(ndf*2, 1.e-3),                               
            nn.Conv3d(ndf*2, ndf, 3, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(ndf, 1.e-3),            
            
            nn.Conv3d(ndf, nc, 3, padding=1),
            
            nn.ReLU(True) # input image range [0, n)
        )

    def forward(self, input):
        batch_size = input.size(0)
        hidden = self.decoder_dense(input).view(
            batch_size, self.ndf*16, self.isize[0]//16, self.isize[1]//16, self.isize[2]//16) #TODO tuple
        output = self.decoder_conv(hidden)
        return output

class Decoder_x16_lkrelu(nn.Module):
    def __init__(self, nc, ndf, nz, isize):
        super(Decoder_x16_lkrelu, self).__init__()

        # Map the latent vector to the feature map space
        #TODO attension, the following code indicates that ndf has to equal to nef
        self.ndf = ndf
        self.isize = isize
        self.out_size = (isize[0] // 16) * (isize[1] // 16) * (isize[2] // 16)
        self.decoder_dense = nn.Sequential(
            nn.Linear(nz, ndf*16*self.out_size),
            # nn.ReLU(True)
            # new Jan 17, 2022
            nn.LeakyReLU(0.2, True)
        )
        # Decoder: (ndf*8, isize//16, isize//16) -> (nc, isize, isize)
        # https://pytorch.org/docs/stable/generated/torch.nn.ConvTranspose3d.html#torch.nn.ConvTranspose3d
        self.decoder_conv = nn.Sequential(
            #nn.UpsamplingNearest3d(scale_factor=2),
            nn.ConvTranspose3d(ndf*16, ndf*16, 3, stride=2, padding=1, 
                               output_padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(ndf*16, 1.e-3),            
            nn.Conv3d(ndf*16, ndf*8, 3, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(ndf*8, 1.e-3),

            #nn.UpsamplingNearest3d(scale_factor=2),
            nn.ConvTranspose3d(ndf*8, ndf*8, 3, stride=2, padding=1, 
                               output_padding=1),            
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(ndf*8, 1.e-3),
            nn.Conv3d(ndf*8, ndf*4, 3, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(ndf*4, 1.e-3),
        
            #nn.UpsamplingNearest3d(scale_factor=2),
            nn.ConvTranspose3d(ndf*4, ndf*4, 3, stride=2, padding=1, 
                               output_padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(ndf*4, 1.e-3),                               
            nn.Conv3d(ndf*4, ndf*2, 3, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(ndf*2, 1.e-3),

            #nn.UpsamplingNearest3d(scale_factor=2),
            nn.ConvTranspose3d(ndf*2, ndf*2, 3, stride=2, padding=1, 
                               output_padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(ndf*2, 1.e-3),                               
            nn.Conv3d(ndf*2, ndf, 3, padding=1),
            nn.LeakyReLU(0.2, True),
            nn.BatchNorm3d(ndf, 1.e-3),            
            
            nn.Conv3d(ndf, nc, 3, padding=1),
            
            nn.ReLU(True) # input image range [0, n)
        )

    def forward(self, input):
        batch_size = input.size(0)
        hidden = self.decoder_dense(input).view(
            batch_size, self.ndf*16, self.isize[0]//16, self.isize[1]//16, self.isize[2]//16) #TODO tuple
        output = self.decoder_conv(hidden)
        return output
    
class Decoder_linearUp(nn.Module):
    def __init__(self, nc, ndf, nz, isize, alpha=0.1, eps=1e-5):
        super(Decoder_linearUp, self).__init__()

        # Map the latent vector to the feature map space
        #TODO attension, the following code indicates that ndf has to equal to nef
        self.ndf = ndf
        self.isize = isize
        self.out_size = (isize[0] // 16) * (isize[1] // 16) * (isize[2] // 16)
        self.decoder_dense = nn.Sequential(
            nn.Linear(nz, ndf*8*self.out_size),
            nn.ReLU(True)
        
        )
        # Decoder: (ndf*8, isize//16, isize//16) -> (nc, isize, isize)
        # https://pytorch.org/docs/stable/generated/torch.nn.ConvTranspose3d.html#torch.nn.ConvTranspose3d
        # https://pytorch.org/docs/stable/generated/torch.nn.Upsample.html?highlight=upsampling
        """changes: 1. trilinear upsampling instead of nearest
                    2. eps in BN, use default, i.e., 1e-5, instead of -3
        
        """
        self.decoder_conv = nn.Sequential(
#            nn.UpsamplingNearest3d(scale_factor=2),
#            nn.ConvTranspose3d(ndf*8, ndf*8, 3, stride=2, padding=1, 
#                               output_padding=1),            
            nn.Upsample(scale_factor=2, mode='trilinear'),
            nn.Conv3d(ndf*8, ndf*8, 3, padding=1),            
            nn.LeakyReLU(alpha, True),
            nn.BatchNorm3d(ndf*8, eps),            
            nn.Conv3d(ndf*8, ndf*4, 3, padding=1),
            nn.LeakyReLU(alpha, True),
            nn.BatchNorm3d(ndf*4, eps),

            #nn.UpsamplingNearest3d(scale_factor=2),
            #nn.ConvTranspose3d(ndf*4, ndf*4, 3, stride=2, padding=1, 
            #                   output_padding=1),            
            nn.Upsample(scale_factor=2, mode='trilinear'),
            nn.Conv3d(ndf*4, ndf*4, 3, padding=1),             
            nn.LeakyReLU(alpha, True),
            nn.BatchNorm3d(ndf*4, eps),
            nn.Conv3d(ndf*4, ndf*2, 3, padding=1),
            nn.LeakyReLU(alpha, True),
            nn.BatchNorm3d(ndf*2, eps),
        
            #nn.UpsamplingNearest3d(scale_factor=2),
            #nn.ConvTranspose3d(ndf*2, ndf*2, 3, stride=2, padding=1, 
            #                   output_padding=1),
            nn.Upsample(scale_factor=2, mode='trilinear'),
            nn.Conv3d(ndf*2, ndf*2, 3, padding=1), 
            nn.LeakyReLU(alpha, True),
            nn.BatchNorm3d(ndf*2, eps),                               
            nn.Conv3d(ndf*2, ndf, 3, padding=1),
            nn.LeakyReLU(alpha, True),
            nn.BatchNorm3d(ndf, eps),

            #nn.UpsamplingNearest3d(scale_factor=2),
            #nn.ConvTranspose3d(ndf, ndf, 3, stride=2, padding=1, 
            #                   output_padding=1),
            nn.Upsample(scale_factor=2, mode='trilinear'),
            nn.Conv3d(ndf, ndf, 3, padding=1), 
            nn.LeakyReLU(alpha, True),
            nn.BatchNorm3d(ndf, eps),                               
            nn.Conv3d(ndf, nc, 3, padding=1),
            
            nn.ReLU(True) # input image range [0, n)
        )

    def forward(self, input):
        batch_size = input.size(0)
        hidden = self.decoder_dense(input).view(
            batch_size, self.ndf*8, self.isize[0]//16, self.isize[1]//16, self.isize[2]//16) #TODO tuple
        output = self.decoder_conv(hidden)
        return output
    
class Decoder_linearUp_x16(nn.Module):
    def __init__(self, nc, ndf, nz, isize, alpha=0.1, eps=1e-5):
        super(Decoder_linearUp_x16, self).__init__()

        # Map the latent vector to the feature map space
        #TODO attension, the following code indicates that ndf has to equal to nef
        self.ndf = ndf
        self.isize = isize
        self.out_size = (isize[0] // 16) * (isize[1] // 16) * (isize[2] // 16)
        self.decoder_dense = nn.Sequential(
            nn.Linear(nz, ndf*16*self.out_size),
            nn.ReLU(True)
        
        )
        # Decoder: (ndf*8, isize//16, isize//16) -> (nc, isize, isize)
        # https://pytorch.org/docs/stable/generated/torch.nn.ConvTranspose3d.html#torch.nn.ConvTranspose3d
        # https://pytorch.org/docs/stable/generated/torch.nn.Upsample.html?highlight=upsampling
        """changes: 1. trilinear upsampling instead of nearest
                    2. eps in BN, use default, i.e., 1e-5, instead of -3
        
        """
        self.decoder_conv = nn.Sequential(
#            nn.UpsamplingNearest3d(scale_factor=2),
#            nn.ConvTranspose3d(ndf*8, ndf*8, 3, stride=2, padding=1, 
#                               output_padding=1),            
            nn.Upsample(scale_factor=2, mode='trilinear'),
            nn.Conv3d(ndf*16, ndf*16, 3, padding=1),            
            nn.LeakyReLU(alpha, True),
            nn.BatchNorm3d(ndf*16, eps),            
            nn.Conv3d(ndf*16, ndf*8, 3, padding=1),
            nn.LeakyReLU(alpha, True),
            nn.BatchNorm3d(ndf*8, eps),

            #nn.UpsamplingNearest3d(scale_factor=2),
            #nn.ConvTranspose3d(ndf*4, ndf*4, 3, stride=2, padding=1, 
            #                   output_padding=1),            
            nn.Upsample(scale_factor=2, mode='trilinear'),
            nn.Conv3d(ndf*8, ndf*8, 3, padding=1),             
            nn.LeakyReLU(alpha, True),
            nn.BatchNorm3d(ndf*8, eps),
            nn.Conv3d(ndf*8, ndf*4, 3, padding=1),
            nn.LeakyReLU(alpha, True),
            nn.BatchNorm3d(ndf*4, eps),
        
            #nn.UpsamplingNearest3d(scale_factor=2),
            #nn.ConvTranspose3d(ndf*2, ndf*2, 3, stride=2, padding=1, 
            #                   output_padding=1),
            nn.Upsample(scale_factor=2, mode='trilinear'),
            nn.Conv3d(ndf*4, ndf*4, 3, padding=1), 
            nn.LeakyReLU(alpha, True),
            nn.BatchNorm3d(ndf*4, eps),                               
            nn.Conv3d(ndf*4, ndf*2, 3, padding=1),
            nn.LeakyReLU(alpha, True),
            nn.BatchNorm3d(ndf*2, eps),

            #nn.UpsamplingNearest3d(scale_factor=2),
            #nn.ConvTranspose3d(ndf, ndf, 3, stride=2, padding=1, 
            #                   output_padding=1),
            nn.Upsample(scale_factor=2, mode='trilinear'),
            nn.Conv3d(ndf*2, ndf*2, 3, padding=1), 
            nn.LeakyReLU(alpha, True),
            nn.BatchNorm3d(ndf*2, eps),                               
            nn.Conv3d(ndf*2, ndf, 3, padding=1),
            nn.LeakyReLU(alpha, True),
            nn.BatchNorm3d(ndf, eps),           
            
            nn.Conv3d(ndf, nc, 3, padding=1),
            
            nn.ReLU(True) # input image range [0, n)
        )

    def forward(self, input):
        batch_size = input.size(0)
        hidden = self.decoder_dense(input).view(
            batch_size, self.ndf*16, self.isize[0]//16, self.isize[1]//16, self.isize[2]//16) #TODO tuple
        output = self.decoder_conv(hidden)
        return output

    
class VAE_3d_convTrans(nn.Module):
    """
    nc: number output channel
    ndf: number of filters in decoder
    nef: number of filters in encoder
    nz: size of latent features
    isize: input and output image size
    """
#    def __init__(self, nc=1, ndf=32, nef=32, nz=100, isize=64, device=torch.device("cuda:0"), is_train=True):
#        super(VAE, self).__init__()
    # ndf=16 or 8, test memory need
    def __init__(self, nc=1, ndf=16, nef=16, nz=256, isize=[160,192,144],#isize=[144,160,192], #nz=64
                 device=torch.device("cuda"), is_train=True, variational=False):
        super(VAE_3d_convTrans, self).__init__()
        self.nz = nz
        # Encoder
        self.encoder = Encoder(nc=nc, nef=nef, nz=nz, isize=isize, device=device, variational=variational)
        # Decoder
        self.decoder = Decoder(nc=nc, ndf=ndf, nz=nz, isize=isize)
        self.pe = nn.Linear(nz, 1)
        if is_train == False:
            for param in self.encoder.parameters():
                param.requires_grad = False
            for param in self.decoder.parameters():
                param.requires_grad = False

    def forward(self, x):
        latent_z, mean, logvar = self.encoder(x)
        rec_x = self.decoder(latent_z)
        zp = self.pe(latent_z)
        return rec_x
    
    def encode(self, x):
        latent_z, _, _ = self.encoder(x)
        return latent_z

    def decode(self, z):
        return self.decoder(z)

## best performance for unsupervised learning
class VAE_3d_convTrans_x16(nn.Module):
    """
    nc: number output channel
    ndf: number of filters in decoder
    nef: number of filters in encoder
    nz: size of latent features
    isize: input and output image size
    """
#    def __init__(self, nc=1, ndf=32, nef=32, nz=100, isize=64, device=torch.device("cuda:0"), is_train=True):
#        super(VAE, self).__init__()
    # ndf=16 or 8, test memory need
    def __init__(self, nc=1, ndf=16, nef=16, nz=256, isize=[160,192,144], #nz=64
                 device=torch.device("cuda"), is_train=True, variational=False):
        super(VAE_3d_convTrans_x16, self).__init__()
        self.nz = nz
        # Encoder
        self.encoder = Encoder_x16(nc=nc, nef=nef, nz=nz, isize=isize, device=device, variational=variational)
        # Decoder
        self.decoder = Decoder_x16(nc=nc, ndf=ndf, nz=nz, isize=isize)
        self.pe = nn.Linear(nz, 1)
        if is_train == False:
            for param in self.encoder.parameters():
                param.requires_grad = False
            for param in self.decoder.parameters():
                param.requires_grad = False

    def forward(self, x):
        latent_z, mean, logvar = self.encoder(x)
        rec_x = self.decoder(latent_z)
        zp = self.pe(latent_z)
        return rec_x
    
    def encode(self, x):
        latent_z, _, _ = self.encoder(x)
        return latent_z

    def decode(self, z):
        return self.decoder(z)

class VAE_3d_convTrans_x16_lkrelu(nn.Module):
    """
    nc: number output channel
    ndf: number of filters in decoder
    nef: number of filters in encoder
    nz: size of latent features
    isize: input and output image size
    """
#    def __init__(self, nc=1, ndf=32, nef=32, nz=100, isize=64, device=torch.device("cuda:0"), is_train=True):
#        super(VAE, self).__init__()
    # ndf=16 or 8, test memory need
    def __init__(self, nc=1, ndf=16, nef=16, nz=256, isize=[160,192,144], #nz=64
                 device=torch.device("cuda"), is_train=True, variational=False):
        super(VAE_3d_convTrans_x16_lkrelu, self).__init__()
        self.nz = nz
        # Encoder
        self.encoder = Encoder_x16_lkrelu(nc, nef, nz, isize, device, variational)(nc=nc, nef=nef, nz=nz, isize=isize, device=device, variational=variational)
        # Decoder
        self.decoder = Decoder_x16_lkrelu(nc, ndf, nz, isize)(nc=nc, ndf=ndf, nz=nz, isize=isize)
        self.pe = nn.Linear(nz, 1)
        if is_train == False:
            for param in self.encoder.parameters():
                param.requires_grad = False
            for param in self.decoder.parameters():
                param.requires_grad = False

    def forward(self, x):
        latent_z, mean, logvar = self.encoder(x)
        rec_x = self.decoder(latent_z)
        zp = self.pe(latent_z)
        return rec_x
    
    def encode(self, x):
        latent_z, _, _ = self.encoder(x)
        return latent_z

    def decode(self, z):
        return self.decoder(z)
    
class VAE_3d_linearUp(nn.Module):
    """
    nc: number output channel
    ndf: number of filters in decoder
    nef: number of filters in encoder
    nz: size of latent features
    isize: input and output image size
    """
#    def __init__(self, nc=1, ndf=32, nef=32, nz=100, isize=64, device=torch.device("cuda:0"), is_train=True):
#        super(VAE, self).__init__()
    # ndf=16 or 8, test memory need
    def __init__(self, nc=1, ndf=16, nef=16, nz=256, isize=[160,192,144], 
                 device=torch.device("cuda"), is_train=True, variational=False):
        super(VAE_3d_linearUp, self).__init__()
        self.nz = nz
        # Encoder
        self.encoder = Encoder(nc=nc, nef=nef, nz=nz, isize=isize, device=device, variational=variational)
        # Decoder
        self.decoder = Decoder_linearUp(nc=nc, ndf=ndf, nz=nz, isize=isize,
                                        alpha=0.1, eps=1e-5)
        self.pe = nn.Linear(nz, 1)
        if is_train == False:
            for param in self.encoder.parameters():
                param.requires_grad = False
            for param in self.decoder.parameters():
                param.requires_grad = False

    def forward(self, x):
        latent_z, mean, logvar = self.encoder(x)
        rec_x = self.decoder(latent_z)
        zp = self.pe(latent_z)
        return rec_x
    
    def encode(self, x):
        latent_z, _, _ = self.encoder(x)
        return latent_z

    def decode(self, z):
        return self.decoder(z)


class VAE_3d_linearUp_x16(nn.Module):
    """
    nc: number output channel
    ndf: number of filters in decoder
    nef: number of filters in encoder
    nz: size of latent features
    isize: input and output image size
    """
#    def __init__(self, nc=1, ndf=32, nef=32, nz=100, isize=64, device=torch.device("cuda:0"), is_train=True):
#        super(VAE, self).__init__()
    # ndf=16 or 8, test memory need
    def __init__(self, nc=1, ndf=16, nef=16, nz=256, isize=[160,192,144], 
                 device=torch.device("cuda"), is_train=True, variational=False):
        super(VAE_3d_linearUp_x16, self).__init__()
        self.nz = nz
        # Encoder
        self.encoder = Encoder_x16(nc=nc, nef=nef, nz=nz, isize=isize, device=device, variational=variational)
        # Decoder
        self.decoder = Decoder_linearUp_x16(nc=nc, ndf=ndf, nz=nz, isize=isize,
                                        alpha=0.1, eps=1e-5)
        self.pe = nn.Linear(nz, 1)
        if is_train == False:
            for param in self.encoder.parameters():
                param.requires_grad = False
            for param in self.decoder.parameters():
                param.requires_grad = False

    def forward(self, x):
        latent_z, mean, logvar = self.encoder(x)
        rec_x = self.decoder(latent_z)
        zp = self.pe(latent_z)
        return rec_x
    
    def encode(self, x):
        latent_z, _, _ = self.encoder(x)
        return latent_z

    def decode(self, z):
        return self.decoder(z)
    
class VAE_3d_convTrans_x16_zp(nn.Module):
    """
    nc: number output channel
    ndf: number of filters in decoder
    nef: number of filters in encoder
    nz: size of latent features
    isize: input and output image size
    """
#    def __init__(self, nc=1, ndf=32, nef=32, nz=100, isize=64, device=torch.device("cuda:0"), is_train=True):
#        super(VAE, self).__init__()
    # ndf=16 or 8, test memory need
    def __init__(self, nc=1, ndf=16, nef=16, nz=256, isize=[160,192,144], #nz=64
                 device=torch.device("cuda"), is_train=True, variational=False):
        super(VAE_3d_convTrans_x16_zp, self).__init__()
        self.nz = nz
        # Encoder
        self.encoder = Encoder_x16(nc=nc, nef=nef, nz=nz, isize=isize, device=device, variational=variational)
        # Decoder
        self.decoder = Decoder_x16(nc=nc, ndf=ndf, nz=nz, isize=isize)
        self.pe = nn.Linear(nz, 1, bias=False)
        if is_train == False:
            for param in self.encoder.parameters():
                param.requires_grad = False
            for param in self.decoder.parameters():
                param.requires_grad = False
            for param in self.pe.parameters():
                param.requires_grad = False                

    def forward(self, x):
        latent_z, mean, logvar = self.encoder(x)
        rec_x                  = self.decoder(latent_z)
        zp                     = self.pe(latent_z)    
        
        return rec_x, zp
    
    def encode(self, x):
        latent_z, _, _ = self.encoder(x)
        return latent_z

    def decode(self, z):
        return self.decoder(z)
    

## semi-supervised learning using shared models to double batch-size      
class VAE_3d_convTrans_x16_zp_semi(nn.Module):
    """
    nc: number output channel
    ndf: number of filters in decoder
    nef: number of filters in encoder
    nz: size of latent features
    isize: input and output image size
    """
#    def __init__(self, nc=1, ndf=32, nef=32, nz=100, isize=64, device=torch.device("cuda:0"), is_train=True):
#        super(VAE, self).__init__()
    # ndf=16 or 8, test memory need
    def __init__(self, nc=1, ndf=16, nef=16, nz=256, isize=[160,192,144], #nz=64
                 device=torch.device("cuda"), is_train=True, variational=False):
        super(VAE_3d_convTrans_x16_zp_semi, self).__init__()
        self.nz = nz
        # Encoder
        self.encoder = Encoder_x16(nc=nc, nef=nef, nz=nz, isize=isize, device=device, variational=variational)
        # Decoder
        self.decoder = Decoder_x16(nc=nc, ndf=ndf, nz=nz, isize=isize)
        self.pe = nn.Linear(nz, 1, bias=False)
        if is_train == False:
            for param in self.encoder.parameters():
                param.requires_grad = False
            for param in self.decoder.parameters():
                param.requires_grad = False
            for param in self.pe.parameters():
                param.requires_grad = False
                
    # https://discuss.pytorch.org/t/how-to-create-model-with-sharing-weight/398
    def forward(self, x1, x2):
        latent_z1, mean1, logvar1 = self.encoder(x1)
        rec_x1                    = self.decoder(latent_z1)
        zp1                       = self.pe(latent_z1)

        latent_z2, mean2, logvar2 = self.encoder(x2)
        rec_x2                    = self.decoder(latent_z2)
        zp2                       = self.pe(latent_z2)        
        
        return rec_x1, zp1, rec_x2, zp2
    
    def encode(self, x):
        latent_z, _, _ = self.encoder(x)
        return latent_z

    def decode(self, z):
        return self.decoder(z)


class Encoder_3d_convTrans_x16(nn.Module):
    """
    nc: number output channel
    ndf: number of filters in decoder
    nef: number of filters in encoder
    nz: size of latent features
    isize: input and output image size
    """
#    def __init__(self, nc=1, ndf=32, nef=32, nz=100, isize=64, device=torch.device("cuda:0"), is_train=True):
#        super(VAE, self).__init__()
    # ndf=16 or 8, test memory need
    def __init__(self, nc=1, ndf=16, nef=16, nz=256, isize=[160,192,144], #nz=64
                 device=torch.device("cuda"), is_train=True, variational=False):
        super(Encoder_3d_convTrans_x16, self).__init__()
        self.nz = nz
        # Encoder
        self.encoder = Encoder_x16(nc=nc, nef=nef, nz=nz, isize=isize, device=device, variational=variational)
        # Decoder
        # self.decoder = Decoder_x16(nc=nc, ndf=ndf, nz=nz, isize=isize)
        self.pe = nn.Linear(nz, 1, bias=False)
        if is_train == False:
            for param in self.encoder.parameters():
                param.requires_grad = False
            # for param in self.decoder.parameters():
            #     param.requires_grad = False
            for param in self.pe.parameters():
                param.requires_grad = False            

    def forward(self, x):
        latent_z, mean, logvar = self.encoder(x)
        # rec_x = self.decoder(latent_z)
        zp = self.pe(latent_z)
        return zp
    
    def encode(self, x):
        latent_z, _, _ = self.encoder(x)
        return latent_z

    # def decode(self, z):
    #     return self.decoder(z)    
    
    