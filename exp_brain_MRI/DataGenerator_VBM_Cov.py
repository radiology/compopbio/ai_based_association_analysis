# -*- coding: utf-8 -*-
"""
Created on Wed Jan 12 19:19:51 2022
Datagenerator for brain autoencoder

@author: BL
"""
import numpy as np
from os.path import join,  isfile #exists,
from nilearn import image
#import nibabel as nib
# import random
# import pandas as pd
# from scipy.ndimage import rotate as scipy_rotate
# from scipy.ndimage import zoom as scipy_zoom
# from scipy.ndimage import median_filter as median_smooth
# from scipy.stats import trim_mean
# from scipy.stats.mstats import trimmed_std
# shell
# import matplotlib
# matplotlib.use('Agg')
# from matplotlib import pyplot as plt    


class DataGenerator_VBM(object):
  'Generates data for Keras'
  def __init__(self, dim_xyz, batch_size, shuffle=True): 
      'Initialization'
      self.dim_xyz = dim_xyz      
      self.batch_size = batch_size  
      self.shuffle = shuffle

# if all of the data save in same file, no need of part_path. ids could be randomly arrange in each experiment
  def generate(self, part_index, data_p, df, inc_cov):
      'Generates batches of samples'     
      # Infinite loop
      while 1:
          # Generate order of exploration of dataset
          indexes = self.__get_exploration_order(part_index)

          # Generate batches
          imax = int(len(indexes)/self.batch_size)
          if inc_cov:
              for i in range(imax):
                  # Find list of IDs
                  list_IDs_temp = [part_index[k] for k in indexes[i*self.batch_size:(i+1)*self.batch_size]]
    
                  # Generate data
    #              x, y = self.__data_generation(list_IDs_temp, data_p, exp, augm, save_fig)
                  x, y = self.__data_generation_cov(list_IDs_temp, data_p, df)
    
                  yield x, y
          else:
              for i in range(imax):
                  # Find list of IDs
                  list_IDs_temp = [part_index[k] for k in indexes[i*self.batch_size:(i+1)*self.batch_size]]
    
                  # Generate data
    #              x, y = self.__data_generation(list_IDs_temp, data_p, exp, augm, save_fig)
                  x, y = self.__data_generation(list_IDs_temp, data_p)
    
                  yield x, y              
              
  def __get_exploration_order(self, part_index):
      'Generates order of exploration'
      # Find exploration order
      indexes = np.arange(len(part_index))
      if self.shuffle == True:
          np.random.shuffle(indexes)

      return indexes
  
  def __data_generation_cov(self, list_IDs_temp, data_p, df):
      'Generates data of batch_size samples' # X : (n_samples, v_size, v_size, v_size, n_channels)
      # Initialization
      input_ = np.zeros((self.batch_size, *self.dim_xyz, 1)).astype(dtype='float32')
      # keras.backend.floatx() default is
      # Out[2]: 'float32'
      ##TODO include the following for supervised training
      # if inc_cov:
      cov    = np.zeros((self.batch_size, 3))
      label  = np.zeros((self.batch_size, 1))
      
      # Generate batch
      for i, ID in enumerate(list_IDs_temp):
          # string already
#          ID = str(int(ID))
          # load img                         
          img_p = join(data_p, ID+'_GM_to_template_GM_mod_GM.nii.gz')
          # brain mask does not exist for some dataset
          if isfile(img_p):
              img = image.load_img(img_p).get_fdata().astype(dtype='float32')
          else:
              print("file %s does not exist"%ID)
              break

          # if inc_cov:
          age  = df[df.index==ID]['age'].values      
          # print('temp_age: ', age)
          age  = age[0]
              
          sex  = df[df.index==ID]['sex'].values      
          sex  = sex[0]          
    
          dementia  = df[df.index==ID]['dementia'].values      
          dementia  = dementia[0] 
    
          apoe  = df[df.index==ID]['ApoE4'].values      
          apoe  = apoe[0]
              
          cov[i, 0] = age
          cov[i, 1] = sex
          cov[i, 2] = apoe
          label[i, 0] = dementia
         
                  
#          else:
          input_[i, :, :, :, 0] = img
          
      return [input_, cov], label  
      
  def __data_generation(self, list_IDs_temp, data_p):
      'Generates data of batch_size samples' # X : (n_samples, v_size, v_size, v_size, n_channels)
      # Initialization
      input_ = np.zeros((self.batch_size, *self.dim_xyz, 1)).astype(dtype='float32')
      # keras.backend.floatx() default is
      # Out[2]: 'float32'
      
      # Generate batch
      for i, ID in enumerate(list_IDs_temp):
          # string already
#          ID = str(int(ID))
          # load img                         
          img_p = join(data_p, ID+'_GM_to_template_GM_mod_GM.nii.gz')
          # brain mask does not exist for some dataset
          if isfile(img_p):
              img = image.load_img(img_p).get_fdata().astype(dtype='float32')
          else:
              print("file %s does not exist"%ID)
              break
                           
          input_[i, :, :, :, 0] = img
          
          # unsupervised training, y=input_
          return input_, input_ 


  
    
