This is the official codes and the synthetic image data set for the paper:<br />
AI-based association analysis for medical imaging using latent-space geometric confounder correction<br />
https://www.sciencedirect.com/science/article/pii/S1361841525000775


---------------------------Folder descriptions--------------------------<br />
Scripts for the brain MRI experiment ----- folder 'exp_brain_MRI'<br />
Scripts for the facial mesh experiment ----- folder 'exp_facial_mesh'<br />
Scripts for the synthetic dataset experiment ----- folder 'exp_synthetic_dataset'<br />
Data set for the synthetic dataset experiment ----- folder 'synthetic_dataset'<br />
<br />
<br />
<br />




---------------------------Replication of experiments--------------------------<br />
To replicate the experiment 3.1 of the manuscript, <strong>either</strong> check 'exp_3.1_with_confounder_control.ipynb', 'exp_3.1_without_confounder_control.ipynb', <strong>or</strong> follow steps below:
 

 1. install the requirements:<br />
        matplotlib==3.3.3<br />
        numpy==1.19.5<br />
        imageio==2.8.0<br />
        scikit-image==0.17.2<br />
        Shapely==1.7.1<br />
        six==1.15.0<br />
        scipy==1.5.4<br />
        Pillow>=6.2.2<br />
        opencv-python==4.2.0.34<br />
        imgaug==0.4.0<br />
        scikit-learn==0.24.1<br />
        torch==1.8.0+cu101<br />
        torchvision==0.9.0+cu101<br />
        seaborn==0.11.2<br />
        pandas==1.1.5<br />


2. download the repository

3. in folder 'synthetic_dataset', unzip 'image.zip' to its current directory, and then the image data will be at 'synthetic_dataset/image/' 

4. in command line, cd exp_synthetic_dataset, and then excute the file 'run_with_confounder_correction' (or 'run_without_confounder_correction.sh')

5. Result will be generated after the running<br />
    1)in the 'exp_synthetic_dataset' folder, find the latent_space.svg for the plotting of the latent space and the solved vector p* <br />
    2)in the folder 'exp_synthetic_dataset/log/pvae/', find the 'frame-11.png' for the 11 frames image reconstructed along vector p* 


<br />
<br />
<br />
<br />
---------------------------Methodological highlights--------------------------<br />
I. To guarantee variables are linearly captured in the latent space (n=2 in this example for intuitive demonstration), by using our proposed correlation loss in the training<br />

<figure>
  <img src="https://gitlab.com/radiology/compopbio/ai_based_association_analysis/-/raw/main/highlight_1.jpg" alt="Alt Text" />
</figure>

<br />
<br />
<br />
<br />
II. To perform vector orthogonalization for confounder control, by using the same proposed correlation loss<br />

<figure>
  <img src="https://gitlab.com/radiology/compopbio/ai_based_association_analysis/-/raw/main/highlight_2.jpg" alt="Alt Text" />
</figure>

